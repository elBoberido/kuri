<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>À propos de Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Application de suivi sportif pour Sailfish OS</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Licence: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Recherche d&apos;équipements Bluetooth</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Début de la recherche...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Annuler la recherche</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Équipement BT actuel</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Fréquence cardiaque: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Niveau de la batterie: </translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Équipements BT trouvés (appuyer pour se connecter):</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Cardiofréquencemètre</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Type de connection</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>Adresse publique BLE</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>Adresse aléatoire BLE</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Bluetooth classique</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Annuler la connexion</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">Vitesse</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Altitude</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Enregistrement</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>En pause</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Durée</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distance</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Envoyer vers Strava</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Récapitulatif</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Durée</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distance</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished">Pause</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">Carte</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">Nom</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Paramètres de carte</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Mode de centrage de la carte</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Centrer la carte sur la position actuelle</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Centrer la carte sur la piste</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Limiter la mise en cache permet d&apos;avoir des cartes à jour et de controler l&apos;utilisation de l&apos;espace disque, mais ralentit le chargement des cartes et consomme davantage de donnés. Il est nécessaire de redémarrer l&apos;application pour appliquer les paramètres.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Taille du cache</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Choissiez le style de carte.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Carte</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Rues</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Extérieur</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Satellite avec rues</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>Serveur OSM Scout</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Mes activités Strava</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Allons-y !</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Utiliser un capteur cardiaque</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Désactiver l&apos;extinction de l&apos;écran</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Désactiver l&apos;extinction de l&apos;écran pendant l&apos;enregistrement.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Utiliser le capteur cardiaque si disponible</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Utiliser le capteur cardiaque d&apos;une autre application, par ex. Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Masquer la carte</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Afficher la carte</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Départ</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Durée</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distance</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation type="unfinished">Cadence</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Altitude</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distance</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Durée</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Altitude</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Enregistrer l&apos;activité</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Quitter sans enregistrer</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Cardiofréquencemètre</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Carte</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Paramètres généraux</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Activer l&apos;enregistrement automatique</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Course à pied</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Vélo de route</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>VTT</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Marche</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Patins à roulette</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Ski</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Randonnée</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Toutes les activités</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Description:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Heure de début:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Durée:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distance:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Vitesse max/⌀:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kudos:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Commentaires:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Dénivelé:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Réalisations/PRs</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kudos</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Durée:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distance:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Dénivelé:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Catégorie d&apos;escalade:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Meilleur effort:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Segments</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Paramètres Strava</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Nom d&apos;utilisateur: </translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Pays: </translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Nom de l&apos;activité pour Strava</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Description de l&apos;activité pour Strava</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Trajet</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Envoi des données...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Vérification de l&apos;envoi...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Une erreur inconnue est survenue</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Envoi de l&apos;activité achevé</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Connexion à l&apos;équipement HR</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Erreur HR: </translation>
    </message>
</context>
</TS>
