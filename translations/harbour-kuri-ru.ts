<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>О программе Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Ваш персональный тренер для ОС Sailfish</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Лицензия: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation>jdrescher2006 (разработчик Laufhelden), Simoma (разработчик Rena), и все, внёсшие вклад в развитие Laufhelden и Rena</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Участники разработки</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Благодарности</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Исходный код</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation>Иконки</translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation>Отзывы, баги</translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Поиск Bluetooth устройств</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Сканировать...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Отменить сканирование</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Подключенное устройство</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Пульс: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> уд/мин</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Заряд батареи: </translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Найденные устройства (коснитесь чтобы подключить):</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Пульсометр</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Подключить</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Тип подключения</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>BLE зарегистрированный адрес</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>BLE случайный адрес</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Обычный Bluetooth</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Прервать подключение</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">Скорость</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Подъём</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished">Сердечный ритм</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Запись</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Длительность</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Расстояние</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">км</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">мин/км</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished">Темп ⌀</translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Отправить в Strava</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Обзор</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Длительность</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>чч:мм:сс</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation>Скорость ⌀</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Темп ⌀</translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation>Пульс ⌀</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">км</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">км/ч</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">мин/км</translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">Карта</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">м</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Описание</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation>GPS: </translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation>Ожидание GPS</translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation>МСР выкл.</translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation>МСР: </translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>км/ч</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Настройка карты</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Центрирование карты</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Текущее положение</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Пройденный путь</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Ограничение кеширования обеспечит самые свежие карты и экономит место в памяти, но загрузка карт происходит медленнее, а объём передачи данных растёт. Учтите, что размер кеша изменится только после перезапуска программы.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Размер кеша</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Выберите стиль карты.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Карта</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Улицы</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>На природе</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Светлый</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Тёмный</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Спутник</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Спутник + улицы</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>Сервер OSM Scout</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Моя активность на Strava</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">км</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Старт! </translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Пульсометр</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Запретить гашение экрана</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Отключить гашение экрана во время тренировки.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Использовать сервер данных пульса</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Использовать данные пульса из другого приложения, например Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Скрыть карту</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Показать карту</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation>Смена цветовой схемы</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation>Экран блокировки</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation>Длительность</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation>Темп</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation>Калории</translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Сердечный ритм</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Подъём</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation>Калории</translation>
    </message>
    <message>
        <source>kcal</source>
        <translation>ккал</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation>Длительность</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>ч:м:с</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation>м:c</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation>Подъём</translation>
    </message>
    <message>
        <source>m</source>
        <translation>м</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation>Сердечный ритм</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>уд/мин</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation>мин/км</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished">Темп ⌀</translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Сохранить путь</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Выйти без сохранения</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Пульсометр</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Общие настройки</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Карта</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Общие настройки</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Автосохранение</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Бег</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Шоссейный велосипед</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Горный велосипед</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Ходьба</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Роликовые коньки</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Лыжи</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Поход</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Все виды активности</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Описание:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Начало:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Длительность:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Расстояние:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Отклик:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Комментарии:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Набор высоты:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Достижения:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Скорость макс/средн:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">км</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">км/ч</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">м</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Комментарии</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Отклик</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Длительность:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Расстояние:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Разность высоты:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Категория подъёма:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Лучший результат:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">км</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">м</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Сегменты</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">км</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Настройка Strava</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Войти</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Пользователь: </translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Страна: </translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Имя активности для Strava</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Описание активности для Strava</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Поездка</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Отправка данных...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Проверка...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Неизвестная ошибка</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Отправка данных завершена</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation>Загрузка в Strava</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation>Загрузка GPX...</translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation>км</translation>
    </message>
    <message>
        <source>/km</source>
        <translation>/км</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>уд/мин</translation>
    </message>
    <message>
        <source>m</source>
        <translation>м</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Подключение пульсометра</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Ошибка измерения пульса:</translation>
    </message>
</context>
</TS>
