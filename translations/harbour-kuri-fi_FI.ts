<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi_FI">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>Tietoja Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Kuntoilusovellus Sailfish OS käyttöjärjestelmälle</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Lisenssi: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation type="unfinished">Sykemittari</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">Nopeus</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Harjoitus</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Keskeytetty</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Kesto</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Matka</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Lähetä Stravaan</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Kesto</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Matka</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished">Pysäytä</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">Kartta</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">Nimi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Kuvaus</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Kartta-asetukset</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Keskitä kartta</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Nykyisen sijainnin mukaan</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Reitin mukaan</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Rajoittamalla palasten välimuistia varmistaa kartan ajantasaisuuden ja pitää levyntilan käytön kurissa, mutta samalla hidastaa käyttöä ja lisää datankäyttöä. Asetus tulee voimaan uudelleenkäynnistyksen jälkeen.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Välimuistin koko</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Valitse kartan tyyli.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Kartta</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Kadu</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Ulkoilu</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Kevyt</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Tumma</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satelliitti</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Satelliitti Kadut</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Minun Strava Aktiviteetit</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>N-Y-T nyt!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Käytä sykemittaria</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Estä näytön sammuminen</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Estä ruudun sammuminen kun tallennetaan.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Käytä HRM palvelua jos käytettävissä</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Käytä sykkeen seurantaa toisesta sovelluksesta. esim. Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Piilota kartta</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Näytä kartta</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Aloita</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Kesto</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Matka</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Matka</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Kesto</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Tallenna reitti</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Tallenna</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Kuvaus</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Sulje tallentamatta</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Sykemittari</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Yleiset</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Kartta</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Yleiset asetukset</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Automaatti tallennus</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Juoksu</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Maantiepyöräily</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Maastopyöräily</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Kävely</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Rullaluistelu</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Hiihto</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Vaeltaminen</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Kaikki harjoitukset</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Kuvaus:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Alotusaika:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Kesto:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Matka:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Nopeus max/⌀:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Saavutukset:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kudos:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Kommentit:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Korkeusero:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Kommentit</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kudos</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Kesto:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Matka:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Korkeusero:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Kiipeämis kategoria:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Paras yritys:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Osioita:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Strava asetukset</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Country: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Harjoituksen nimi Stravaan</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Kuvaus</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Harjoituksen kuvaus Stravaan</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Lähetetään tietoja...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Tarkistetaan lähetystä...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Tapahtui tuntematon virhe</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Harjoitus lähetetty</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Tyyppi</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">Km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
