<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>Acerca de Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Aplicación de entrenamiento deportivo para Saifish OS</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Licencia: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation>Sensor de frecuencia cardiaca</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Buscar dispositivos Bluetooth</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Iniciar escaneo...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Cancelar escaneo</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Dispositivo BT actual</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Frecuencia cardiaca: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Nivel de batería: </translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Tipo de conexión</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>Dirección pública BLE</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>Dirección aleatoria BLE</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Bluetooth clásico</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Conectar</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Desconectar</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Dispositivos BT (presiona para conectar):</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Cancelar Conectar</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">Velocidad</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Altitud</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Grabando</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>En pausa</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duración</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distancia</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duración</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distancia</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished">Pausa</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">Mapa</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">Nombre</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Descripción</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Ajustes del mapa</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Modo de centrado del mapa</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Centrar en la posición actual</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Centrar en el trayecto</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Al limitar el almacenamiento de teselas en la caché te aseguras que los mapas estén actualizados y mantienes el disco bajo control, pero los mapas tardan más en cargarse y se produce un mayor tráfico de datos. Ten en cuenta que el ajuste del tamaño de la caché se aplicará después de reiniciar la aplicación.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Tamaño de la caché</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Elige el estilo del mapa.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Calles</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Exterior</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Claro</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Oscuro</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satélite</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Calles satélite</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Mis actividades Strava</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>¡Vamos!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Usar dispositivo HRM</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Desactivar apagado de pantalla</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Desactivar que la pantalla se apague durante la grabación.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Usar servicio HRM si está disponible</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Usar monitor cardiaco desde otra aplicación, p.e. Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Ocultar mapa</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Mostrar mapa</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duración</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distancia</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation type="unfinished">Ritmo</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Altitud</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Distancia</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duración</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Altitud</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Guardar trayecto</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Salir sin guardar</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Sensor de frecuencia cardiaca</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Ajustes generales</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Habilitar autoguardado</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Correr</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Bicicleta</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Bicicleta de montaña</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Caminar</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Patinaje</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Esquí</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Senderismo</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Todas las actividades</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Descripción:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Hora de inicio:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Duración:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distancia:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Velocidad máx/⌀:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Méritos:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Comentarios:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Desnivel:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Logros/PRs:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Méritos</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Duración:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distancia:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Dif. altitud:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Categoría de escalada:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Mejor esfuerzo:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Segmentos</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Ajustes de Strava</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Cerrar sesión</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Iniciar sesión</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Nombre de usuario: </translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>País: </translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Nombre de actividad para Strava</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Descripción de actividad para Strava</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Desplazamiento</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Subiendo datos...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Comprobando subida...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Ha ocurrido un error desconocido</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Subida de actividad completada</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Conectando a dispositivo FC...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Error FC: </translation>
    </message>
</context>
</TS>
