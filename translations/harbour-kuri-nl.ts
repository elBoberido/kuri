<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>Over Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Sporttracker voor Sailfish OS</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Licentie: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation>Hartslagmeter</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Bluetoothapparaten zoeken</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Begin zoeken...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Zoeken annuleren</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Huidig apparaat</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Hartslag:</translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> slagen per minuut</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Accuniveau</translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Verbindingstype</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>BLE omroep</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>BLE willekeurig adres</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Standaard Bluetooth</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Ontkoppelen</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>BT-apparaten gevonden (druk om te verbinden):</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Verbinden annuleren</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">Snelheid</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Hoogte</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Opnemen</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Gepauzeerd</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duur</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Afstand</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Verzenden naar Strava</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Overzicht</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duur</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Afstand</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished">Pauzeren</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/u</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">Kaart</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">Naam</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Beschrijving</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/u</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Kaartinstellingen</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Kaartcentreerummodus</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Huidige positie op kaart centreren</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Route op kaart centreren</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Inperken van tegelcache zorgt ervoor dat je kaarten up-to-date blijven en beperkt het geheugengebruik, maar zorgt er ook voor dat kaarten minder snel laden en verhoogt het dataverbruik. Let op: de cachegrootteinstelling wordt pas toegepast na herstarten van de app.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Cachegrootte</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Kies een kaartstijl.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Kaart</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Straten</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Buiten</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Licht</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Donker</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satelliet</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Straten (satelliet)</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Mijn Strava-activiteiten</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Tijd om te vertrekken!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Hartslagmonitor gebruiken</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Automatisch uitschakelen van scherm uitschakelen</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Schakel automatisch uitschakelen van scherm uit tijdens opnames.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Gebruik hartslagmonitor indien beschikbaar.</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Gebruik hartslagmonitor van een andere app zoals Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Kaart verbergen</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Kaart weergeven</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Begin</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duur</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Afstand</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation type="unfinished">Tempo</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Hoogte</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Afstand</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Duur</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Hoogte</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation type="unfinished">min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Route opslaan</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschrijving</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Afsluiten zonder op te slaan</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Hartslagapparaat</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Kaart</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Algemene instellingen</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Automatisch opslaan inschakelen</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Lopen</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Fietsen</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Mountainbiken</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Wandelen</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Inline-skaten</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Skiën</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Bergwandelen</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Alle activiteiten</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Beschrijving:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Begintijd:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Tijd:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Afstand:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Snelheid max/⌀:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kudo’s:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Reacties:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Hoogteverschil</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Prestaties / persoonlijke records:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/u</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Reacties</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kudo’s</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Duur:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Afstand:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Hoogteverschil:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Klimcategorie:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Beste poging:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Segmenten</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Strava-instellingen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Uitloggen</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Inloggen</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Gebruikersnaam: </translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Land</translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Activiteitsnaam voor Strava</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschrijving</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Activiteitsbeschrijving voor Strava</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Woon/werk</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Gegevens uploaden...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Upload controleren...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Er is een onbekende fout opgetreden</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Activiteitsupload voltooid</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Verbinding maken met hartslagapparaat...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Fout bij hartslag: </translation>
    </message>
</context>
</TS>
