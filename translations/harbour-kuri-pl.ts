<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>O Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Sport Tracker dla Sailfish OS</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Licencja: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Wersja</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation>jdrescher2006 (opiekun Laufhelden), Simona (opiekun Rena) oraz wszyscy współautorzy Laufhelden i Rena</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Współtwórcy</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Podziękowania</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Kod źródłowy</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation>Ikony</translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation>Informacja zwrotna, błędy</translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation>Urządzenie do pomiaru tętna</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Skanuj w poszukiwaniu urzadzen Bluetooth</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Początek skanowania...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Anuluj skanowanie</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Obecne urzadzenie BT</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Brak</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Tętno: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Poziom baterii: </translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Typ połączenia</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>Publiczny adres BLE</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>Losowy adres BLE</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Klasyczny Bluetooth</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Połącz</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Rozłącz</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Znaleziono urządzenia BT (naciśnij aby połączyć):</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Anuluj łączenie</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation>Prędkość</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Przewyższenie</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Tętno</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Rejestrowanie</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Wstrzymany</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Dystans</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Udostępnij na Strava</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Dystans</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation>Prędkość ⌀</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation>Tętno ⌀</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Wstrzymaj</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation>Przewyższenie 🠝</translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation>Przewyższenie 🠟</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">Nazwa</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Opis</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation>GPS: </translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation>Oczekiwanie na GPS</translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation>HRM wyłączone</translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation>HRM: </translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>O Kuri</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Ustawienia map</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Tryb wyśrodkowanej mapy</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Wyśrodkuj obecną pozycję na mapie</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Wyśrodkuj ścieżkę na mapie</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Ograniczanie buforowania kafelków zapewnia aktualność map i kontroluje użycie dysku, ale ładuje mapy wolniej i powoduje większy ruch danych. Należy pamiętać, że ustawienia rozmiaru pamięci podręcznej zostaną zastosowane po ponownym uruchomieniu aplikacji.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Rozmiar pamięci podręcznej</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Wybierz styl map.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Ulice</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Na zewnątrz</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Światło</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Ciemno</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satellite</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Satellite Streets</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Moje aktywności Strava</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Start!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Użyj urządzenia HRM</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Wyłącz wygaszanie ekranu</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Wyłącz wygaszanie ekranu podczas rejetrowania treningu.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Użyj usługi HRM jeśli dostępna</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Użyj monitora pracy serca z innej aplikacji np. Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Ukryj mapę</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Pokaż mapę</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Początek</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation>Zmień schemat kolorów</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation>Zablokuj ekran</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Zatrzymaj</translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation>Usługa HRM nie została znaleziona</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Sekcja</translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Dystans</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation>Kalorie</translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation>Rodzaje pola</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Tętno</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Przewyższenie</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation>Czas trwania sekcji</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation>Kalorie</translation>
    </message>
    <message>
        <source>kcal</source>
        <translation>kcal</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Dystans</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation>Czas trwania</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation>Czas trwania (sekcja %1)</translation>
    </message>
    <message>
        <source>m:s</source>
        <translation>m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation>Przewyższenie</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation>Tętno</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translatorcomment>min/km</translatorcomment>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Zapisz ślad</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Wyjdź bez zapisywania</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Ustawienia</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Urządzenie do pomiaru tętna</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Ustawienia ogólne</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Włącz autozapisywanie</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Bieganie</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Rower szosowy</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Rower górski</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Spacer</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Jazda na rolkach</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Narciarstwo</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Wędrówka</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Wszystkie aktywności</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Starting time:</source>
        <translation>Czas rozpoczęcia:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Czas trwania:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Dystans:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Prędkość max/⌀:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kudos:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Komentarze:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Przewyższenie:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Osiągnięcia:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Komentarze</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kudos</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Czas trwania:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Dystans:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Przewyższenie:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Rodzaj wspinaczki:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Najlepsze osiągnięcie:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Segmenty</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Ustawienia Stravy</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Wyloguj</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Zaloguj</translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Kraj: </translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Nazwa użytkownika: </translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation>brak logów w</translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Strava Upload</source>
        <translation>Udostępnij na Strava</translation>
    </message>
    <message>
        <source>Activity name for Strava</source>
        <translation>Nazwa aktywniości dla Stravy</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Opis aktywności dla Stravy</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Opis</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Rodzaj</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Do lub z pracy</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Udostępnij</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Wysyłanie danych...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Sprawdzanie wysyłania...</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Wysyłanie aktywności zakończone</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Wystąpił nieznany błąd</translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation>Plik GPX wysłany...</translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation>Pozycja:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation>/km</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Łączenie z urządzeniem HR...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Błąd HR: </translation>
    </message>
</context>
</TS>
