<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>Om Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Spårning av sportaktiviteter för Sailfish OS</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Licens: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation>jdrescher2006 (Laufheldens utvecklare), Simona (Renas utvecklare) och alla bidragsgivare till Laufhelden och Rena</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Medhjälpare</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Tack</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Källkod</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation>Ikoner</translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation>Återkoppling och fel</translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation>Hjärtfrekvensenhet</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Sök efter Bluetooth-enheter</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Starta sökning...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Avbryt sökning</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Aktuell Bluetooth-enhet</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Hjärtfrekvens: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Batterinivå: </translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Anslutningstyp</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>BLE publik adress</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>BLE slumpmässig adress</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Klassisk Bluetooth</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Anslut</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Koppla ifrån</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Hittade BT-enheter (tryck för att ansluta):</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Avbryt anslutning</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation>Diagram</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation>Hastighet</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Höjd</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Hjärtfrekvens</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Spelar in</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Pausad</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Varaktighet</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distans</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation>Sammanlagd distans</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>tim</translation>
    </message>
    <message>
        <source>Total time</source>
        <translation>Sammanlagd tid</translation>
    </message>
    <message>
        <source>Activities</source>
        <translation>Aktiviteter</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Skicka till Strava</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Översikt</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Varaktighet</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distans</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation>Hastighet ⌀</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation>Hjärtfrekvens ⌀</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Paus</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Karta</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation>Höjd 🠝</translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation>Höjd 🠟</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exportera</translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation>Aktivitetsexport slutförd</translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beskrivning</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation>Aktivitet:</translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation>GPS: </translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation>Väntar på GPS</translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation>Hfm av</translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation>Hfm: </translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation>Import/Export</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Exportera</translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation>Målmapp</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Importera</translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation>Källmapp</translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation>Åsidosätt befintlig</translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation>Redan importerade aktiviteter åsidosätts</translation>
    </message>
    <message>
        <source>Migration</source>
        <translation>Migration</translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation>Tidigare migrerade aktiviteter åsidosätts</translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation>Upprepa migration</translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation>Aktivitetsexport slutförd</translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation>Aktivitetsimport slutförd</translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation>Aktivitetsmigration slutförd</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation>Mina Strava-aktiviteter</translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation>Starta aktivitet</translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation>%1 aktiviteter</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation>Tar bort aktivitet...</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Redigera</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>tim</translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation>Migrerar gamla aktiviteter</translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Kartinställningar</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Kartcentreringsläge</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Centrera aktuell position på kartan</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Centrera spåret på kartan</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Begränsad cachelagring säkerställer uppdaterade kartor och håller diskanvändningen under kontroll, men orsakar långsammare kartinläsning och mer datatrafik. Notera att inställning av cachestorlek tillämpas först efter omstart av appen.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Cachestorlek</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Välj kartstil.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Karta</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Gator</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Utomhus</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Ljus</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Mörk</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satellit</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Satellitgator</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Mina Strava-aktiviteter</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Starta!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Använd hfm-enhet</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Inaktivera skärmsläckning</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Inaktivera skärmsläckning vid inspelning.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Använd hfm-tjänst om tillgänglig</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Använd hjärtfrekvensmätare från annat program, t.ex. Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation>Aktivitet:</translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation>Använd hjärtfrekvensmätare i denna aktivitet.</translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Dölj karta</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Visa karta</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Starta</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation>Byt färgtema</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation>Låsskärm</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Stopp</translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation>Ingen hfm-tjänst hittades</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Delmoment</translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation>Varaktighet</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distans</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation>Kalorier</translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation>Fälttyper</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Hjärtfrekvens</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Höjd</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation>Delmomentets varaktighet</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation>Kalorier</translation>
    </message>
    <message>
        <source>kcal</source>
        <translation>kcal</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>Distance</source>
        <translation>Distans</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation>Varaktighet</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation>Varaktighet (Delmoment %1)</translation>
    </message>
    <message>
        <source>m:s</source>
        <translation>m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation>Höjd</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation>Hjärtfrekvens</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation>Återanslut HRM-enhet</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Spara spårningen</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beskrivning</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Avsluta utan att spara</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Hjärtfrekvensenhet</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allmänt</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Karta</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation>Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation>Import/Export</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Generella inställningar</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Spara automatiskt</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation>Du behöver inte ange aktivitetsnamn i slutet av aktiviteten.</translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Löpning</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Landsvägscykel</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Mountainbike</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Gång</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Inlines</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Skidor</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Fotvandring</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Alla aktiviteter</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Okänd</translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation>Strava autentiseringsfel: </translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Beskrivning:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Starttid:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Varaktighet:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distans:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Hastighet max/⌀:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kudos:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Kommentarer:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Höjdökning:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Insatser/kodbidrag:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/t</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Kommentarer</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kudos</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Varaktighet:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distans:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Höjdskillnad:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Klättringskategori:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Bästa insats:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Segment</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Strava Inställningar</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Logga ut</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Logga in</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Användarnamn: </translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Land: </translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation>Inte inloggad</translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Aktivitetsnamn för Strava</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beskrivning</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Aktivitetsbeskrivning för Strava</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Växla</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Laddar upp data...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Kontrollerar uppladdning...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Ett okänt fel inträffade</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Uppladdning slutförd</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation>Strava-uppladdning</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Ladda upp</translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation>GPX uppladdad...</translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation>Pos:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation>/km</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Ansluter hjärtfrekvensenhet...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Hjärtfrekvensfel: </translation>
    </message>
</context>
</TS>
