<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>关于 Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>旗鱼系统运动轨迹记录应用</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>许可协议: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation>jdrescher2006 (Laufhelden 维护者), Simona (Rena 维护者) 及全体 Laufhelden 与 Rena 贡献者</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>搜索蓝牙设备</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>开始扫描...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>取消扫描</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>当前蓝牙设备</translation>
    </message>
    <message>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>心率:</translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>电量水平:</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>发现蓝牙设备(按下以连接):</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>心率设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>连接类型</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>蓝牙低功耗播音</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>蓝牙随机地址</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>经典蓝牙</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>取消连接</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">速度</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">海拔</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished">心率</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>正在记录</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>暂停</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished">h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">距离</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">分钟/千米</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished">h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">距离</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished">暂停</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">千米/小时</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished">分钟/千米</translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">地图</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">米</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished">名称</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">描述</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation>GPS:</translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation>等待 GPS</translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation type="unfinished">心率设备断开</translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>km</source>
        <translation>千米</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>千米/小时</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>地图设置</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>地图集中模式</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>在地图上集中到当前位置</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>在地图上集中轨迹</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>限制磁贴缓存可确保最新的地图并使磁盘的使用处于控制之下, 但加载地图的速度会变慢, 并导致更多的数据流量。请注意， 应用缓存大小设置将在应用程序重新启动后生效。</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>缓存大小</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>选择地图风格。</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>地图</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>街道</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>户外</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>亮色</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>黑暗</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>卫星</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>卫星街道</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout 服务</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>我的 Strava 动态</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>让我们开始吧! </translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>使用心率监测设备</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>禁用屏幕熄灭</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>当记录时禁用屏幕熄灭</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>如果可用则使用心率监测器</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>通过另一个软件使用心率监测器，例如 Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>隐藏地图</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>隐藏地图</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation>切换色彩模式</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation>锁屏</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation>时长</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>距离</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation>步频</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>心率</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation>卡路里</translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation>区域类型</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">海拔</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation>卡路里</translation>
    </message>
    <message>
        <source>kcal</source>
        <translation>千卡</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation>千米</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>距离</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation>用时</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation>m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">海拔</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">米</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation>心率</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation>分钟/千米</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>储存轨迹</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>退出且不保存</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>心率设备</translation>
    </message>
    <message>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>地图</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>常规设置</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>启用自动保存</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>跑步</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>越野车</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>山地自行车</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>步行中</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>滑冰</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>滑雪</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>徒步旅行</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>所有运动</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>描述:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>开始时间:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>持续时间:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>距离:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>荣誉:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>评论:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>海拔获取:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>成就/PRs:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>最大速率x/⌀：</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">千米/小时</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">米</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>评论</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>荣誉</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>持续时间:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>距离:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>海拔差:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>增加分类:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>勉力:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">米</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>片段</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Strava 设置</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>登出</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>登陆</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>用户名:</translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>国家：</translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Strava 活动名称</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Strava 活动描述</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>通勤</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>正在上传数据...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>检测上传中...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>发生未知错误</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>运动已上传完毕</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation>Strava 上传</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>上传</translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">千米</translation>
    </message>
    <message>
        <source>/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished">bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">米</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>正在连接到心率设备...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>心率记录错误:</translation>
    </message>
</context>
</TS>
