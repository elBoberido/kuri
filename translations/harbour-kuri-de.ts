<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <source>About Kuri</source>
        <translation>Über Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Sport Tracker App für Sailfish OS</translation>
    </message>
    <message>
        <source>License: GPLv3</source>
        <translation>Lizenz: GPLv3</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation>jdrescher2006 (Laufhelden Maintainer), Simona (Rena Maintainer) und alle die zu Laufhelden and Rena beigetragen haben</translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation>Mitwirkende</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Danksagung</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Quellcode</translation>
    </message>
    <message>
        <source>Icons</source>
        <translation>Symbole</translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation>Feedback, Probleme</translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation>Herzfrequenzmesser</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Suchen nach Bluetooth-Geräten</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Starte Suche...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Suche Abbrechen</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Aktuelles BT-Gerät</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Keines</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Herzfrequenz: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Batterie: </translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Bluetooth-Typ</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>BLE Public Adresse</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>BLE Random Adresse</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Classic Bluetooth</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Trennen</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Gefundene Geräte (drücken zum Verbinden):</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Verbindung Abbrechen</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation>Diagramme</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation>Geschwindigkeit</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Puls</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Aufzeichnen</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Pausiert</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation>Gesamtdistanz</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>Std.</translation>
    </message>
    <message>
        <source>Total time</source>
        <translation>Gesamtzeit</translation>
    </message>
    <message>
        <source>Activities</source>
        <translation>Aktivitäten</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Auf Strava hochladen</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Übersicht</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation>Geschw. ⌀</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation>Puls ⌀</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Karte</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation>Höhenmeter 🠝</translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation>Höhenmeter 🠟</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation>Aktivitätenexport beendet</translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation>Aktivität:</translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation>GPS: </translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation>Warten auf GPS</translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM: </source>
        <translation>HFM: </translation>
    </message>
    <message>
        <source>HRM off</source>
        <translation>HFM aus</translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation>Import/Export</translation>
    </message>
    <message>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation>Zielordner</translation>
    </message>
    <message>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation>Quellordner</translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation>Bestehende überschreiben</translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation>Bereits importierte Aktivitäten werden überschrieben</translation>
    </message>
    <message>
        <source>Migration</source>
        <translation>Migration</translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation>Zuvor migrierte Aktivitäten werden überschrieben</translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation>Migration wiederholen</translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation>Aktivitätenexport beendet</translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation>Aktivitätenimport beendet</translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation>Aktivitätenmigration beendet</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Über Kuri</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation>Meine Strava Aktivitäten</translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation>Aktivität starten</translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation>%1 Aktivitäten</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation>Entferne Aktivität...</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <source>hrs</source>
        <translation>Std.</translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation>Alte Aktivitäten werden migriert</translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Karten Einstellungen</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Karten-Mittelpunkt</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Aktuelle Position auf der Karte zentrieren</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Track auf der Karte zentrieren</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>Diese Einstellung wird erst nach Neustart der App übernommen.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Cache Grösse</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Kartenstil wählen.</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Karte</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Strassen</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Outdoor/Gelände</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Satellit</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Satellit mit Strassen</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Meine Strava Aktivitäten</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Los geht&apos;s!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Pulsgurt benutzen</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Bildschirmschoner abschalten</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Bildschirmschoner abschalten während Training</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>HFM Dienst benutzen</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Herzfrequenzmonitor aus einer anderen Anwendung verwenden, z.B. Amazfish</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation>Aktivität:</translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation>Pulsgurt in dieser Aktivität benutzen</translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Karte ausblenden</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Karte anzeigen</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation>Farbschema wechseln</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation>Sperren</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Stopp</translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation>HFM Dienst nicht gefunden</translation>
    </message>
    <message>
        <source>Section</source>
        <translation>Abschnitt</translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation>Tempo</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation>Kalorien</translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation>Felder</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation>Dauer des Abschnitts</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation>Kalorien</translation>
    </message>
    <message>
        <source>kcal</source>
        <translation>kcal</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation>Distanz</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation>Dauer</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation>h:m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation>Dauer (Abschnitt %1)</translation>
    </message>
    <message>
        <source>m:s</source>
        <translation>m:s</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation>Höhe</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation>HFM wiederverbinden</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation>min/km</translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation>Tempo ⌀</translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Aktivität speichern</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Beenden ohne Speichern</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Herzfrequenzmesser</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Karte</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation>Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation>Import/Export</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Allgemeine Einstellungen</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Automatisch speichern</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation>Automatisch speichern am Ende eines Trainings.</translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Laufen</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Rennrad</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Mountainbike</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Walking</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Inlineskating</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Skifahren</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Wandern</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Alle Aktivitäten</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation>Strava Authentifizierungsfehler: </translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Beschreibung:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Startzeit:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Dauer:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distanz:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Geschw. max/⌀:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kudos:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Kommmentare:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Höhengewinn:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Erfolge/PRs</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Kommentare</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kudos</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Dauer:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Distanz:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Höhenunterschied:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Kletterkategorie:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Bester Aufwand:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Segmente</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Strava Einstellungen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Abmelden</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Benutername: </translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Land: </translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation>nicht angemeldet</translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Name der Aktivität</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Beschreibung der Aktivität</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Commute</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Daten hochladen...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Überprüfe...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Ein unbekannter Fehler ist aufgetreten</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Hochladen abgeschlossen</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation>Strava Upload</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Upload</translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation>GPX hochgeladen...</translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation>Pos:</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation>/km</translation>
    </message>
    <message>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>m</source>
        <translation>m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Verbinde mit HR-Gerät...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>HR-Fehler: </translation>
    </message>
</context>
</TS>
