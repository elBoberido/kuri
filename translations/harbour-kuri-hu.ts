<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AboutPage</name>
    <message>
        <source>License: GPLv3</source>
        <translation>Licensz: GPLv3</translation>
    </message>
    <message>
        <source>About Kuri</source>
        <translation>Az Kuri</translation>
    </message>
    <message>
        <source>Sport Tracker application for Sailfish OS</source>
        <translation>Sporttevékenység naplózó alkalmazás Sailfish OS-re</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Verzió</translation>
    </message>
    <message>
        <source>jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback, bugs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BTConnectPage</name>
    <message>
        <source>Heart rate device</source>
        <translation>Pulzusmérő</translation>
    </message>
    <message>
        <source>Scan for Bluetooth devices</source>
        <translation>Bluetooth eszközök keresése</translation>
    </message>
    <message>
        <source>Start scanning...</source>
        <translation>Keresés...</translation>
    </message>
    <message>
        <source>Cancel scanning</source>
        <translation>Keresés megszakítása</translation>
    </message>
    <message>
        <source>Current BT device</source>
        <translation>Jelenlegi BT eszköz</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <source>Heart Rate: </source>
        <translation>Pulzus: </translation>
    </message>
    <message>
        <source> bpm</source>
        <translation>/perc</translation>
    </message>
    <message>
        <source>Battery Level: </source>
        <translation>Akku: </translation>
    </message>
    <message>
        <source>Connection Type</source>
        <translation>Csatlakozás típusa</translation>
    </message>
    <message>
        <source>BLE Public Address</source>
        <translation>BLE publikus cím</translation>
    </message>
    <message>
        <source>BLE Random Address</source>
        <translation>BLE véletlen cím</translation>
    </message>
    <message>
        <source>Classic Bluetooth</source>
        <translation>Klasszikus Bluetooth</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Csatlakozás</translation>
    </message>
    <message>
        <source>Cancel Connect</source>
        <translation>Csatlakozás megszakítása</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Lecsatlakozás</translation>
    </message>
    <message>
        <source>Found BT devices (press to connect):</source>
        <translation>Elérhető BT eszközök:</translation>
    </message>
</context>
<context>
    <name>ChartViewPage</name>
    <message>
        <source>Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished">Sebesség</translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Magasság</translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Recording</source>
        <translation>Rögzítés</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Szüneteltetve</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Időtartam</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Távolság</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailedViewPage</name>
    <message>
        <source>Send to Strava</source>
        <translation>Feltöltés a Strava-ra</translation>
    </message>
    <message>
        <source>Overview</source>
        <translation>Általános</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Időtartam</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Távolság</translation>
    </message>
    <message>
        <source>Speed ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate ⌀</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="unfinished">Szünet</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>min/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Map</source>
        <translation type="unfinished">Térkép</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
    <message>
        <source>Elevation 🠝</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation 🠟</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditActivityDialog</name>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Leírás</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GPSIndicator</name>
    <message>
        <source>GPS: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for GPS</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HRMBatteryIndicator</name>
    <message>
        <source>HRM off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportExportSettingsPage</name>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Override existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already imported activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previously migrated activities will be overridden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Redo migration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity export finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity import finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activity migration finished</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <source>km</source>
        <translation>km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation>km/h</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Az alkalmazásról</translation>
    </message>
    <message>
        <source>My Strava activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start activity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 activities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing activity...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hrs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Migrating old activities</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapSettingsPage</name>
    <message>
        <source>Map settings</source>
        <translation>Térkép beállítások</translation>
    </message>
    <message>
        <source>Map center mode</source>
        <translation>Térkép központozás módja</translation>
    </message>
    <message>
        <source>Center current position on map</source>
        <translation>Jelenlegi helyzet legyen középen</translation>
    </message>
    <message>
        <source>Center track on map</source>
        <translation>Az útvonal legyen középre igazítva</translation>
    </message>
    <message>
        <source>Choose map style.</source>
        <translation>Válaszd ki a térkép megjelenítési módját</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Térkép</translation>
    </message>
    <message>
        <source>Streets</source>
        <translation>Utcák</translation>
    </message>
    <message>
        <source>Outdoors</source>
        <translation>Kültér</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Világos</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Sötét</translation>
    </message>
    <message>
        <source>Satellite</source>
        <translation>Műhold</translation>
    </message>
    <message>
        <source>Satellite Streets</source>
        <translation>Műhold + utcák</translation>
    </message>
    <message>
        <source>OSM Scout Server</source>
        <translation>OSM Scout Server</translation>
    </message>
    <message>
        <source>Limiting tile caching ensures up-to-date maps and keeps disk use under control, but loads maps slower and causes more data traffic. Note that the cache size settings will be applied after restart of the application.</source>
        <translation>A tile cache segít abban, hogy a környező térképadatok mindig frissek legyenek, a lemezhasználat kordában tartásam mellett,azonban így a térképek lassabban töltenek be és nagyobb adatforgalmat igényel a művelet. A cache méret beállítás módosítása csak az alkalmazás újraindítása után lép életbe.</translation>
    </message>
    <message>
        <source>Cache size</source>
        <translation>Tile cache mérete</translation>
    </message>
</context>
<context>
    <name>MyStravaActivities</name>
    <message>
        <source>My Strava Activities</source>
        <translation>Strava-ba feltöltött edzéseim</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>PreRecordPage</name>
    <message>
        <source>Let&apos;s go!</source>
        <translation>Indulhatunk!</translation>
    </message>
    <message>
        <source>Use HRM device</source>
        <translation>Pulzusszámláló használata</translation>
    </message>
    <message>
        <source>Disable screen blanking</source>
        <translation>Képernyő kikapcsolásának tiltása</translation>
    </message>
    <message>
        <source>Disable screen blanking when recording.</source>
        <translation>Rögzítés során a képernyő kikapcsolásának megakadályozása.</translation>
    </message>
    <message>
        <source>Use HRM service if available</source>
        <translation>Pulzusmérés használata ha elérhető</translation>
    </message>
    <message>
        <source>Use heart rate monitor from another application e.g. Amazfish</source>
        <translation>Pulzus adatok átvétele másik alkalmazásból (pld. Amazfish)</translation>
    </message>
    <message>
        <source>Activity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use heart rate monitor in this activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPage</name>
    <message>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <source>Hide Map</source>
        <translation>Térkép elrejtése</translation>
    </message>
    <message>
        <source>Show Map</source>
        <translation>Térkép megjelenítése</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Indítás</translation>
    </message>
    <message>
        <source>Switch Color Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HRM service not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageFieldDialog</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Időtartam</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Távolság</translation>
    </message>
    <message>
        <source>Pace</source>
        <translation type="unfinished">Lépés</translation>
    </message>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Field Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Magasság</translation>
    </message>
    <message>
        <source>Section Duration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Calories</name>
    <message>
        <source>Calories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kcal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Distance</name>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="unfinished">Távolság</translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration</name>
    <message>
        <source>Duration</source>
        <translation type="unfinished">Időtartam</translation>
    </message>
    <message>
        <source>h:m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Duration_Section</name>
    <message>
        <source>Duration (Section %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m:s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Elevation</name>
    <message>
        <source>Elevation</source>
        <translation type="unfinished">Magasság</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>RecordPageField_HeartRate</name>
    <message>
        <source>Heart Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reconnect HRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecordPageField_Pace</name>
    <message>
        <source>min/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pace ⌀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveDialog</name>
    <message>
        <source>Save track</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Megnevezés</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <source>Quit without saving</source>
        <translation>Kilépés mentés nélkül</translation>
    </message>
</context>
<context>
    <name>SettingsMenu</name>
    <message>
        <source>Settings</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <source>Heart rate device</source>
        <translation>Pulzusmérő</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Általános</translation>
    </message>
    <message>
        <source>Map</source>
        <translation>Térkép</translation>
    </message>
    <message>
        <source>Strava</source>
        <translation type="unfinished">Strava</translation>
    </message>
    <message>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>General settings</source>
        <translation>Általános beállítások</translation>
    </message>
    <message>
        <source>Enable autosave</source>
        <translation>Automatikus mentés engedélyezése</translation>
    </message>
    <message>
        <source>No need to enter activity name at end of activity.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SharedResources</name>
    <message>
        <source>Running</source>
        <translation>Futás</translation>
    </message>
    <message>
        <source>Roadbike</source>
        <translation>Országúti kerékpár</translation>
    </message>
    <message>
        <source>Mountainbike</source>
        <translation>Hegyikerékpár</translation>
    </message>
    <message>
        <source>Walking</source>
        <translation>Séta</translation>
    </message>
    <message>
        <source>Inline skating</source>
        <translation>Görkorcsolya</translation>
    </message>
    <message>
        <source>Skiing</source>
        <translation>Síelés</translation>
    </message>
    <message>
        <source>Hiking</source>
        <translation>Túrázás</translation>
    </message>
    <message>
        <source>All activities</source>
        <translation>Minden adat</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaAPI</name>
    <message>
        <source>Strava authentication error: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaActivityPage</name>
    <message>
        <source>Description:</source>
        <translation>Leírás:</translation>
    </message>
    <message>
        <source>Starting time:</source>
        <translation>Kezdés:</translation>
    </message>
    <message>
        <source>Duration:</source>
        <translation>Időtartam:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Távolság:</translation>
    </message>
    <message>
        <source>Speed max/⌀:</source>
        <translation>Max/átlag sebesség:</translation>
    </message>
    <message>
        <source>Kudos:</source>
        <translation>Kedvelések:</translation>
    </message>
    <message>
        <source>Comments:</source>
        <translation>Hozzászólások:</translation>
    </message>
    <message>
        <source>Elevation Gain:</source>
        <translation>Emelkedő szorzója:</translation>
    </message>
    <message>
        <source>Achievements/PRs:</source>
        <translation>Eredmények/PR-ok:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>km/h</source>
        <translation type="unfinished">km/h</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaComments</name>
    <message>
        <source>Comments</source>
        <translation>Hozzászólások</translation>
    </message>
</context>
<context>
    <name>StravaKudos</name>
    <message>
        <source>Kudos</source>
        <translation>Kedvelések</translation>
    </message>
</context>
<context>
    <name>StravaSegment</name>
    <message>
        <source>Duration:</source>
        <translation>Időtartam:</translation>
    </message>
    <message>
        <source>Distance:</source>
        <translation>Távolság:</translation>
    </message>
    <message>
        <source>Elevation Diff:</source>
        <translation>Szintkülönbség:</translation>
    </message>
    <message>
        <source>Climb Category:</source>
        <translation>Mászási kategória:</translation>
    </message>
    <message>
        <source>Best Effort:</source>
        <translation>Legjobb eredmény:</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>StravaSegments</name>
    <message>
        <source>Segments</source>
        <translation>Szegmensek</translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
</context>
<context>
    <name>StravaSettingsPage</name>
    <message>
        <source>Strava settings</source>
        <translation>Strava beállításai</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Kijelentkezés</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Bejelentkezés</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>Felhasználónév:</translation>
    </message>
    <message>
        <source>Country: </source>
        <translation>Ország:</translation>
    </message>
    <message>
        <source>not logged in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StravaUploadPage</name>
    <message>
        <source>Activity name for Strava</source>
        <translation>Strava-ban megjelenített edzésnév</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Leírás</translation>
    </message>
    <message>
        <source>Activity description for Strava</source>
        <translation>Strava-ban megjelenített edzés leírás</translation>
    </message>
    <message>
        <source>Commute</source>
        <translation>Ingázás</translation>
    </message>
    <message>
        <source>Uploading data...</source>
        <translation>Edzésadatok feltöltése...</translation>
    </message>
    <message>
        <source>Checking upload...</source>
        <translation>Feltöltés ellenőrzése...</translation>
    </message>
    <message>
        <source>An unknown error occurred</source>
        <translation>Ismeretlen hiba történt</translation>
    </message>
    <message>
        <source>Activity upload complete</source>
        <translation>Sikeres feltöltés</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Típus</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>NévMegnevezés</translation>
    </message>
    <message>
        <source>Strava Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GPX uploaded...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrackSlider</name>
    <message>
        <source>Pos:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>km</source>
        <translation type="unfinished">km</translation>
    </message>
    <message>
        <source>/km</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>m</source>
        <translation type="unfinished">m</translation>
    </message>
</context>
<context>
    <name>harbour-kuri</name>
    <message>
        <source>Connecting to HR device...</source>
        <translation>Csatlakozás a pulzusmérőhöz...</translation>
    </message>
    <message>
        <source>HR error: </source>
        <translation>Pulzusmérő hiba: </translation>
    </message>
</context>
</TS>
