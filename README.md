# Kuri

Kuri is a sport tracking application for Sailfish OS. Kuri means "run" in Esperanto. Funnily it also means "discipline" in Finnish.
Features are:
- recording tracks
- view recorded track on a map and show statistics
- export track as GPX file
- connecting to bluetooth heart rate device
- uploading to Strava
- and more...

This application originates from a fork of Laufhelden by Jens Drescher: https://github.com/jdrescher2006/Laufhelden

# Credits

- Jens Drescher and all the contributors of Laufhelden

Workout icons are from here https://icons8.com (license: https://creativecommons.org/licenses/by-nd/3.0/)

# License

License: GNU General Public License (GNU GPLv3)

# Icon Color Palette

- `#c02222` red
- `#c06622` orange
- `#c0a622` yellow
- `#22c071` green
- `#228bc0` blue
- `#7e22c0` violet
- `#c022c0` purple
- `#707b80` silver
- `#333333` pebble

# Development

With the introduction of Rust, the Docker based SDK is required to build Kuri.

## Basics

`sfdk` is used to build Kuri, see also [sfdk](https://sailfishos.org/wiki/Tutorial_-_Building_packages_-_advanced_techniques)

- create alias for `sfdk`
  - `alias sfdk=~/SailfishOS/bin/sfdk`
- list targets
  - `sfdk tools list`
- set target from within build directory
  - `sfdk config target=SailfishOS-4.4.0.58-i486`
- build
  - `sfdk build -j 4 ..`
  - rpm is in `RPM` folder
- check for compatibility with store
  - `sfdk check RPMS/harbour-kuri.rpm`
- list devices
  - `sfdk device list`
- set emulator
  - `sfdk config device="Sailfish OS Emulator 4.4.0.58"`
- deploy to emulator
  - `sfdk deploy --sdk`
- run in emulator
  - `sfdk device exec harbour-kuri`
- enter emulator environment
  - `sfdk device exec`
- access build environment
  - `sfdk build-shell ls`
- enter build environment
  - `sfdk build-shell`

## Emulator Setup

In order to run Kuri in the emulator, `mapbox-gl.rpm` must be installed. The easiest way to do this is by the `chum` repository.
Get the [chum rpm](https://chumrpm.netlify.app/) download link for the specific emulator, e.g.
`https://repo.sailfishos.org/obs/sailfishos:/chum//4.4.0.58_i486//i486/sailfishos-chum-gui-0.6.0-1.12.1.jolla.i486.rpm`.

Instructions to setup emulator:

```console
alias sfdk=~/SailfishOS/bin/sfdk
sfdk config target=SailfishOS-4.4.0.58-i486
sfdk config device="Sailfish OS Emulator 4.4.0.58"
sfdk device exec curl -O https://repo.sailfishos.org/obs/sailfishos:/chum//4.4.0.58_i486//i486/sailfishos-chum-gui-0.6.0-1.12.1.jolla.i486.rpm
sfdk device exec sudo pkcon install-local -y sailfishos-chum-gui-0.6.0-1.12.1.jolla.i486.rpm
sfdk device exec sudo pkcon refresh
```

Start `chum` in the emulator and uncheck the `Show applications only` option in the settings. Then navigate to `Packages` and search and install `Mapbox GL QML Plugin`.

To make Strava work in the emulator, a browser needs to be installed, e.g. by `sfdk device exec sudo pkcon install -y sailfish-browser`. If the browser does not start via the GUI just try to start from the terminal and then login to Strava.

When deployed to `Sailfish OS Emulator 4.5.0.18` the removed Qt Connectivity package needs to be installed from `https://openrepos.net/content/abranson/qtconnectivity-bluetooth`.
```console
sfdk device exec curl -O https://openrepos.net/sites/default/files/packages/4801/qt5-qtconnectivity-qtbluetooth-5.6.2git3-1.1.2.jolla_.i486.rpm
sfdk device exec sudo pkcon install-local -y qt5-qtconnectivity-qtbluetooth-5.6.2git3-1.1.2.jolla_.i486.rpm
```

## Build and Test

Instructions to build Kuri:

```console
$ mkdir -p build/x86
$ cd build/x86
$ alias sfdk=~/SailfishOS/bin/sfdk
$ sfdk config target=SailfishOS-4.4.0.58-i486
$ sfdk build -j 4 ..
$ sfdk check RPMS/harbour-kuri.rpm
```

Instructions to run Kuri on the emulator:

```console
$ sfdk config device="Sailfish OS Emulator 4.4.0.58"
$ sfdk deploy --sdk
$ sfdk device exec harbour-kuri
```
