/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>

#include <memory>

namespace kuri { namespace ffi {
class EventVoid : public QObject {
    Q_OBJECT
public:
    EventVoid() = default;

    EventVoid(const EventVoid&) = delete;
    EventVoid(EventVoid&&)      = delete;

    EventVoid& operator=(const EventVoid&) = delete;
    EventVoid& operator=(EventVoid&&) = delete;

    inline void notify() { emit signal(); }

    Q_SIGNAL void signal();
};

inline std::unique_ptr<EventVoid> new_event_void() {
    return std::make_unique<EventVoid>();
}

#define KURI_CREATE_EVENT_CLASS(EVENT_CLASS_NAME, EVENT_TYPE, EVENT_CLASS_FACTORY) \
    class EVENT_CLASS_NAME : public QObject {                                      \
        Q_OBJECT                                                                   \
    public:                                                                        \
        EVENT_CLASS_NAME() = default;                                              \
                                                                                   \
        EVENT_CLASS_NAME(const EVENT_CLASS_NAME&) = delete;                        \
        EVENT_CLASS_NAME(EVENT_CLASS_NAME&&)      = delete;                        \
                                                                                   \
        EVENT_CLASS_NAME& operator=(const EVENT_CLASS_NAME&) = delete;             \
        EVENT_CLASS_NAME& operator=(EVENT_CLASS_NAME&&) = delete;                  \
                                                                                   \
        inline void notify(EVENT_TYPE value) { emit signal(value); }               \
                                                                                   \
        Q_SIGNAL void signal(EVENT_TYPE);                                          \
    };                                                                             \
                                                                                   \
    inline std::unique_ptr<EVENT_CLASS_NAME> EVENT_CLASS_FACTORY() { return std::make_unique<EVENT_CLASS_NAME>(); }


KURI_CREATE_EVENT_CLASS(EventBool, bool, new_event_bool)

KURI_CREATE_EVENT_CLASS(EventI8, int8_t, new_event_i8)
KURI_CREATE_EVENT_CLASS(EventI16, int16_t, new_event_i16)
KURI_CREATE_EVENT_CLASS(EventI32, int32_t, new_event_i32)
KURI_CREATE_EVENT_CLASS(EventI64, int64_t, new_event_i64)

KURI_CREATE_EVENT_CLASS(EventU8, uint8_t, new_event_u8)
KURI_CREATE_EVENT_CLASS(EventU16, uint16_t, new_event_u16)
KURI_CREATE_EVENT_CLASS(EventU32, uint32_t, new_event_u32)
KURI_CREATE_EVENT_CLASS(EventU64, uint64_t, new_event_u64)

KURI_CREATE_EVENT_CLASS(EventF32, float, new_event_f32)
KURI_CREATE_EVENT_CLASS(EventF64, double, new_event_f64)

}} // namespace kuri::ffi
