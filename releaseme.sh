#! /bin/bash

set -e
shopt -s expand_aliases

# colors
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
COLOR_OFF='\033[0m'

GIT_TOPLEVEL=$(git rev-parse --show-toplevel)
VERSION="0.0.0"

X86_DEVICE="Sailfish OS Emulator 4.4.0.58"
X86_TARGET=SailfishOS-4.4.0.58-i486
ARMv7_TARGET=SailfishOS-4.4.0.58-armv7hl
AARCH64_TARGET=SailfishOS-4.4.0.58-aarch64

DO_BUILD_ONLY=false
DO_PREPARATORY_WORK=false
DO_RELEASE=false

OPTION_COUNTER=0
while (( "$#" )); do
    ((OPTION_COUNTER+=1))
    if [[ $OPTION_COUNTER -gt 1 ]]
    then
        echo -e "${RED}Error:${COLOR_OFF} Too many arguments specified! Try '--help' for more information."
        exit -1
    fi

    case $1 in
        -h|--help)
            echo "Kuri Release Script"
            echo ""
            echo "Usage: releaseme.sh [option]"
            echo "Only one option at a time is allowed!"
            echo "Options:"
            echo "    -b, --build-only                  Creates the RPMs only"
            echo "    -h, --help                        Prints this help"
            echo "    -p, --preparatory-work <VERSION>  Performs release preparatory work like"
            echo "                                      setting the specified VERSION,"
            echo "                                      building and running on the emulator"
            echo "    -r, --release                     Creates a git commit, git tag and the RPMs"
            echo ""
            echo "Example:"
            echo "    releaseme.sh --release 0.4.1"
            exit 0
            ;;
        -b|--build-only)
            DO_BUILD_ONLY=true
            shift 1
            ;;
        -p|--preparatory-work)
            if [[ $# -ne 2 ]]
            then
                echo -e "${RED}Error:${COLOR_OFF} No parameter specified! Try '--help' for more information."
            fi
            VERSION="$2"

            DO_PREPARATORY_WORK=true
            shift 2
            ;;
        -r|--release)
            DO_RELEASE=true
            shift 1
            ;;
        *)
            echo "Invalid argument '$1'. Try '--help' for options."
            exit -1
            ;;
    esac
done

alias sfdk=~/SailfishOS/bin/sfdk

function build_rpm() {
    cd $GIT_TOPLEVEL

    mkdir -p build/$1 || exit 1
    rm -rf build/$1/*
    cd build/$1

    sfdk config target=$2
    sfdk build -j 4 ../..
}

function build_armv7_rpm() {
    build_rpm armv7 $ARMv7_TARGET
}

function build_aarch64_rpm() {
    build_rpm aarch64 $AARCH64_TARGET
}

function build_all_arm_rpm() {
    build_armv7_rpm
    build_aarch64_rpm
}

if [[ $DO_PREPARATORY_WORK == true ]]
then
    cd $GIT_TOPLEVEL

    # set version
    sed -i "s/set(KURI_VERSION \(.*\))/set(KURI_VERSION ${VERSION})/g" CMakeLists.txt
    sed -i "s/Version: \(.*\)/Version: ${VERSION}/g" rpm/harbour-kuri.yaml

    while true; do
        read -p "Edit 'rpm/harbour-kuri.changes'? (Y/N) [default=Y]: " yn
        case $yn in
            [Yy]* | "")
                editor rpm/harbour-kuri.changes
                break
                ;;
            [Nn]*)
                break
                ;;
            *) echo -e "${YELLOW}Please use either 'Y' or 'N'.${COLOR_OFF}";;
        esac
    done

    while true; do
        echo "Did the minimal supported SFOS version change?"
        read -p "Edit 'rpm/harbour-kuri.yaml' and set 'sailfish-version'? (Y/N) [default=N]: " yn
        case $yn in
            [Yy]*)
                editor rpm/harbour-kuri.yaml
                break
                ;;
            [Nn]* | "")
                break
                ;;
            *) echo -e "${YELLOW}Please use either 'Y' or 'N'.${COLOR_OFF}";;
        esac
    done

    # build for minimal supported SFOS version on x86
    build_rpm x86 $X86_TARGET

    sfdk config device="$X86_DEVICE"
    sfdk deploy --sdk
    sfdk device exec /usr/bin/harbour-kuri

    exit 0
fi

if [[ $DO_BUILD_ONLY == true ]]
then
    build_all_arm_rpm
    echo -e "${GREEN}Success!${COLOR_OFF} RPMs are available!"

    exit 0
fi

if [[ $DO_RELEASE == true ]]
then
    cd $GIT_TOPLEVEL

    # try to get version from rpm/harbour-kuri.yaml
    VERSION=$(grep Version rpm/harbour-kuri.yaml | sed "s/Version: //g")
    GIT_TAG="v${VERSION}"

    git add .
    git commit -m"Bump version to ${VERSION}"
    git tag ${GIT_TAG}

    build_all_arm_rpm

    while true; do
        echo "Upstream release?"
        read -p "Perform 'git push origin' and 'git push origin tag ${GIT_TAG}'? (Y/N): " yn
        case $yn in
            [Yy]*)
                echo "Push commits:"
                git push origin
                echo "Push tag:"
                git push origin tag ${GIT_TAG}
                echo -e "${GREEN}Success!${COLOR_OFF} Please upload the RPMs to openrepos!"
                break
                ;;
            [Nn]*)
                echo -e "${GREEN}Success!${COLOR_OFF} Please push the commit and the git tag upstream to finalize the release and upload the RPMs to openrepos!"
                break
                ;;
            *) echo -e "${YELLOW}Please use either 'Y' or 'N'.${COLOR_OFF}";;
        esac
    done

    exit 0
fi
