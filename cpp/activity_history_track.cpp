/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_history_track.hpp"

#include "kuri-rs/ffi/events.h"

namespace kuri {

ActivityHistoryTrack::ActivityHistoryTrack(QObject* parent)
    : QObject(parent) {}

void ActivityHistoryTrack::setActivityHistoryRs(rs::ActivityHistory& history) {
    m_history = &history;
}

void ActivityHistoryTrack::load(QString activityId) {
    const auto id = activityId.toUtf8();
    m_history->load_track({id.data(), static_cast<size_t>(id.size())});
}

void ActivityHistoryTrack::discard() {
    m_history->discard_track();

    totalPointCountSet(0);
    chartDataPointRangeEndSet(0);

    currentElevationSet(0);
    currentDurationSet(0);
    currentHeartRateSet(0);
    currentDistanceSet(0);
    currentPaceSet(0);
}

void ActivityHistoryTrack::emitTrackPoints() {
    auto connectLateAvailabity    = [this] { this->connect(this, &ActivityHistoryTrack::availableChanged, this, &ActivityHistoryTrack::emitTrackPoints); };
    auto disconnectLateAvailabity = [this] { this->disconnect(this, &ActivityHistoryTrack::availableChanged, this, &ActivityHistoryTrack::emitTrackPoints); };

    if (!m_available) {
        disconnectLateAvailabity(); // just to prevent multiple signal connections
        connectLateAvailabity();
        return;
    }

    auto* track = m_history->track();
    if (track) {
        int64_t        lastTimestamp {0};
        QGeoCoordinate lastCoordinate;
        int64_t        lastTimestampWithValidCoordinate {0};

        const auto sectionCount    = track->section_count();
        const auto totalPointCount = track->total_point_count();

        // BEGIN map related
        auto emitMapMarkerPoints = [&](const auto sectionCounter, const auto m) {
            if (sectionCounter == 0) {
                emit newTrackPoint({TrackPoint::Tag::Start, {m.latitude, m.longitude, m.altitude}});
                lastTimestampWithValidCoordinate = 0;
            } else if (m.timestamp == lastTimestamp) {
                emit newTrackPoint({TrackPoint::Tag::SectionCrossing, {m.latitude, m.longitude, m.altitude}});

            } else {
                emit newTrackPoint({TrackPoint::Tag::Pause, lastCoordinate});
                emit newTrackPoint({TrackPoint::Tag::Resume, {m.latitude, m.longitude, m.altitude}});
                lastTimestampWithValidCoordinate = 0;
            }
        };
        // END map related

        // BEGIN chart related
        quint64 chartPoints {MAX_CHART_POINTS};
        if (totalPointCount < chartPoints) { chartPoints = totalPointCount; }
        auto trackPointsPerChartInterval = totalPointCount / chartPoints;

        QVector<float>    speedInterval;
        QVector<float>    elevationInterval;
        QVector<uint16_t> heartRateInterval;

        speedInterval.reserve(trackPointsPerChartInterval);
        elevationInterval.reserve(trackPointsPerChartInterval);
        heartRateInterval.reserve(trackPointsPerChartInterval);

        quint64 chartPointCounter {0};
        quint64 remainingPointsInInterval {trackPointsPerChartInterval};

        auto addToChartCache = [&](auto& intervalCache, auto& chartCache) {
            const auto pointCount = intervalCache.size();
            if (pointCount > 0) {
                std::sort(intervalCache.begin(), intervalCache.end());
                const qreal median = intervalCache[pointCount / 2];
                const qreal min    = intervalCache.front();
                const qreal max    = intervalCache.back();
                chartCache.push_back(QVariant::fromValue(ChartDataPoint {chartPointCounter, median, min, max}));
                intervalCache.clear();
            }
        };
        // END chart related

        prepareCache(sectionCount, totalPointCount);

        constexpr int64_t TO_SECS {1000};

        for (quint64 s = 0; s < sectionCount; ++s) {
            const auto sectionPointCount = track->section_point_count(s);

            if (sectionPointCount > 0) {
                auto m = m_history->track()->measurement_at(s, 0);

                m_cache.sectionPointCounts.push_back(sectionPointCount);
                m_cache.sectionInfo.push_back({m.timestamp / TO_SECS});
                auto& sectionInfo = m_cache.sectionInfo.back();

                emitMapMarkerPoints(s, m);

                // emit all points in the section and populate cache
                for (quint64 i = 0; i < sectionPointCount; ++i) {
                    const auto m  = m_history->track()->measurement_at(s, i);
                    lastTimestamp = m.timestamp;

                    if (m.position_valid) {
                        auto currentCoordinate = QGeoCoordinate(m.latitude, m.longitude, m.altitude);
                        if (lastTimestampWithValidCoordinate != 0) {
                            // TODO in theory this should deliver the same results as statistics::great_circle_distance
                            // but to prevent deviations the same calculation method should be used
                            auto distance = currentCoordinate.distanceTo(lastCoordinate) * 1000.;
                            m_cache.distance.push_back(m_cache.distance.back() + distance);
                            sectionInfo.distanceInSection += distance;

                            // BEGIN map related
                            const auto       timeDiffMilliseconds            = static_cast<qreal>(m.timestamp - lastTimestampWithValidCoordinate);
                            const auto       paceInMillisecondsPerMillimeter = timeDiffMilliseconds / distance;
                            constexpr double TO_SECONDS_PER_KM               = 1000.;
                            m_cache.pace.push_back(paceInMillisecondsPerMillimeter * TO_SECONDS_PER_KM);
                            // END map related

                            // BEGIN chart related
                            constexpr double TO_KM_PER_HOUR = 3600000. / 1000000.;
                            speedInterval.push_back(TO_KM_PER_HOUR / paceInMillisecondsPerMillimeter);
                            // END chart related
                        } else if (m_cache.distance.empty()) {
                            m_cache.distance.push_back(0);
                            m_cache.pace.push_back(0.);
                        } else {
                            m_cache.distance.push_back(m_cache.distance.back());
                            m_cache.pace.push_back(m_cache.pace.back());
                        }

                        // BEGIN map related
                        emit newTrackPoint({TrackPoint::Tag::Track, currentCoordinate});
                        lastCoordinate                   = currentCoordinate;
                        lastTimestampWithValidCoordinate = m.timestamp;
                        // END map related

                        elevationInterval.push_back(m.altitude);
                    }

                    // BEGIN chart related
                    if (m.heart_rate > 0) { heartRateInterval.push_back(m.heart_rate); }

                    if (--remainingPointsInInterval == 0) {
                        remainingPointsInInterval = trackPointsPerChartInterval;
                        addToChartCache(speedInterval, m_cache.chartSpeedData);
                        addToChartCache(elevationInterval, m_cache.chartElevationData);
                        addToChartCache(heartRateInterval, m_cache.chartHeartRateData);
                        ++chartPointCounter;
                    }
                    // END chart related
                }

                sectionInfo.durationInSection = lastTimestamp / TO_SECS - sectionInfo.sectionStartTime;
            }
        }

        // BEGIN map related
        emit newTrackPoint({TrackPoint::Tag::Stop, lastCoordinate});
        totalPointCountSet(totalPointCount);
        currentIndexSet(0);
        // END map related

        // BEGIN chart related
        chartDataPointRangeEndSet(chartPointCounter > 0 ? chartPointCounter - 1 : 0);
        emit chartDataReady();
        // END chart related

        // TODO simple moving average for data in m_cache.distance and m_cache.pace
    }

    disconnectLateAvailabity();
}

void ActivityHistoryTrack::emitChartDataReady() {
    // if total point count is zero, emitTrackPoints will take care of emitting the signal
    // this is just for late connection joiners
    if (m_totalPointCount != 0) { emit chartDataReady(); }
}

quint64 ActivityHistoryTrack::currentIndex() const {
    return m_currentIndex;
}

void ActivityHistoryTrack::currentIndexSet(const quint64 index) {
    if (index >= m_totalPointCount || index == m_currentIndex) { return; }

    constexpr int64_t TO_SECS {1000};

    uint64_t section                    = 0;
    uint64_t indexInSection             = index;
    uint64_t accumulatedSectionDuration = 0;
    for (auto sectionPointCount : m_cache.sectionPointCounts) {
        if (indexInSection >= sectionPointCount) {
            indexInSection -= sectionPointCount;
            accumulatedSectionDuration += m_cache.sectionInfo[section].durationInSection;
            ++section;
        } else {
            auto m = m_history->track()->measurement_at(section, indexInSection);

            currentPositionSet(QGeoCoordinate(m.latitude, m.longitude, m.altitude));
            currentElevationSet(m.altitude);
            currentDurationSet(m.timestamp / TO_SECS - m_cache.sectionInfo[section].sectionStartTime + accumulatedSectionDuration);
            currentHeartRateSet(m.heart_rate);
            currentDistanceSet(m_cache.distance[index]);
            currentPaceSet(m_cache.pace[index]);

            break;
        }
    }

    m_currentIndex = index;
    emit currentIndexChanged();
}

void ActivityHistoryTrack::prepareCache(size_t numberOfSections, size_t totalNumberOfPoints) {
    m_cache.sectionPointCounts.clear();
    m_cache.sectionInfo.clear();
    m_cache.distance.clear();
    m_cache.pace.clear();

    m_cache.sectionPointCounts.reserve(numberOfSections);
    m_cache.sectionInfo.reserve(numberOfSections);
    m_cache.distance.reserve(totalNumberOfPoints);
    m_cache.pace.reserve(totalNumberOfPoints);

    m_cache.chartSpeedData.clear();
    m_cache.chartElevationData.clear();
    m_cache.chartHeartRateData.clear();

    m_cache.chartSpeedData.reserve(MAX_CHART_POINTS);
    m_cache.chartElevationData.reserve(MAX_CHART_POINTS);
    m_cache.chartHeartRateData.reserve(MAX_CHART_POINTS);

    currentElevationSet(0);
    currentDurationSet(0);
    currentHeartRateSet(0);
    currentDistanceSet(0);
    currentPaceSet(0);
}

void ActivityHistoryTrack::trackToGpx() {
    m_history->track_to_gpx();
}

QString ActivityHistoryTrack::getGpxTrack() {
    auto gpxTrack = m_history->get_gpx_track();
    return QString::fromUtf8(gpxTrack.data(), gpxTrack.length());
}

} // namespace kuri
