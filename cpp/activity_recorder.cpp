/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_recorder.hpp"

#include "formatter.hpp"

#include "rust/cxx.h"

#include <QDateTime>
#include <QDebug>

namespace kuri {

ActivityRecorder::ActivityRecorder()
    : QObject(nullptr) {
    m_tickTimer.setSingleShot(false);

    connect(&m_tickTimer, &QTimer::timeout, [&] {
        auto millisecondsToNextTick = this->millisecondsToNextTick();
        m_recorder.tick();
        // prevent double activation due to jitter in the wake-up
        if (millisecondsToNextTick < OFFSET_IN_MILLISECONDS) { millisecondsToNextTick += INTERVAL_IN_MILLISECONDS; };
        m_tickTimer.start(millisecondsToNextTick);
    });
}

uint16_t ActivityRecorder::millisecondsToNextTick() const {
    auto currentMilliseconds = static_cast<uint16_t>(QDateTime::currentDateTimeUtc().time().msec());

    return (currentMilliseconds <= OFFSET_IN_MILLISECONDS) ? OFFSET_IN_MILLISECONDS - currentMilliseconds
                                                           : (INTERVAL_IN_MILLISECONDS + OFFSET_IN_MILLISECONDS) - currentMilliseconds;
}

rs::ActivityRecorder ActivityRecorder::initActivityRecorderRs() {
    auto  events     = rs::new_activity_recorder_events();
    auto& syncEvents = events.sync_events;
    connect(syncEvents.new_activity_available.get(), &ffi::EventVoid::signal, [this] {
        auto activityId = m_recorder.last_stored_activity_id();
        if (!activityId.empty()) { emit this->newActivityAvailable(QString::fromUtf8(activityId.data(), activityId.length())); }
    });

    auto& asyncEvents = events.async_events;
    connect(asyncEvents.new_artifacts.get(), &ffi::EventVoid::signal, this, &ActivityRecorder::processArtifacts, Qt::QueuedConnection);
    connect(asyncEvents.recording.get(), &ffi::EventBool::signal, this, &ActivityRecorder::recordingSet, Qt::QueuedConnection);
    connect(asyncEvents.pausing.get(), &ffi::EventBool::signal, this, &ActivityRecorder::pausingSet, Qt::QueuedConnection);
    connect(asyncEvents.duration.get(), &ffi::EventU64::signal, this, &ActivityRecorder::durationSet, Qt::QueuedConnection);
    connect(asyncEvents.sectionCount.get(), &ffi::EventU64::signal, this, &ActivityRecorder::sectionCountSet, Qt::QueuedConnection);
    connect(asyncEvents.durationInSection.get(), &ffi::EventU64::signal, this, &ActivityRecorder::durationInSectionSet, Qt::QueuedConnection);
    connect(asyncEvents.heartRate.get(), &ffi::EventU16::signal, this, &ActivityRecorder::heartRateSet, Qt::QueuedConnection);
    connect(asyncEvents.heartRateAverage.get(), &ffi::EventU16::signal, this, &ActivityRecorder::heartRateAverageSet, Qt::QueuedConnection);
    connect(asyncEvents.distance.get(), &ffi::EventU64::signal, this, &ActivityRecorder::distanceSet, Qt::QueuedConnection);
    connect(asyncEvents.distance60s.get(), &ffi::EventU64::signal, this, &ActivityRecorder::distance60sSet, Qt::QueuedConnection);
    connect(asyncEvents.elevation.get(), &ffi::EventI64::signal, this, &ActivityRecorder::elevationSet, Qt::QueuedConnection);
    connect(asyncEvents.elevationUp.get(), &ffi::EventU64::signal, this, &ActivityRecorder::elevationUpSet, Qt::QueuedConnection);
    connect(asyncEvents.elevationDown.get(), &ffi::EventU64::signal, this, &ActivityRecorder::elevationDownSet, Qt::QueuedConnection);
    connect(asyncEvents.speedAverage.get(), &ffi::EventF64::signal, this, &ActivityRecorder::speedAverageSet, Qt::QueuedConnection);
    connect(asyncEvents.paceAverage.get(), &ffi::EventF64::signal, this, &ActivityRecorder::paceAverageSet, Qt::QueuedConnection);
    return rs::new_activity_recorder(std::move(events));
}

void ActivityRecorder::processArtifacts() {
    m_recorder.process_artifacts();
}

ActivityRecorder::~ActivityRecorder() {
    m_recorder.shutdown();
}

void ActivityRecorder::positionUpdate(const QGeoPositionInfo& update) {
    // the position is updated every second; if there is no position update for more than a second, let the tickTimer trigger a timeout
    if (m_recordingStartPending || m_recording) { m_tickTimer.start(INTERVAL_IN_MILLISECONDS + OFFSET_IN_MILLISECONDS); }

    auto milliSecondsSinceEpoch = update.timestamp().toUTC().toMSecsSinceEpoch();

    rs::PositionInfo positionInfo;
    positionInfo.timestamp = milliSecondsSinceEpoch;
    m_latestCoordinate     = update.coordinate();
    positionInfo.latitude  = m_latestCoordinate.latitude();
    positionInfo.longitude = m_latestCoordinate.longitude();
    positionInfo.altitude  = static_cast<float>(m_latestCoordinate.altitude());
    if (update.hasAttribute(QGeoPositionInfo::HorizontalAccuracy)) {
        positionInfo.horizontal_accuracy = static_cast<uint16_t>(update.attribute(QGeoPositionInfo::HorizontalAccuracy));
    } else {
        positionInfo.horizontal_accuracy = 0;
    }
    if (update.hasAttribute(QGeoPositionInfo::VerticalAccuracy)) {
        positionInfo.vertical_accuracy = static_cast<uint16_t>(update.attribute(QGeoPositionInfo::VerticalAccuracy));
    } else {
        positionInfo.vertical_accuracy = 0;
    }
    m_recorder.position_update(positionInfo);

    if (m_recording && !m_pausing) { emit newTrackPoint({TrackPoint::Tag::Track, m_latestCoordinate}); }
}

TrackPoint ActivityRecorder::latestTrackPoint(TrackPoint::Tag tag) {
    return TrackPoint {tag, m_latestCoordinate};
}

void ActivityRecorder::heartRateUpdate(const quint16 update) {
    m_recorder.heart_rate_update(update);
}

void ActivityRecorder::start() {
    m_recordingStartPending = true;
    // even if there is less than OFFSET_IN_MILLISECONDS time to the next tick,
    // either there are already position data available and those will be used
    // or there are none and most probably none will arrive withing the next second
    auto millisecondsToNextTick = this->millisecondsToNextTick();
    m_recorder.start();
    m_tickTimer.start(millisecondsToNextTick);
}

void ActivityRecorder::stop() {
    m_recordingStartPending = false;
    m_tickTimer.stop();
    m_recorder.stop();
}

void ActivityRecorder::pause() {
    m_recorder.pause();
}

void ActivityRecorder::resume() {
    m_recorder.resume();
}

void ActivityRecorder::nextSection() {
    m_recorder.next_section();
}

void ActivityRecorder::setActivityType(QString activityType) {
    const auto type = activityType.toUtf8();
    m_recorder.set_activity_type({type.data(), static_cast<size_t>(type.size())});
}

void ActivityRecorder::setActivityName(QString activityName) {
    const auto name = activityName.toUtf8();
    m_recorder.set_activity_name({name.data(), static_cast<size_t>(name.size())});
}

void ActivityRecorder::setActivityDescription(QString activityDesc) {
    const auto desc = activityDesc.toUtf8();
    m_recorder.set_activity_description({desc.data(), static_cast<size_t>(desc.size())});
}

void ActivityRecorder::saveRecording() {
    m_recorder.save_recording();
}

void ActivityRecorder::discardRecording() {
    m_recorder.discard_recording();
}

bool ActivityRecorder::recording() const {
    return m_recording;
}

void ActivityRecorder::recordingSet(bool value) {
    m_recordingStartPending = false;
    m_recording             = value;
    emit recordingChanged();
}

QString ActivityRecorder::durationAsHoursMinutesSeconds() const {
    return Formatter::secondsToHoursMinutesSeconds(m_duration);
}

QString ActivityRecorder::durationInSectionAsMinutesSeconds() const {
    return Formatter::secondsToMinutesSeconds(m_durationInSection);
}

QString ActivityRecorder::paceAverageInMinutesPerKm() const {
    return m_paceAverage > 0 ? Formatter::secondsToMinutesSeconds(m_paceAverage) : "---";
}

} // namespace kuri
