/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "activity_history_model.hpp"
#include "activity_history_track.hpp"
#include "macros.hpp"
#include "settings.hpp"

#include "kuri-rs/ffi/events.h"
#include "kuri-rs/rust/activity_history.rs.h"

#include <QHash>
#include <QObject>
#include <QSortFilterProxyModel>

namespace kuri {

class ActivityHistory : public QSortFilterProxyModel {
    Q_OBJECT

public:
    ActivityHistory(Settings& settings, QObject* parent = nullptr);
    ~ActivityHistory();

    ActivityHistory(const ActivityHistory&) = delete;
    ActivityHistory(ActivityHistory&&)      = delete;

    ActivityHistory& operator=(const ActivityHistory&) = delete;
    ActivityHistory& operator=(ActivityHistory&&)      = delete;

    ActivityHistoryTrack& track();

    void loadNewActivity(QString activityId);

    Q_INVOKABLE void updateActivitySummary(QString activityId, int row, QString oldActivityType, QString newActivityType);
    Q_INVOKABLE void saveActivityChanges(QString activityId, int row);
    Q_INVOKABLE void deleteActivity(QString activityId, int row);

    Q_INVOKABLE void importActivities();

    Q_INVOKABLE void exportActivities();
    Q_INVOKABLE void exportActivity(QString activityId);

    void             migrateActivitiesFromLegacyPath();
    Q_INVOKABLE void redoActivityMigrationFromLegacyPath();

    Q_INVOKABLE QString intervalSummaryName(quint64 intervalId) const;
    Q_INVOKABLE quint64 intervalSummaryDistance(quint64 intervalId) const;
    Q_INVOKABLE quint64 intervalSummaryDuration(quint64 intervalId) const;

    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, activityImportOngoing, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, activityExportOngoing, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, activityMigrationOngoing, false);

    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, activitiesLoading, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, activityTotalCount, 0);

    KURI_RW_PROPERTY(QString, activityTypeFilter, "allactivities");
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, hasVariousActivityTypes, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, activityCountFromFilter, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, activityDurationFromFilter, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, activityDistanceFromFilter, 0);

signals:
    void activityChanged(quint64 index);

private:
    rs::ActivityHistory initActivityHistoryRs();

    void processArtifacts();
    void propagateActivitySummaryChanges();

    void setActivityMigrationComplete(bool ongoing);

    int mapToSourceRow(int row);

private:
    Settings&            m_settings;
    ActivityHistoryTrack m_track; // must be placed befor m_history in order connect signals
    rs::ActivityHistory  m_history {initActivityHistoryRs()};
    ActivityHistoryModel m_model {m_history};

    mutable QHash<quint64, QString> m_intervalSummaryNameCache;
};

} // namespace kuri
