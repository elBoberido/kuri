/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "kuri-rs/rust/activity_history.rs.h"

#include <QAbstractListModel>
#include <QDateTime>
#include <QList>
#include <QObject>

namespace kuri {

quint64               flattenIntervalSummaryId(rs::IntervalSummaryId id);
rs::IntervalSummaryId unflattenIntervalSummaryId(quint64 id);

class ActivityHistoryModel : public QAbstractListModel {
    Q_OBJECT

public:
    enum class Roles {
        StartTime = Qt::UserRole + 1,
        ActivityId,
        ActivityType,
        Name,
        Description,
        IntervalSummaryId,
        DateTimeShort,
        DateTimeLong,
        Duration,
        Pause,
        Distance,
        ElevationUp,
        ElevationDown,
        SpeedAverage,
        PaceAverage,
        HeartRateAverage,
    };

    ActivityHistoryModel(rs::ActivityHistory& history, QObject* parent = nullptr);

    void updateActivitySummary(QString activityId, int row, QString oldActivityType, QString newActivityType);
    void saveActivityChanges(QString activityId, int row);
    void removeActivity(QString activityId, int row);

    void updateSincleActivityFromCache(quint64 row);
    void updateActivityCache();
    void forceUpdateActivityCache();

    int rowCount(const QModelIndex& parent = QModelIndex {}) const override;

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    bool setData(const QModelIndex& index, const QVariant& value, int role) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    bool rowInRange(int row) const;

    void fillCache(int fromIndex, int toIndexExcluding);

    // pointer to activity and cache for data with expensive conversion or calculations
    struct ActivityData {
        ActivityData(rs::Activity& activity);
        void refresh() const;

        rs::Activity*     activity {nullptr};
        mutable bool      needsRefresh {true};
        mutable QString   activityId;
        mutable QString   activityType;
        mutable QString   name;
        mutable QString   description;
        mutable QDateTime startTime;
        mutable QString   startTimeShort;
        mutable QString   startTimeLong;
        mutable qreal     speedAverage;
        mutable qreal     paceAverage;
    };

private:
    rs::ActivityHistory& m_history;
    QList<ActivityData>  m_cache;
};

} // namespace kuri
