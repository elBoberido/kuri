/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "kuri-rs/rust/settings.rs.h"
#include "macros.hpp"
#include "rust/cxx.h"

#include <QObject>

namespace kuri {

class Settings : public QObject {
    Q_OBJECT

private:
    // these member must be declared/initialized before KURI_*_PROPERTY
    bool useCachedValues {false};

    rust::Box<kuri::rs::Settings> m_settings {kuri::rs::load_settings()};

public:
    explicit Settings(QObject* parent = nullptr);
    ~Settings();

    // history specific settings
    KURI_RW_PROPERTY(QString, activityTypeFilter, activityTypeFilter());

    // recording specific settings
    KURI_RW_PROPERTY(QString, activityType, activityType());
    KURI_RW_PROPERTY(qreal, positionAccuracyThreshold, positionAccuracyThreshold());
    KURI_RW_PROPERTY(bool, showMapRecordPage, showMapRecordPage());
    KURI_RW_PROPERTY(bool, disableScreenBlanking, disableScreenBlanking());
    KURI_RW_PROPERTY(quint64, displayMode, displayMode());
    KURI_RW_PROPERTY(bool, enableAutosave, enableAutosave());

    // map related settings
    KURI_RW_PROPERTY(quint64, mapCenterMode, mapCenterMode());
    KURI_RW_PROPERTY(QString, mapStyle, mapStyle());
    KURI_RW_PROPERTY(quint64, mapCache, mapCache());

    // HRM related settings
    KURI_RW_PROPERTY(bool, useHRMdevice, useHRMdevice());
    KURI_RW_PROPERTY(bool, useHRMservice, useHRMservice());
    KURI_RW_PROPERTY(QString, hrmDeviceName, hrmDeviceName());
    KURI_RW_PROPERTY(QString, hrmDeviceAddress, hrmDeviceAddress());
    KURI_RW_PROPERTY(quint64, bluetoothType, bluetoothType());

    // import related settings
    KURI_RO_PROPERTY(QString, exportDestinationFolder, exportDestinationFolder());
    KURI_RO_PROPERTY(QString, importSourceFolder, importSourceFolder());
    KURI_RW_PROPERTY(bool, overrideImportedActivities, overrideImportedActivities());
    KURI_RW_PROPERTY(bool, activityMigrationComplete, activityMigrationComplete());

    // strava specific settings
    KURI_RO_PROPERTY(quint64, stravaClientId, stravaClientId());
    KURI_RO_PROPERTY(QString, stravaClientSecret, stravaClientSecret());
    KURI_RW_PROPERTY(QString, stravaAccessToken, stravaAccessToken());
    KURI_RW_PROPERTY(QString, stravaRefreshToken, stravaRefreshToken());
    KURI_RW_PROPERTY(quint64, stravaExpirationTime, stravaExpirationTime());
    KURI_RW_PROPERTY(QString, stravaUserName, stravaUserName());
    KURI_RW_PROPERTY(QString, stravaCountry, stravaCountry());
};

} // namespace kuri
