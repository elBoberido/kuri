/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>

namespace kuri {

struct ChartDataPoint {
    Q_GADGET

public:
    ChartDataPoint() {}

    ChartDataPoint(quint64 x, qreal median, qreal min, qreal max)
        : x(x)
        , median(median)
        , min(min)
        , max(max) {}

    Q_PROPERTY(quint64 x MEMBER x)
    Q_PROPERTY(qreal median MEMBER median)
    Q_PROPERTY(qreal min MEMBER min)
    Q_PROPERTY(qreal max MEMBER max)

    quint64 x {0};
    qreal   median {0.};
    qreal   min {0.};
    qreal   max {0.};

    static void qmlRegisterTypes();
};

} // namespace kuri

Q_DECLARE_METATYPE(kuri::ChartDataPoint)
