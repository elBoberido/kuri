/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "macros.hpp"
#include "track_point.hpp"

#include "kuri-rs/ffi/events.h"
#include "kuri-rs/rust/activity_recorder.rs.h"

#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QObject>
#include <QTimer>

namespace kuri {

class ActivityRecorder : public QObject {
    Q_OBJECT

public:
    ActivityRecorder();
    ~ActivityRecorder();

    ActivityRecorder(const ActivityRecorder&) = delete;
    ActivityRecorder(ActivityRecorder&&)      = delete;

    ActivityRecorder& operator=(const ActivityRecorder&) = delete;
    ActivityRecorder& operator=(ActivityRecorder&&)      = delete;

    void positionUpdate(const QGeoPositionInfo& update);
    // TODO this is currently called from QML because some parsing for non BTLE devices is done there;
    //      the parsing should be moved to C++/Rust, e.g. a HrmSource class
    Q_INVOKABLE void heartRateUpdate(const quint16 update);

    Q_INVOKABLE TrackPoint latestTrackPoint(kuri::TrackPoint::Tag tag); // kuri namespace is required to let QML recording the type

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE void nextSection();
    Q_INVOKABLE void setActivityType(QString activityType);
    Q_INVOKABLE void setActivityName(QString activityName);
    Q_INVOKABLE void setActivityDescription(QString activityDesc);
    Q_INVOKABLE void saveRecording();
    Q_INVOKABLE void discardRecording();

    KURI_RO_PROPERTY(bool, recording, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(bool, pausing, false);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, duration, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, sectionCount, 1);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, durationInSection, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint16, heartRate, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint16, heartRateAverage, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, distance, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, distance60s, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(qint64, elevation, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, elevationUp, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(quint64, elevationDown, 0);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(qreal, speedAverage, 0.);
    KURI_RO_PROPERTY_WITH_DEFAULT_IMPL(qreal, paceAverage, 0.);

public:
    Q_PROPERTY(QString durationAsHoursMinutesSeconds READ durationAsHoursMinutesSeconds NOTIFY durationChanged)
    Q_PROPERTY(QString durationInSectionAsMinutesSeconds READ durationInSectionAsMinutesSeconds NOTIFY durationInSectionChanged)
    Q_PROPERTY(QString paceAverageInMinutesPerKm READ paceAverageInMinutesPerKm NOTIFY paceAverageChanged)

    QString durationAsHoursMinutesSeconds() const;
    QString durationInSectionAsMinutesSeconds() const;
    QString paceAverageInMinutesPerKm() const;

signals:
    void newTrackPoint(TrackPoint point);
    void newActivityAvailable(QString activityId);

private:
    uint16_t millisecondsToNextTick() const;

    void processArtifacts();

    rs::ActivityRecorder initActivityRecorderRs();

private:
    static constexpr uint16_t INTERVAL_IN_MILLISECONDS {1000};
    static constexpr uint16_t OFFSET_IN_MILLISECONDS {200};

    rs::ActivityRecorder m_recorder {initActivityRecorderRs()};

    QTimer m_tickTimer;

    QGeoCoordinate m_latestCoordinate;

    bool m_recordingStartPending {false};
};

} // namespace kuri
