/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_history.hpp"
#include "activity_history_track.hpp"
#include "activity_recorder.hpp"
#include "chart_data_point.hpp"
#include "device.h"
#include "formatter.hpp"
#include "geo_position_info_source.hpp"
#include "settings.hpp"
#include "track_point.hpp"

#include <sailfishapp.h>

#include <QGuiApplication>
#include <QtGlobal>
#include <QtQml/QtQml>
#include <QtQuick/QQuickView>

int main(int argc, char* argv[]) {
    // disable debug logging of category `default` (only qDebug from C++; not console.log) since QGridLayout spams the console
    QLoggingCategory::setFilterRules("default.debug=false\n");

    QScopedPointer<QGuiApplication> app {SailfishApp::application(argc, argv)};

    app->setApplicationName("Kuri");
    app->setApplicationVersion(QString(APP_VERSION));

    qDebug() << app->applicationName() << " version " << app->applicationVersion();

    using namespace kuri;

    ChartDataPoint::qmlRegisterTypes();
    TrackPoint::qmlRegisterTypes();

    qmlRegisterSingletonType(SailfishApp::pathTo("qml/components/RecordPageColorTheme.qml"), "harbour.kuri", 1, 0, "RecordPageColorTheme");

    Formatter             formatter;
    Settings              settings;
    ActivityHistory       history {settings};
    GeoPositionInfoSource geoPositionInfo;
    ActivityRecorder      activityRecorder;
    Device                hrmDevice;

    QObject::connect(&geoPositionInfo, &GeoPositionInfoSource::positionUpdated, &activityRecorder, &ActivityRecorder::positionUpdate);
    QObject::connect(&activityRecorder, &ActivityRecorder::newActivityAvailable, &history, &ActivityHistory::loadNewActivity);

    QScopedPointer<QQuickView> view {SailfishApp::createView()};
    view->rootContext()->setContextProperty("appVersion", app->applicationVersion());
    view->rootContext()->setContextProperty("StringFormatter", &formatter);
    view->rootContext()->setContextProperty("Settings", &settings);
    view->rootContext()->setContextProperty("GeoPositionInfo", &geoPositionInfo);
    view->rootContext()->setContextProperty("ActivityHistory", &history);
    view->rootContext()->setContextProperty("ActivityHistoryTrack", &history.track());
    view->rootContext()->setContextProperty("ActivityRecorder", &activityRecorder);
    view->rootContext()->setContextProperty("HrmDevice", &hrmDevice);

    view->setSource(SailfishApp::pathTo("qml/harbour-kuri.qml"));
    view->showFullScreen();

    history.migrateActivitiesFromLegacyPath();

    return app->exec();
}
