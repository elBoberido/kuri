/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_history.hpp"

#include <QDate>

namespace kuri {

ActivityHistory::ActivityHistory(Settings& settings, QObject* parent)
    : QSortFilterProxyModel(parent)
    , m_activityTypeFilter(settings.activityTypeFilter())
    , m_settings(settings) {
    setSourceModel(&m_model);
    setFilterRole(static_cast<int>(ActivityHistoryModel::Roles::ActivityType));
    setFilterFixedString(m_activityTypeFilter.compare("allactivities") == 0 ? "" : m_activityTypeFilter);
    setSortRole(static_cast<int>(ActivityHistoryModel::Roles::StartTime));
    constexpr int COLUMN {0};
    sort(COLUMN, Qt::DescendingOrder);

    m_track.setActivityHistoryRs(m_history);

    m_history.load_all_activities();

    connect(this, &ActivityHistory::activityTotalCountChanged, &m_model, &ActivityHistoryModel::updateActivityCache);
    connect(this, &ActivityHistory::activityChanged, &m_model, &ActivityHistoryModel::updateSincleActivityFromCache);
    m_model.updateActivityCache();
}

rs::ActivityHistory ActivityHistory::initActivityHistoryRs() {
    activitiesLoadingSet(false);

    auto  events  = rs::new_artifact_and_history_events();
    auto& aEvents = events.artifact;
    connect(aEvents.new_artifacts.get(), &ffi::EventVoid::signal, this, &ActivityHistory::processArtifacts, Qt::QueuedConnection);
    auto& hEvents = events.history;
    connect(hEvents.activities_loading.get(), &ffi::EventBool::signal, this, &ActivityHistory::activitiesLoadingSet);
    connect(hEvents.activity_import_ongoing.get(), &ffi::EventBool::signal, this, &ActivityHistory::activityImportOngoingSet);
    connect(hEvents.activity_export_ongoing.get(), &ffi::EventBool::signal, this, &ActivityHistory::activityExportOngoingSet);
    connect(hEvents.activity_migration_ongoing.get(), &ffi::EventBool::signal, this, &ActivityHistory::activityMigrationOngoingSet);
    connect(hEvents.activity_migration_ongoing.get(), &ffi::EventBool::signal, this, &ActivityHistory::setActivityMigrationComplete);
    connect(hEvents.track_available.get(), &ffi::EventBool::signal, &this->m_track, &ActivityHistoryTrack::availableSet);
    connect(hEvents.activity_total_count_changed.get(), &ffi::EventU64::signal, this, &ActivityHistory::activityTotalCountSet);
    connect(hEvents.activity_summary_changed.get(), &ffi::EventVoid::signal, this, &ActivityHistory::propagateActivitySummaryChanges);
    connect(hEvents.activity_changed.get(), &ffi::EventU64::signal, this, &ActivityHistory::activityChanged);
    connect(hEvents.gpx_track_available.get(), &ffi::EventVoid::signal, &this->m_track, &ActivityHistoryTrack::gpxTrackAvailable);
    return rs::new_activity_history(std::move(events));
}

void ActivityHistory::processArtifacts() {
    m_history.process_artifacts();
}

ActivityHistory::~ActivityHistory() {
    m_history.shutdown();
}

int ActivityHistory::mapToSourceRow(int row) {
    constexpr int COLUMN {0};
    auto          modelIndex = this->index(row, COLUMN);
    return mapToSource(modelIndex).row();
}

ActivityHistoryTrack& ActivityHistory::track() {
    return m_track;
}

void ActivityHistory::loadNewActivity(QString activityId) {
    const auto id = activityId.toUtf8();
    m_history.load_activity({id.data(), static_cast<size_t>(id.size())});
}

QString ActivityHistory::activityTypeFilter() const {
    return m_activityTypeFilter;
}

void ActivityHistory::activityTypeFilterSet(QString activityType) {
    setFilterFixedString(activityType.compare("allactivities") == 0 ? "" : activityType);
    m_activityTypeFilter = activityType;
    m_settings.activityTypeFilterSet(activityType);
    propagateActivitySummaryChanges();
}

void ActivityHistory::updateActivitySummary(QString activityId, int row, QString oldActivityType, QString newActivityType) {
    m_model.updateActivitySummary(activityId, mapToSourceRow(row), oldActivityType, newActivityType);
}

void ActivityHistory::saveActivityChanges(QString activityId, int row) {
    m_model.saveActivityChanges(activityId, mapToSourceRow(row));
}

void ActivityHistory::deleteActivity(QString activityId, int row) {
    m_model.removeActivity(activityId, mapToSourceRow(row));

    const auto id = activityId.toUtf8();
    m_history.delete_activity({id.data(), static_cast<size_t>(id.size())});
}

void ActivityHistory::importActivities() {
    const auto sf = m_settings.importSourceFolder().toUtf8();
    m_history.import_activities({sf.data(), static_cast<size_t>(sf.size())}, m_settings.overrideImportedActivities());
}

void ActivityHistory::exportActivities() {
    const auto df = m_settings.exportDestinationFolder().toUtf8();
    m_history.export_activities({df.data(), static_cast<size_t>(df.size())});
}

void ActivityHistory::exportActivity(QString activityId) {
    const auto id = activityId.toUtf8();
    const auto df = m_settings.exportDestinationFolder().toUtf8();
    m_history.export_activity({df.data(), static_cast<size_t>(df.size())}, {id.data(), static_cast<size_t>(id.size())});
}

void ActivityHistory::migrateActivitiesFromLegacyPath() {
    if (!m_settings.activityMigrationComplete()) { m_history.migrate_activities_from_legacy_path(); }
}

void ActivityHistory::setActivityMigrationComplete(bool ongoing) {
    if (ongoing) {
        disconnect(this, &ActivityHistory::activityTotalCountChanged, &m_model, &ActivityHistoryModel::updateActivityCache);
        disconnect(this, &ActivityHistory::activityChanged, &m_model, &ActivityHistoryModel::updateSincleActivityFromCache);
    } else {
        m_settings.activityMigrationCompleteSet(true);

        connect(this, &ActivityHistory::activityTotalCountChanged, &m_model, &ActivityHistoryModel::updateActivityCache);
        disconnect(this, &ActivityHistory::activityChanged, &m_model, &ActivityHistoryModel::updateSincleActivityFromCache);
        m_model.forceUpdateActivityCache();
    }
}

void ActivityHistory::redoActivityMigrationFromLegacyPath() {
    m_history.migrate_activities_from_legacy_path();
}

void ActivityHistory::propagateActivitySummaryChanges() {
    const auto type = m_activityTypeFilter.toUtf8();
    const auto s    = m_history.activity_summary({type.data(), static_cast<size_t>(type.size())});

    activityCountFromFilterSet(s.activity_count);
    activityDurationFromFilterSet(s.duration);
    activityDistanceFromFilterSet(s.distance);

    hasVariousActivityTypesSet(m_history.activity_type_count() > 1);
}

QString ActivityHistory::intervalSummaryName(quint64 intervalId) const {
    auto& name = m_intervalSummaryNameCache[intervalId];
    if (name.isEmpty()) {
        const auto id = unflattenIntervalSummaryId(intervalId);
        name          = QDate(id.year, id.month, 1).toString("MMMM yyyy");
    }

    return name;
}

quint64 ActivityHistory::intervalSummaryDistance(quint64 intervalId) const {
    const auto type = m_activityTypeFilter.toUtf8();
    const auto s    = m_history.activity_interval_summary({type.data(), static_cast<size_t>(type.size())}, unflattenIntervalSummaryId(intervalId));

    return s.distance;
}

quint64 ActivityHistory::intervalSummaryDuration(quint64 intervalId) const {
    const auto type = m_activityTypeFilter.toUtf8();
    const auto s    = m_history.activity_interval_summary({type.data(), static_cast<size_t>(type.size())}, unflattenIntervalSummaryId(intervalId));

    return s.duration;
}

} // namespace kuri
