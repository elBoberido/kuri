/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.hpp"

#include "device.h"

namespace kuri {

Settings::Settings(QObject* parent)
    : QObject(parent) {
    useCachedValues = true;
}

Settings::~Settings() {
    m_settings->save();
}

QString Settings::activityTypeFilter() const {
    if (useCachedValues) { return m_activityTypeFilter; }
    const auto s = m_settings->history_activity_type_filter();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::activityTypeFilterSet(QString activityType) {
    if (activityType == m_activityTypeFilter) { return; }
    m_activityTypeFilter = activityType;
    const auto s         = m_activityTypeFilter.toUtf8();
    m_settings->history_set_activity_type_filter({s.data(), static_cast<size_t>(s.size())});
    // signal intentionally not emitted
}

QString Settings::activityType() const {
    if (useCachedValues) { return m_activityType; }
    const auto s = m_settings->rec_activity_type();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::activityTypeSet(QString activityType) {
    if (activityType == m_activityType) { return; }
    m_activityType = activityType;
    const auto s  = m_activityType.toUtf8();
    m_settings->rec_set_activity_type({s.data(), static_cast<size_t>(s.size())});
    emit activityTypeChanged();
}

qreal Settings::positionAccuracyThreshold() const {
    if (useCachedValues) { return m_positionAccuracyThreshold; }
    return m_settings->rec_position_accuracy_threshold();
}
void Settings::positionAccuracyThresholdSet(qreal accuracyThreshold) {
    if (accuracyThreshold == m_positionAccuracyThreshold) { return; }
    m_positionAccuracyThreshold = accuracyThreshold;
    m_settings->rec_set_position_accuracy_threshold(m_positionAccuracyThreshold);
    emit positionAccuracyThresholdChanged();
}

bool Settings::showMapRecordPage() const {
    if (useCachedValues) { return m_showMapRecordPage; }
    return m_settings->rec_show_map();
}
void Settings::showMapRecordPageSet(bool showMapRecordPage) {
    if (showMapRecordPage == m_showMapRecordPage) { return; }
    m_showMapRecordPage = showMapRecordPage;
    m_settings->rec_set_show_map(m_showMapRecordPage);
    emit showMapRecordPageChanged();
}

bool Settings::disableScreenBlanking() const {
    if (useCachedValues) { return m_disableScreenBlanking; }
    return m_settings->rec_screen_blanking_off();
}
void Settings::disableScreenBlankingSet(bool disableScreenBlanking) {
    if (disableScreenBlanking == m_disableScreenBlanking) { return; }
    m_disableScreenBlanking = disableScreenBlanking;
    m_settings->rec_set_screen_blanking_off(m_disableScreenBlanking);
    emit disableScreenBlankingChanged();
}

quint64 Settings::displayMode() const {
    if (useCachedValues) { return m_displayMode; }
    return m_settings->rec_display_mode();
}
void Settings::displayModeSet(quint64 displayMode) {
    if (displayMode == m_displayMode) { return; }
    m_displayMode = displayMode;
    m_settings->rec_set_display_mode(m_displayMode);
    emit displayModeChanged();
}

bool Settings::enableAutosave() const {
    if (useCachedValues) { return m_enableAutosave; }
    return m_settings->rec_auto_save();
}
void Settings::enableAutosaveSet(bool enableAutosave) {
    if (enableAutosave == m_enableAutosave) { return; }
    m_enableAutosave = enableAutosave;
    m_settings->rec_set_auto_save(m_enableAutosave);
    emit enableAutosaveChanged();
}

quint64 Settings::mapCenterMode() const {
    if (useCachedValues) { return m_mapCenterMode; }
    return m_settings->map_center_mode();
}
void Settings::mapCenterModeSet(quint64 mapCenterMode) {
    if (mapCenterMode == m_mapCenterMode) { return; }
    m_mapCenterMode = mapCenterMode;
    m_settings->map_set_center_mode(m_mapCenterMode);
    emit mapCenterModeChanged();
}

QString Settings::mapStyle() const {
    if (useCachedValues) { return m_mapStyle; }
    const auto s = m_settings->map_style();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::mapStyleSet(QString mapStyle) {
    if (mapStyle == m_mapStyle) { return; }
    m_mapStyle   = mapStyle;
    const auto s = m_mapStyle.toUtf8();
    m_settings->map_set_style({s.data(), static_cast<size_t>(s.size())});
    emit mapStyleChanged();
}

quint64 Settings::mapCache() const {
    if (useCachedValues) { return m_mapCache; }
    return m_settings->map_cache_size();
}
void Settings::mapCacheSet(quint64 mapCache) {
    if (mapCache == m_mapCache) { return; }
    m_mapCache = mapCache;
    m_settings->map_set_cache_size(m_mapCache);
    emit mapCacheChanged();
}

bool Settings::useHRMdevice() const {
    if (useCachedValues) { return m_useHRMdevice; }
    return m_settings->hrm_source() == 1;
}
void Settings::useHRMdeviceSet(bool value) {
    if (value == m_useHRMdevice) { return; }
    m_useHRMdevice = value;
    if (m_useHRMdevice) {
        useHRMserviceSet(false);
        m_settings->hrm_set_source(1);
    } else {
        m_settings->hrm_set_source(0);
    }
    emit useHRMdeviceChanged();
}

bool Settings::useHRMservice() const {
    if (useCachedValues) { return m_useHRMservice; }
    return m_settings->hrm_source() == 2;
}
void Settings::useHRMserviceSet(bool value) {
    if (value == m_useHRMservice) { return; }
    m_useHRMservice = value;
    if (m_useHRMservice) {
        useHRMdeviceSet(false);
        m_settings->hrm_set_source(2);
    } else {
        m_settings->hrm_set_source(0);
    }
    emit useHRMserviceChanged();
}

QString Settings::hrmDeviceName() const {
    if (useCachedValues) { return m_hrmDeviceName; }
    const auto s = m_settings->hrm_device_name();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::hrmDeviceNameSet(QString deviceName) {
    if (deviceName == m_hrmDeviceName) { return; }
    m_hrmDeviceName = deviceName;
    const auto s    = m_hrmDeviceName.toUtf8();
    m_settings->hrm_set_device_name({s.data(), static_cast<size_t>(s.size())});
    emit hrmDeviceNameChanged();
}

QString Settings::hrmDeviceAddress() const {
    if (useCachedValues) { return m_hrmDeviceAddress; }
    const auto s = m_settings->hrm_device_address();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::hrmDeviceAddressSet(QString deviceAddress) {
    if (deviceAddress == m_hrmDeviceAddress) { return; }
    m_hrmDeviceAddress = deviceAddress;
    const auto s       = m_hrmDeviceAddress.toUtf8();
    m_settings->hrm_set_device_address({s.data(), static_cast<size_t>(s.size())});
    emit hrmDeviceAddressChanged();
}

quint64 Settings::bluetoothType() const {
    if (useCachedValues) { return m_bluetoothType; }
    return m_settings->hrm_bluetooth_type();
}
void Settings::bluetoothTypeSet(quint64 value) {
    if (value == m_bluetoothType) { return; }
    m_bluetoothType = value;
    m_settings->hrm_set_bluetooth_type(value);
    emit bluetoothTypeChanged();
}

QString Settings::exportDestinationFolder() const {
    if (useCachedValues) { return m_exportDestinationFolder; }
    const auto s = m_settings->export_destination_folder();
    return QString::fromUtf8(s.data(), s.length());
}

QString Settings::importSourceFolder() const {
    if (useCachedValues) { return m_importSourceFolder; }
    const auto s = m_settings->import_source_folder();
    return QString::fromUtf8(s.data(), s.length());
}

bool Settings::overrideImportedActivities() const {
    if (useCachedValues) { return m_overrideImportedActivities; }
    return m_settings->import_override_imported_activities();
}
void Settings::overrideImportedActivitiesSet(bool value) {
    if (value == m_overrideImportedActivities) { return; }
    m_overrideImportedActivities = value;
    m_settings->import_set_override_imported_activities(m_overrideImportedActivities);
    emit overrideImportedActivitiesChanged();
}

bool Settings::activityMigrationComplete() const {
    if (useCachedValues) { return m_activityMigrationComplete; }
    return m_settings->migration_legacy_activities_imported();
}
void Settings::activityMigrationCompleteSet(bool value) {
    if (value == m_activityMigrationComplete) { return; }
    m_activityMigrationComplete = value;
    m_settings->migration_set_legacy_activities_imported(m_activityMigrationComplete);
    emit activityMigrationCompleteChanged();
}

quint64 Settings::stravaClientId() const {
    if (useCachedValues) { return m_stravaClientId; }
    return m_settings->strava_client_id();
}
QString Settings::stravaClientSecret() const {
    if (useCachedValues) { return m_stravaClientSecret; }
    const auto s = m_settings->strava_client_secret();
    return QString::fromUtf8(s.data(), s.length());
}

QString Settings::stravaAccessToken() const {
    if (useCachedValues) { return m_stravaAccessToken; }
    const auto s = m_settings->strava_access_token();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::stravaAccessTokenSet(QString accessToken) {
    if (accessToken == m_stravaAccessToken) { return; }
    m_stravaAccessToken = accessToken;
    const auto s        = m_stravaAccessToken.toUtf8();
    m_settings->strava_set_access_token({s.data(), static_cast<size_t>(s.size())});
    emit stravaAccessTokenChanged();
}

QString Settings::stravaRefreshToken() const {
    if (useCachedValues) { return m_stravaRefreshToken; }
    const auto s = m_settings->strava_refresh_token();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::stravaRefreshTokenSet(QString refreshToken) {
    if (refreshToken == m_stravaRefreshToken) { return; }
    m_stravaRefreshToken = refreshToken;
    const auto s         = m_stravaRefreshToken.toUtf8();
    m_settings->strava_set_refresh_token({s.data(), static_cast<size_t>(s.size())});
    emit stravaRefreshTokenChanged();
}

quint64 Settings::stravaExpirationTime() const {
    if (useCachedValues) { return m_stravaExpirationTime; }
    return m_settings->strava_expiration_time();
}
void Settings::stravaExpirationTimeSet(quint64 expirationTime) {
    if (expirationTime == m_stravaExpirationTime) { return; }
    m_stravaExpirationTime = expirationTime;
    m_settings->strava_set_expiration_time(m_stravaExpirationTime);
    emit stravaExpirationTimeChanged();
}

QString Settings::stravaUserName() const {
    if (useCachedValues) { return m_stravaUserName; }
    const auto s = m_settings->strava_user_name();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::stravaUserNameSet(QString userName) {
    if (userName == m_stravaUserName) { return; }
    m_stravaUserName = userName;
    const auto s     = m_stravaUserName.toUtf8();
    m_settings->strava_set_user_name({s.data(), static_cast<size_t>(s.size())});
    emit stravaUserNameChanged();
}

QString Settings::stravaCountry() const {
    if (useCachedValues) { return m_stravaCountry; }
    const auto s = m_settings->strava_country();
    return QString::fromUtf8(s.data(), s.length());
}
void Settings::stravaCountrySet(QString country) {
    if (country == m_stravaCountry) { return; }
    m_stravaCountry = country;
    const auto s    = m_stravaCountry.toUtf8();
    m_settings->strava_set_country({s.data(), static_cast<size_t>(s.size())});
    emit stravaCountryChanged();
}

} // namespace kuri
