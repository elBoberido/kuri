
#include "chart_data_point.hpp"

#include <QVector>
#include <QtQml/QtQml>

namespace kuri {

void ChartDataPoint::qmlRegisterTypes() {
    qRegisterMetaType<ChartDataPoint>("ChartDataPoint");
    qRegisterMetaType<QVector<ChartDataPoint>>("QVector<ChartDataPoint>");
    qmlRegisterUncreatableType<ChartDataPoint>("harbour.kuri", 1, 0, "ChartDataPoint", "Not a creatable type for QML");
}

} // namespace kuri
