/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activity_history_model.hpp"
#include "formatter.hpp"

namespace kuri {

quint64 flattenIntervalSummaryId(rs::IntervalSummaryId id) {
    return (static_cast<quint64>(id.year) << 32) + static_cast<quint64>(id.month);
}

rs::IntervalSummaryId unflattenIntervalSummaryId(quint64 id) {
    const auto year  = static_cast<int32_t>((id >> 32) & 0xFFFFFFFF);
    const auto month = static_cast<int32_t>(id & 0xFFFFFFFF);
    return rs::IntervalSummaryId {year, month};
}

ActivityHistoryModel::ActivityHistoryModel(rs::ActivityHistory& history, QObject* parent)
    : QAbstractListModel(parent)
    , m_history(history) {}

void ActivityHistoryModel::fillCache(int fromIndex, int toIndexExcluding) {
    beginInsertRows(QModelIndex(), fromIndex, toIndexExcluding - 1);
    for (int i = fromIndex; i < toIndexExcluding; ++i) {
        m_cache.push_back(ActivityData(m_history.activity_at_mut(i)));

        // set activity type and start time for filtering and sorting
        auto& c = m_cache.back();

        auto activityType = c.activity->activity_type();
        c.activityType    = QString::fromUtf8(activityType.data(), activityType.length());

        constexpr auto DATE_TIME_WITHOUT_OFFSET {19};
        auto           startTime = c.activity->start_time();
        if (startTime.length() >= DATE_TIME_WITHOUT_OFFSET) {
            auto timeString = QString::fromUtf8(startTime.data(), DATE_TIME_WITHOUT_OFFSET);
            c.startTime     = QDateTime::fromString(timeString, "yyyy-MM-ddTHH:mm:ss");
        }
    }
    endInsertRows();
}

void ActivityHistoryModel::updateSincleActivityFromCache(quint64 row) {
    if (!rowInRange(row)) { return; }

    m_cache[row].needsRefresh = true;

    constexpr int COLUMN {0};

    auto index = this->index(row, COLUMN);
    emit dataChanged(index, index);
}

void ActivityHistoryModel::updateActivityCache() {
    int fromIndex     = m_cache.size();
    int activityCount = m_history.activity_total_count();
    if (fromIndex == 0 || (fromIndex < activityCount && m_cache.back().activity == &m_history.activity_at(fromIndex - 1))) {
        fillCache(fromIndex, activityCount);
    } else if (fromIndex != activityCount) {
        forceUpdateActivityCache();
    }
}

void ActivityHistoryModel::forceUpdateActivityCache() {
    int cacheSize     = m_cache.size();
    int activityCount = m_history.activity_total_count();

    this->beginRemoveRows(QModelIndex(), 0, cacheSize - 1);
    this->m_cache.clear();
    this->endRemoveRows();

    fillCache(0, activityCount);
}

ActivityHistoryModel::ActivityData::ActivityData(rs::Activity& activity)
    : activity(&activity) {}

void ActivityHistoryModel::ActivityData::refresh() const {
    if (!needsRefresh) { return; }

    auto activityId  = activity->id();
    this->activityId = QString::fromUtf8(activityId.data(), activityId.length());

    auto activityType  = activity->activity_type();
    this->activityType = QString::fromUtf8(activityType.data(), activityType.length());

    auto name  = activity->name();
    this->name = QString::fromUtf8(name.data(), name.length());

    auto description  = activity->description();
    this->description = QString::fromUtf8(description.data(), description.length());

    if (this->startTime.isValid()) {
        this->startTimeShort = this->startTime.toString("yyyy-MM-dd HH:mm");
        this->startTimeLong  = this->startTime.toString(Qt::TextDate);
    }

    this->speedAverage = activity->speed_average();
    this->paceAverage  = activity->pace_average();

    needsRefresh = false;
}

void ActivityHistoryModel::updateActivitySummary(QString activityId, int row, QString oldActivityType, QString newActivityType) {
    if (!rowInRange(row)) { return; }

    const auto oldAT = oldActivityType.toUtf8();
    const auto newAT = newActivityType.toUtf8();

    auto update = [&](auto& activity) {
        m_history.update_activity_summary(activity, {oldAT.data(), static_cast<size_t>(oldAT.size())}, {newAT.data(), static_cast<size_t>(newAT.size())});
    };

    auto& cache = m_cache[row];
    if (cache.activityId == activityId) {
        update(*cache.activity);
    } else {
        for (auto& cache : m_cache) {
            if (cache.activityId == activityId) {
                update(*cache.activity);
                break;
            }
        }
    }
}

void ActivityHistoryModel::saveActivityChanges(QString activityId, int row) {
    if (!rowInRange(row)) { return; }

    auto& cache = m_cache[row];
    if (cache.activityId == activityId) {
        cache.activity->save_changes();
    } else {
        for (auto& cache : m_cache) {
            if (cache.activityId == activityId) {
                cache.activity->save_changes();
                break;
            }
        }
    }
}

void ActivityHistoryModel::removeActivity(QString activityId, int row) {
    if (!rowInRange(row)) { return; }

    auto remove = [this](int row) {
        this->beginRemoveRows(QModelIndex(), row, row);
        this->m_cache.removeAt(row);
        this->endRemoveRows();
    };

    auto& cache = m_cache[row];
    if (cache.activityId == activityId) {
        remove(row);
    } else {
        row = 0;
        for (auto& cache : m_cache) {
            if (cache.activityId == activityId) {
                remove(row);
                break;
            }
            ++row;
        }
    }
}

int ActivityHistoryModel::rowCount(const QModelIndex&) const {
    return m_history.activity_total_count();
}

QVariant ActivityHistoryModel::data(const QModelIndex& index, int role) const {
    auto row = index.row();
    if (!rowInRange(row)) { return QVariant {}; }

    auto  historyRole = static_cast<Roles>(role);
    auto& cache       = m_cache[row];

    // fast path without cache refresh for start time and activity type
    switch (historyRole) {
        case Roles::StartTime:
            return cache.startTime;
        case Roles::ActivityType:
            return cache.activityType;
        default:
            break;
    }

    // remap display role to activity name
    if (role == Qt::DisplayRole) { historyRole = Roles::Name; }

    cache.refresh();
    switch (historyRole) {
        case Roles::ActivityId:
            return cache.activityId;
        case Roles::Name:
            return cache.name;
        case Roles::ActivityType:
            return cache.activityType;
        case Roles::Description:
            return cache.description;
        case Roles::IntervalSummaryId:
            return flattenIntervalSummaryId(cache.activity->interval_id());
        case Roles::DateTimeShort:
            return cache.startTimeShort;
        case Roles::DateTimeLong:
            return cache.startTimeLong;
        case Roles::Duration:
            return static_cast<quint64>(cache.activity->duration_active());
        case Roles::Pause:
            return static_cast<quint64>(cache.activity->duration_pause());
        case Roles::Distance:
            return static_cast<quint64>(cache.activity->distance());
        case Roles::ElevationUp:
            return static_cast<quint64>(cache.activity->elevation_up());
        case Roles::ElevationDown:
            return static_cast<quint64>(cache.activity->elevation_down());
        case Roles::SpeedAverage:
            return cache.speedAverage;
        case Roles::PaceAverage:
            return cache.paceAverage;
        case Roles::HeartRateAverage:
            return static_cast<quint16>(cache.activity->heart_rate_average());
        default:
            return QVariant();
    }

    return QVariant {};
}

QVariant ActivityHistoryModel::headerData(int section, Qt::Orientation, int role) const {
    if (role != Qt::DisplayRole) { return QVariant(); }

    return QString("TODO: Header Data for section %1").arg(section);
}

bool ActivityHistoryModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    auto row = index.row();
    if (!rowInRange(row)) { return false; }

    auto  historyRole = static_cast<Roles>(role);
    auto& cache       = m_cache[row];

    // remap display role to activity name
    if (role == Qt::DisplayRole) { historyRole = Roles::Name; }

    cache.refresh();
    switch (historyRole) {
        case Roles::Name: {
            cache.name      = value.toString();
            const auto name = cache.name.toUtf8();
            cache.activity->set_activity_name({name.data(), static_cast<size_t>(name.size())});
            break;
        }
        case Roles::ActivityType: {
            cache.activityType = value.toString();
            const auto type    = cache.activityType.toUtf8();
            cache.activity->set_activity_type({type.data(), static_cast<size_t>(type.size())});
            break;
        }
        case Roles::Description: {
            cache.description      = value.toString();
            const auto description = cache.description.toUtf8();
            cache.activity->set_activity_description({description.data(), static_cast<size_t>(description.size())});
            break;
        }
        default:
            return false;
    }

    emit dataChanged(index, index, {role});
    return true;
}

Qt::ItemFlags ActivityHistoryModel::flags(const QModelIndex& index) const {
    if (!rowInRange(index.row())) { return Qt::NoItemFlags; }

    return Qt::ItemIsEditable | QAbstractListModel::flags(index);
}

QHash<int, QByteArray> ActivityHistoryModel::roleNames() const {
    QHash<int, QByteArray> roles;

    roles[static_cast<int>(Roles::StartTime)]         = "startTime";
    roles[static_cast<int>(Roles::ActivityId)]        = "activityId";
    roles[static_cast<int>(Roles::ActivityType)]      = "activityType";
    roles[static_cast<int>(Roles::Name)]              = "name";
    roles[static_cast<int>(Roles::Description)]       = "description";
    roles[static_cast<int>(Roles::IntervalSummaryId)] = "intervalSummaryId";
    roles[static_cast<int>(Roles::DateTimeShort)]     = "dateTimeShort";
    roles[static_cast<int>(Roles::DateTimeLong)]      = "dateTimeLong";
    roles[static_cast<int>(Roles::Duration)]          = "duration";
    roles[static_cast<int>(Roles::Pause)]             = "pause";
    roles[static_cast<int>(Roles::Distance)]          = "distance";
    roles[static_cast<int>(Roles::ElevationUp)]       = "elevationUp";
    roles[static_cast<int>(Roles::ElevationDown)]     = "elevationDown";
    roles[static_cast<int>(Roles::SpeedAverage)]      = "speedAverage";
    roles[static_cast<int>(Roles::PaceAverage)]       = "paceAverage";
    roles[static_cast<int>(Roles::HeartRateAverage)]  = "heartRateAverage";

    return roles;
}

bool ActivityHistoryModel::rowInRange(int row) const {
    return row >= 0 && row < m_cache.size();
}

} // namespace kuri
