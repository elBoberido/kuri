/*
 * Copyright (C) 2022 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QObject>
#include <QTimer>

#include <memory>

namespace kuri {

class GeoPositionInfoSource : public QObject {
    Q_OBJECT
public:
    GeoPositionInfoSource();

    Q_INVOKABLE void startUpdates();
    Q_INVOKABLE void stopUpdates();

    int updateInterval() const;

    Q_PROPERTY(qreal accuracy READ accuracy NOTIFY accuracyChanged)
    Q_PROPERTY(QGeoCoordinate currentPosition READ currentPosition NOTIFY currentPositionChanged)

    qreal          accuracy() const;
    QGeoCoordinate currentPosition() const;

signals:
    void positionUpdated(const QGeoPositionInfo& update);

    void accuracyChanged();
    void currentPositionChanged();

private:
    void updateSimulatedPosition();
    void handlePositionUpdate(const QGeoPositionInfo& update);

private:
    std::unique_ptr<QGeoPositionInfoSource> m_source {QGeoPositionInfoSource::createDefaultSource(nullptr)};
    bool                                    m_emulatorDetected {false};

    int            m_millisecondsUpdateIntervall {1000};
    qreal          m_accuracy {-1};
    QGeoCoordinate m_currentPosition;

    QTimer m_accuracyOutdatedTimer;
    QTimer m_simulationTimer;
};

} // namespace kuri
