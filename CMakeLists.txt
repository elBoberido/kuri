cmake_minimum_required(VERSION 3.5)

set(KURI_VERSION 0.5.3)

project(harbour-kuri
    LANGUAGES CXX
    VERSION ${KURI_VERSION}
    DESCRIPTION "Sport Tracker"
)

set(QT_MIN_VERSION "5.6.0")
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

include(FeatureSummary)

find_package (Qt5
    ${QT_MIN_VERSION}
    COMPONENTS
    Bluetooth
    Concurrent
    Core
    DBus
    Gui
    LinguistTools
    Network
    Positioning
    Qml
    Quick
    Sensors
    REQUIRED
)

include(FindPkgConfig)
pkg_search_module(SAILFISH sailfishapp REQUIRED)
pkg_search_module(SAILFISH sailfishapp_i18n REQUIRED)

##################
## Translations ##
##################

set(TRANSLATIONS
    translations/harbour-kuri-de.ts
    translations/harbour-kuri-es.ts
    translations/harbour-kuri-fi_FI.ts
    translations/harbour-kuri-fr.ts
    translations/harbour-kuri-hu.ts
    translations/harbour-kuri-nl.ts
    translations/harbour-kuri-nl_BE.ts
    translations/harbour-kuri-pl.ts
    translations/harbour-kuri-ru.ts
    translations/harbour-kuri-sv.ts
    translations/harbour-kuri-zh_CN.ts
)
set_source_files_properties(${TRANSLATIONS} PROPERTIES OUTPUT_LOCATION
"${CMAKE_BINARY_DIR}/translations")
qt5_create_translation(QM_FILES ${CMAKE_SOURCE_DIR}/qml ${CMAKE_SOURCE_DIR}/cpp ${TRANSLATIONS} OPTIONS -source-language en_US -no-obsolete)

#################
## Rust target ##
#################

set(RUST_BUILD_TYPE "release")
set(RUST_BUILD_TYPE_FLAG "--${RUST_BUILD_TYPE}")
if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(RUST_BUILD_TYPE "debug")
    set(RUST_BUILD_TYPE_FLAG "")
endif()

set(RUST_ARCH_TRIPLET "")
set(RUST_ARCH_TRIPLET_FLAG "")
if(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "i486")
    set(RUST_ARCH_TRIPLET "i686-unknown-linux-gnu")
    set(RUST_ARCH_TRIPLET_FLAG "--target=${RUST_ARCH_TRIPLET}")
elseif(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "armv7l")
    set(RUST_ARCH_TRIPLET "armv7-unknown-linux-gnueabihf")
    set(RUST_ARCH_TRIPLET_FLAG "--target=${RUST_ARCH_TRIPLET}")
elseif(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "aarch64")
    set(RUST_ARCH_TRIPLET "${CMAKE_SYSTEM_PROCESSOR}-unknown-linux-gnu")
    set(RUST_ARCH_TRIPLET_FLAG "--target=${RUST_ARCH_TRIPLET}")
endif()

set(RUST_TARGET_DIR ${CMAKE_BINARY_DIR}/rust-target)
set(RUST_ARCH_TARGET_DIR ${RUST_TARGET_DIR}/${RUST_ARCH_TRIPLET})

# get files containing a cxx::bridge and create list of generated files
file (STRINGS "rust_cxx_bridge_file_list.txt" RUST_CXX_BRIDGE_FILE_LIST)

foreach(RUST_CXX_BRIDGE_FILE ${RUST_CXX_BRIDGE_FILE_LIST})
    list(APPEND RUST_CXX_GENERATED_SOURCE_FILES ${RUST_ARCH_TARGET_DIR}/cxxbridge/kuri-rs/${RUST_CXX_BRIDGE_FILE}.cc)
endforeach()

set(KURI_RS_LIB ${RUST_ARCH_TARGET_DIR}/${RUST_BUILD_TYPE}/libkuri_rs.a)

# don't run automoc on the generated files
cmake_policy(SET CMP0071 NEW)
set_source_files_properties(${RUST_CXX_GENERATED_SOURCE_FILES} PROPERTY SKIP_AUTOMOC ON)

# run cargo
add_custom_target(
    kuri-rs ALL
    COMMAND cargo build ${RUST_BUILD_TYPE_FLAG} -j 1 --target-dir=${RUST_TARGET_DIR} ${RUST_ARCH_TRIPLET_FLAG}
    COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/ffi ${RUST_ARCH_TARGET_DIR}/cxxbridge/kuri-rs/ffi
    BYPRODUCTS
    ${RUST_CXX_GENERATED_SOURCE_FILES}
    ${KURI_RS_LIB}
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
    VERBATIM
    USES_TERMINAL
)

# build C++ files generated from cxx::bridge
add_library(kuri-rs-ffi STATIC
    ${RUST_CXX_GENERATED_SOURCE_FILES}
    ffi/events.h # due to moc
)

add_dependencies(kuri-rs-ffi kuri-rs)

set_target_properties(kuri-rs-ffi PROPERTIES CXX_STANDARD 17 CXX_STANDARD_REQUIRED ON)

target_include_directories(kuri-rs-ffi PUBLIC
    ${RUST_ARCH_TARGET_DIR}/cxxbridge
)
target_link_libraries(kuri-rs-ffi
    Qt5::Core
    ${SAILFISH_LDFLAGS}
    ${KURI_RS_LIB}
    dl
    pthread
)

################
## C++ target ##
################

add_executable(harbour-kuri
    cpp/main.cpp
    cpp/activity_history.cpp
    cpp/activity_history_model.cpp
    cpp/activity_history_track.cpp
    cpp/activity_recorder.cpp
    cpp/chart_data_point.cpp
    cpp/device.cpp
    cpp/deviceinfo.cpp
    cpp/formatter.cpp
    cpp/geo_position_info_source.cpp
    cpp/serviceinfo.cpp
    cpp/settings.cpp
    cpp/track_point.cpp
    ${QM_FILES}
)

set_target_properties(harbour-kuri PROPERTIES CXX_STANDARD 17 CXX_STANDARD_REQUIRED ON)

target_include_directories(harbour-kuri PUBLIC
    $<BUILD_INTERFACE:
    ${SAILFISH_INCLUDE_DIRS}>
)

target_link_libraries(harbour-kuri
    Qt5::Bluetooth
    Qt5::Core
    Qt5::DBus
    Qt5::Gui
    Qt5::Network
    Qt5::Positioning
    Qt5::Qml
    Qt5::Quick
    Qt5::Sensors
    ${SAILFISH_LDFLAGS}
    kuri-rs-ffi
)

target_compile_definitions(harbour-kuri PRIVATE APP_VERSION="${PROJECT_VERSION}")

install(TARGETS harbour-kuri
    RUNTIME DESTINATION bin
)
install(DIRECTORY qml
    DESTINATION share/harbour-kuri
)
install(DIRECTORY ${CMAKE_BINARY_DIR}/translations
    DESTINATION share/harbour-kuri
)
install(FILES harbour-kuri.desktop
    DESTINATION share/applications
)
install(FILES icons/86x86/harbour-kuri.png
    DESTINATION share/icons/hicolor/86x86/apps
)
install(FILES icons/108x108/harbour-kuri.png
    DESTINATION share/icons/hicolor/108x108/apps
)
install(FILES icons/128x128/harbour-kuri.png
    DESTINATION share/icons/hicolor/128x128/apps
)
install(FILES icons/172x172/harbour-kuri.png
    DESTINATION share/icons/hicolor/172x172/apps
)

# Get the other files reachable from the project tree in Qt Creator
add_custom_target(distfiles
    SOURCES
    harbour-kuri.desktop
    rpm/harbour-kuri.changes
    rpm/harbour-kuri.spec
    rpm/harbour-kuri.yaml
    ${TRANSLATIONS}
)

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
