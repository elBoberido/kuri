// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::activity_recorder::tests::{Sut, ToDuration};
use crate::activity_recorder::PositionInfo;
use crate::records::{Measurement, Position, TrackInfo, TrackRecord, TrackSection};

use std::time::Duration;

const TIMESTAMP: Duration = Duration::from_secs(1123580000);

const HEART_RATE: u16 = 123;

const POS_0: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64,
    latitude: 13.30,
    longitude: 42.23,
    altitude: 73.36,
    horizontal_accuracy: 8,
    vertical_accuracy: 4,
};
const POS_1: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 1000,
    latitude: 13.31,
    longitude: 42.24,
    altitude: 73.37,
    horizontal_accuracy: 7,
    vertical_accuracy: 3,
};
const POS_2: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 2000,
    latitude: 13.32,
    longitude: 42.25,
    altitude: 73.38,
    horizontal_accuracy: 6,
    vertical_accuracy: 8,
};
const POS_3: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 3000,
    latitude: 13.33,
    longitude: 42.26,
    altitude: 73.39,
    horizontal_accuracy: 3,
    vertical_accuracy: 2,
};
const POS_4: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 4000,
    latitude: 13.34,
    longitude: 42.27,
    altitude: 73.31,
    horizontal_accuracy: 5,
    vertical_accuracy: 6,
};
const POS_5: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 5000,
    latitude: 13.35,
    longitude: 42.28,
    altitude: 73.32,
    horizontal_accuracy: 3,
    vertical_accuracy: 7,
};
const POS_6: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 6000,
    latitude: 13.36,
    longitude: 42.29,
    altitude: 73.33,
    horizontal_accuracy: 4,
    vertical_accuracy: 2,
};

fn minimal_header() -> String {
    format!(
        r#"[info]
version = {}
"#,
        1,
    )
}

#[test]
fn start_and_stop_without_data_does_not_create_a_file() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    assert_eq!(sut.stop_data_writer(TIMESTAMP), None);
}

#[test]
fn start_and_stop_with_one_tick_creates_file_with_one_section_and_timestamp_only() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.tick(TIMESTAMP);

    let file_content = String::from_utf8(sut.stop_data_writer(TIMESTAMP).unwrap()).unwrap();

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
timestamp = {}

"#,
        minimal_header(),
        TIMESTAMP.as_millis()
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_one_position_update_creates_file_with_one_section_and_position_info() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);

    let file_content = String::from_utf8(sut.stop_data_writer(TIMESTAMP).unwrap()).unwrap();

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
timestamp = {}
{}
"#,
        minimal_header(),
        TIMESTAMP.as_millis(),
        toml::to_string_pretty::<Position>(&POS_0.into()).unwrap()
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_one_heart_rate_update_creates_file_with_one_section_and_heart_rate() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.tick(TIMESTAMP);

    let file_content = String::from_utf8(sut.stop_data_writer(TIMESTAMP).unwrap()).unwrap();

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
timestamp = {}
heart-rate = {}

"#,
        minimal_header(),
        TIMESTAMP.as_millis(),
        HEART_RATE,
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn measurements_before_start_are_discarded() {
    let pos = PositionInfo {
        timestamp: TIMESTAMP.as_millis() as i64 - 1000,
        latitude: 13.31,
        longitude: 42.24,
        altitude: 73.37,
        horizontal_accuracy: 7,
        vertical_accuracy: 3,
    };

    let mut sut = Sut::new();

    sut.heart_rate_update(TIMESTAMP - 3.as_secs(), HEART_RATE + 20);

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP - 2.as_secs(), pos);

    sut.tick(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    let file_content = String::from_utf8(sut.stop_data_writer(TIMESTAMP).unwrap()).unwrap();

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
timestamp = {}
heart-rate = {}

"#,
        minimal_header(),
        TIMESTAMP.as_millis(),
        HEART_RATE,
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn measurements_after_stop_are_discarded() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    let file_content = String::from_utf8(sut.stop_data_writer(TIMESTAMP).unwrap()).unwrap();

    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE);

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
timestamp = {}
heart-rate = {}

"#,
        minimal_header(),
        TIMESTAMP.as_millis(),
        HEART_RATE,
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn position_and_heart_rate_from_same_timestamps_but_alternating_order_results_in_all_data_stored() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);
    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 2.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas2 = Measurement {
        timestamp: TIMESTAMP + 2.as_secs(),
        position: Some(POS_2.into()),
        heart_rate: Some(HEART_RATE + 2),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_without_resume_discards_all_measurements_after_pause() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.pause(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);
    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 2.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_and_resume_on_same_timestamp_creates_crossing_section() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.pause(TIMESTAMP + 1.as_secs());
    sut.resume(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.pause(TIMESTAMP + 2.as_secs());

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);

    sut.resume(TIMESTAMP + 2.as_secs());

    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);

    sut.pause(TIMESTAMP + 3.as_secs());

    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 3);

    sut.resume(TIMESTAMP + 3.as_secs());

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 4);

    sut.pause(TIMESTAMP + 4.as_secs());
    sut.resume(TIMESTAMP + 4.as_secs());

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas2 = Measurement {
        timestamp: TIMESTAMP + 2.as_secs(),
        position: Some(POS_2.into()),
        heart_rate: Some(HEART_RATE + 2),
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: Some(POS_3.into()),
        heart_rate: Some(HEART_RATE + 3),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: Some(HEART_RATE + 4),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_with_one_tick_gap_creates_section_with_one_tick_gap() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);

    sut.pause(TIMESTAMP + 1.as_secs());

    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);

    sut.resume(TIMESTAMP + 2.as_secs());

    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);

    sut.pause(TIMESTAMP + 3.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 3);

    sut.resume(TIMESTAMP + 4.as_secs());

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 4);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas2 = Measurement {
        timestamp: TIMESTAMP + 2.as_secs(),
        position: Some(POS_2.into()),
        heart_rate: Some(HEART_RATE + 2),
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: Some(POS_3.into()),
        heart_rate: Some(HEART_RATE + 3),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: Some(HEART_RATE + 4),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_with_two_tick_gap_and_one_measurement_during_pause_creates_section_with_two_ticks_gap() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.pause(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);

    sut.resume(TIMESTAMP + 3.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 3);

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);

    sut.pause(TIMESTAMP + 4.as_secs());

    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 4);

    sut.position_update(TIMESTAMP + 5.as_secs(), POS_5);
    sut.heart_rate_update(TIMESTAMP + 5.as_secs(), HEART_RATE + 5);

    sut.position_update(TIMESTAMP + 6.as_secs(), POS_6);

    sut.resume(TIMESTAMP + 6.as_secs());

    sut.heart_rate_update(TIMESTAMP + 6.as_secs(), HEART_RATE + 6);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 6.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: Some(POS_3.into()),
        heart_rate: Some(HEART_RATE + 3),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: Some(HEART_RATE + 4),
    };
    let meas6 = Measurement {
        timestamp: TIMESTAMP + 6.as_secs(),
        position: Some(POS_6.into()),
        heart_rate: Some(HEART_RATE + 6),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas6.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_with_multiple_tick_gaps_and_measurements_during_pause_creates_section_with_multiple_ticks_gap(
) {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.pause(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 3);

    sut.resume(TIMESTAMP + 4.as_secs());

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 4);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: Some(HEART_RATE + 4),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn multiple_pause_followed_by_multiple_resume_is_handled_gracefully() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.pause(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.pause(TIMESTAMP + 2.as_secs());

    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);

    sut.resume(TIMESTAMP + 3.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 3);

    sut.resume(TIMESTAMP + 4.as_secs());

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 4);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: Some(POS_3.into()),
        heart_rate: Some(HEART_RATE + 3),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: Some(HEART_RATE + 4),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn section_creates_crossing_section() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.next_section(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 1);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);

    sut.next_section(TIMESTAMP + 2.as_secs());

    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 2);

    sut.next_section(TIMESTAMP + 3.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 3);

    sut.next_section(TIMESTAMP + 3.as_secs()); // this double call is intentional

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 4);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE + 1),
    };
    let meas2 = Measurement {
        timestamp: TIMESTAMP + 2.as_secs(),
        position: Some(POS_2.into()),
        heart_rate: Some(HEART_RATE + 2),
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: Some(POS_3.into()),
        heart_rate: Some(HEART_RATE + 3),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: Some(HEART_RATE + 4),
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn heart_rate_is_cached_for_up_to_three_measuremenst() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    let meas0 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(POS_0.into()),
        heart_rate: None,
    };
    let meas1 = Measurement {
        timestamp: TIMESTAMP + 1.as_secs(),
        position: Some(POS_1.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas2 = Measurement {
        timestamp: TIMESTAMP + 2.as_secs(),
        position: Some(POS_2.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: Some(POS_3.into()),
        heart_rate: Some(HEART_RATE),
    };
    let meas4 = Measurement {
        timestamp: TIMESTAMP + 4.as_secs(),
        position: Some(POS_4.into()),
        heart_rate: None,
    };

    let expected_content = format!(
        r#"{}
[[section]]
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section.measurement]]
{}
[[section.measurement]]
{}
"#,
        minimal_header(),
        toml::to_string_pretty::<Measurement>(&meas0.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas1.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas2.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas3.into()).unwrap(),
        toml::to_string_pretty::<Measurement>(&meas4.into()).unwrap(),
    );

    assert_eq!(file_content, expected_content);
}

#[test]
fn incremental_serialization_produces_same_result_as_full_serialization() {
    let pos = PositionInfo {
        timestamp: TIMESTAMP.as_millis() as i64,
        latitude: 13.3,
        longitude: 37.7,
        altitude: 111.1,
        horizontal_accuracy: 9,
        vertical_accuracy: 4,
    };

    let mut sut = Sut::new();

    sut.start(TIMESTAMP);
    sut.position_update(TIMESTAMP, pos);

    sut.tick(TIMESTAMP + 1.as_secs());
    sut.tick(TIMESTAMP + 2.as_secs());
    sut.tick(TIMESTAMP + 3.as_secs());

    sut.next_section(TIMESTAMP + 3.as_secs());

    sut.tick(TIMESTAMP + 4.as_secs());
    sut.tick(TIMESTAMP + 5.as_secs());
    sut.tick(TIMESTAMP + 6.as_secs());

    let file_content =
        String::from_utf8(sut.stop_data_writer(TIMESTAMP + 6.as_secs()).unwrap()).unwrap();

    let meas1 = Measurement {
        timestamp: TIMESTAMP,
        position: Some(pos.into()),
        heart_rate: None,
    };
    let meas2 = Measurement {
        timestamp: TIMESTAMP + 3.as_secs(),
        position: None,
        heart_rate: None,
    };
    let meas3 = Measurement {
        timestamp: TIMESTAMP + 6.as_secs(),
        position: None,
        heart_rate: None,
    };

    let section = vec![
        TrackSection {
            measurement: vec![meas1, meas2],
        },
        TrackSection {
            measurement: vec![meas2, meas3],
        },
    ];

    let record = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    let expected_content = toml::to_string_pretty(&record).unwrap() + "\n";

    assert_eq!(file_content, expected_content);
}
