// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::activity_recorder::tests::{Sut, ToDuration};
use crate::activity_recorder::PositionInfo;

use chrono::{Local, TimeZone};

use std::time::Duration;

const TIMESTAMP: Duration = Duration::from_secs(1123580000);

const HEART_RATE: u16 = 123;

const POS_0: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64,
    latitude: 13.11000,
    longitude: 42.11003,
    altitude: 73.36,
    horizontal_accuracy: 8,
    vertical_accuracy: 4,
};
const POS_1: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 1000,
    latitude: 13.11001,
    longitude: 42.11004,
    altitude: 73.37,
    horizontal_accuracy: 7,
    vertical_accuracy: 3,
};
const POS_2: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 2000,
    latitude: 13.11002,
    longitude: 42.11003,
    altitude: 73.38,
    horizontal_accuracy: 6,
    vertical_accuracy: 8,
};
const POS_3: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 3000,
    latitude: 13.11004,
    longitude: 42.11005,
    altitude: 73.39,
    horizontal_accuracy: 3,
    vertical_accuracy: 2,
};
const POS_4: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 4000,
    latitude: 13.11006,
    longitude: 42.11007,
    altitude: 73.31,
    horizontal_accuracy: 5,
    vertical_accuracy: 6,
};
const POS_5: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 5000,
    latitude: 13.11004,
    longitude: 42.11009,
    altitude: 73.32,
    horizontal_accuracy: 3,
    vertical_accuracy: 7,
};
const POS_6: PositionInfo = PositionInfo {
    timestamp: TIMESTAMP.as_millis() as i64 + 6000,
    latitude: 13.11002,
    longitude: 42.11007,
    altitude: 73.33,
    horizontal_accuracy: 4,
    vertical_accuracy: 2,
};

const GREAT_CIRCLE_DISTANCE: u64 = 1552;

fn minimal_header() -> String {
    format!(
        r#"[info]
version = {}
source = 'kuri'"#,
        1,
    )
}

#[derive(Default)]
struct ExpectedContent {
    start_time: Duration,
    duration_active: u64,
    duration_pause: u64,
    distance: u64,
    heart_rate_average: u16,
    elevation_up: u64,
    elevation_down: u64,
}

impl ExpectedContent {
    fn serialize(&self) -> String {
        let start_time = Local
            .timestamp(self.start_time.as_secs() as i64, 0)
            .format("%Y-%m-%dT%H:%M:%S");
        format!(
            r#"{}
start-time = '{}'

[activity]
type = 'running'
name = ''
description = ''

[statistics]
duration-active = {}
duration-pause = {}
distance = {}
heart-rate-average = {}
elevation-up = {}
elevation-down = {}
"#,
            minimal_header(),
            start_time,
            self.duration_active,
            self.duration_pause,
            self.distance,
            self.heart_rate_average,
            self.elevation_up,
            self.elevation_down
        )
    }
}

fn speed_average(sut: &Sut) -> f64 {
    *sut.amd().speed_average.peek()
}

fn pace_average(sut: &Sut) -> f64 {
    *sut.amd().pace_average.peek()
}

#[test]
fn start_and_stop_without_data_does_not_create_a_file() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    assert_eq!(sut.stop_stats(TIMESTAMP), None);
    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");
}

#[test]
fn start_and_stop_with_one_tick_creates_file_with_minimal_values() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.tick(TIMESTAMP);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP).unwrap()).unwrap();

    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_one_heart_rate_update_creates_file_with_minimal_values() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP).unwrap()).unwrap();

    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 0,
        duration_pause: 0,
        distance: 0,
        heart_rate_average: HEART_RATE,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_multiple_heart_rate_update_creates_file_with_average_heart_rate() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 3);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 9);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 2.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 2,
        duration_pause: 0,
        distance: 0,
        heart_rate_average: HEART_RATE + 4,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn measurements_before_start_are_discarded() {
    let pos = PositionInfo {
        timestamp: TIMESTAMP.as_millis() as i64 - 1000,
        latitude: 13.31,
        longitude: 42.24,
        altitude: 73.37,
        horizontal_accuracy: 7,
        vertical_accuracy: 3,
    };

    let mut sut = Sut::new();

    sut.heart_rate_update(TIMESTAMP - 3.as_secs(), HEART_RATE + 20);

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP - 2.as_secs(), pos);

    sut.tick(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP).unwrap()).unwrap();

    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 0,
        duration_pause: 0,
        distance: 0,
        heart_rate_average: HEART_RATE,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn measurements_after_stop_are_discarded() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP).unwrap()).unwrap();

    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE);

    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 0,
        duration_pause: 0,
        distance: 0,
        heart_rate_average: HEART_RATE,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_one_position_update_creates_file_with_minimal_values() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP).unwrap()).unwrap();

    assert_eq!(format!("{}", speed_average(&sut)), "0");
    assert_eq!(format!("{}", pace_average(&sut)), "0");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_two_position_updates_creates_file_with_position_related_data() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 1.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "5.5872");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "644.33");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 1,
        duration_pause: 0,
        distance: GREAT_CIRCLE_DISTANCE,
        heart_rate_average: 0,
        elevation_up: 10,
        ..Default::default()
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn start_and_stop_with_multiple_position_updates_creates_file_with_position_related_data() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.position_update(TIMESTAMP + 5.as_secs(), POS_5);
    sut.position_update(TIMESTAMP + 6.as_secs(), POS_6);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 6.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "9.3120");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "386.60");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 6,
        duration_pause: 0,
        distance: GREAT_CIRCLE_DISTANCE * 10,
        heart_rate_average: 0,
        elevation_up: 50,
        elevation_down: 80,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_without_resume_discards_all_measurements_after_pause() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 3);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 9);

    sut.pause(TIMESTAMP + 2.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 90);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 3.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "5.5872");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "644.33");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 2,
        duration_pause: 0,
        distance: GREAT_CIRCLE_DISTANCE * 2,
        heart_rate_average: HEART_RATE + 4,
        elevation_up: 20,
        elevation_down: 0,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_and_resume_on_same_timestamp_does_not_discards_any_measurement() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 3);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 9);

    sut.pause(TIMESTAMP + 2.as_secs());
    sut.resume(TIMESTAMP + 2.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 2);

    sut.pause(TIMESTAMP + 4.as_secs());
    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 6);
    sut.resume(TIMESTAMP + 4.as_secs());

    sut.position_update(TIMESTAMP + 5.as_secs(), POS_5);
    sut.heart_rate_update(TIMESTAMP + 5.as_secs(), HEART_RATE + 6);

    sut.pause(TIMESTAMP + 6.as_secs());
    sut.resume(TIMESTAMP + 6.as_secs());

    sut.position_update(TIMESTAMP + 6.as_secs(), POS_6);
    sut.heart_rate_update(TIMESTAMP + 6.as_secs(), HEART_RATE + 2);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 6.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "9.3120");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "386.60");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 6,
        duration_pause: 0,
        distance: GREAT_CIRCLE_DISTANCE * 10,
        heart_rate_average: HEART_RATE + 4,
        elevation_up: 50,
        elevation_down: 80,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_with_one_tick_gap_discards_one_measurement() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 3);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 9);

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 2);

    sut.pause(TIMESTAMP + 4.as_secs());

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 6);

    sut.resume(TIMESTAMP + 5.as_secs());

    sut.position_update(TIMESTAMP + 5.as_secs(), POS_5);
    sut.heart_rate_update(TIMESTAMP + 5.as_secs(), HEART_RATE + 6);

    sut.position_update(TIMESTAMP + 6.as_secs(), POS_6);
    sut.heart_rate_update(TIMESTAMP + 6.as_secs(), HEART_RATE + 2);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 6.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "8.9395");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "402.71");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 5,
        duration_pause: 1,
        distance: GREAT_CIRCLE_DISTANCE * 8,
        heart_rate_average: HEART_RATE + 4,
        elevation_up: 40,
        elevation_down: 80,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn pause_with_multiple_tick_gaps_discards_multiple_measurement() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.pause(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 3);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 9);

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);

    sut.resume(TIMESTAMP + 3.as_secs());

    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 2);

    sut.pause(TIMESTAMP + 4.as_secs());

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 6);

    sut.resume(TIMESTAMP + 5.as_secs());

    sut.position_update(TIMESTAMP + 5.as_secs(), POS_5);
    sut.heart_rate_update(TIMESTAMP + 5.as_secs(), HEART_RATE + 6);

    sut.position_update(TIMESTAMP + 6.as_secs(), POS_6);
    sut.heart_rate_update(TIMESTAMP + 6.as_secs(), HEART_RATE + 2);

    sut.resume(TIMESTAMP + 6.as_secs());

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 6.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "9.3120");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "386.60");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 3,
        duration_pause: 3,
        distance: GREAT_CIRCLE_DISTANCE * 5,
        heart_rate_average: HEART_RATE + 3,
        elevation_up: 20,
        elevation_down: 80,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn section_crossing_has_no_effect_on_statistics() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.next_section(TIMESTAMP + 1.as_secs());

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 3);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.heart_rate_update(TIMESTAMP + 2.as_secs(), HEART_RATE + 9);

    sut.next_section(TIMESTAMP + 2.as_secs());

    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);

    sut.next_section(TIMESTAMP + 3.as_secs());

    sut.heart_rate_update(TIMESTAMP + 3.as_secs(), HEART_RATE + 2);

    sut.next_section(TIMESTAMP + 3.as_secs()); // this double call is intentional

    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);
    sut.heart_rate_update(TIMESTAMP + 4.as_secs(), HEART_RATE + 6);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "8.3808");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "429.55");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 4,
        duration_pause: 0,
        distance: GREAT_CIRCLE_DISTANCE * 6,
        heart_rate_average: HEART_RATE + 4,
        elevation_up: 30,
        elevation_down: 80,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn large_gaps_in_position_info_are_handled_gracefully() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);

    sut.tick(TIMESTAMP + 1.as_secs());

    let mut pos_2 = POS_1;
    pos_2.timestamp = TIMESTAMP.as_millis() as i64 + 2000;
    sut.position_update(TIMESTAMP + 2.as_secs(), pos_2);

    for i in 3..=4 {
        sut.tick(TIMESTAMP + i.as_secs());
    }

    sut.pause(TIMESTAMP + 4.as_secs());
    sut.resume(TIMESTAMP + 5.as_secs());

    let mut pos_5 = POS_2;
    pos_5.timestamp = TIMESTAMP.as_millis() as i64 + 5000;
    sut.position_update(TIMESTAMP + 5.as_secs(), pos_5);

    for i in 6..20 {
        sut.tick(TIMESTAMP + i.as_secs());
    }

    let mut pos_20 = POS_3;
    pos_20.timestamp = TIMESTAMP.as_millis() as i64 + 20000;
    sut.position_update(TIMESTAMP + 20.as_secs(), pos_20);

    sut.tick(TIMESTAMP + 21.as_secs());

    let file_content =
        String::from_utf8(sut.stop_stats(TIMESTAMP + 21.as_secs()).unwrap()).unwrap();

    assert_eq!(format!("{:.4}", speed_average(&sut)), "0.8381");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "4295.53");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 20,
        duration_pause: 1,
        distance: GREAT_CIRCLE_DISTANCE * 3,
        heart_rate_average: 0,
        elevation_up: 20,
        elevation_down: 0,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}

#[test]
fn heart_rate_is_cached_for_up_to_three_measuremenst() {
    let mut sut = Sut::new();

    sut.start(TIMESTAMP);

    sut.position_update(TIMESTAMP, POS_0);
    sut.heart_rate_update(TIMESTAMP, HEART_RATE);

    sut.position_update(TIMESTAMP + 1.as_secs(), POS_1);
    sut.heart_rate_update(TIMESTAMP + 1.as_secs(), HEART_RATE + 4);

    sut.position_update(TIMESTAMP + 2.as_secs(), POS_2);
    sut.position_update(TIMESTAMP + 3.as_secs(), POS_3);
    sut.position_update(TIMESTAMP + 4.as_secs(), POS_4);

    let file_content = String::from_utf8(sut.stop_stats(TIMESTAMP + 4.as_secs()).unwrap()).unwrap();

    assert_eq!(sut.stats().heart_rate_sum_count, 4);

    assert_eq!(format!("{:.4}", speed_average(&sut)), "8.3808");
    assert_eq!(format!("{:.2}", pace_average(&sut)), "429.55");

    let expected_content = ExpectedContent {
        start_time: TIMESTAMP,
        duration_active: 4,
        duration_pause: 0,
        distance: GREAT_CIRCLE_DISTANCE * 6,
        heart_rate_average: HEART_RATE + 3,
        elevation_up: 30,
        elevation_down: 80,
    }
    .serialize();

    assert_eq!(file_content, expected_content);
}
