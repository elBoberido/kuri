// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::aggregator::ActivityMetaData;
use super::fusion::{Fusion, RecordingState, Tag, TaggedMeasurement};
use super::storage::{FileName, Storage, CURRENT_RECORDING_META_FILE_NAME};

use crate::records::{Measurement, Position};
use crate::statistics::great_circle_distance;

use std::collections::VecDeque;
use std::io::Write;
use std::marker::PhantomData;
use std::time::Duration;

struct GeoCalculation {
    timestamp: Duration,
    distance: u64,
}

pub(super) struct Statistics<S: Storage> {
    has_data: bool,
    timestamp_last_heart_rate_update: Duration,
    /// heart rate sum in bpm
    heart_rate_sum: u64,
    heart_rate_sum_count: u64,
    timestamp_last_geo_calculation: Duration,
    latest_unprocessed_position: Option<Position>,
    geo_calculations: VecDeque<GeoCalculation>,
    _phantom: PhantomData<S>,
}

impl<S: Storage<Backend = S>> Statistics<S> {
    pub fn new() -> Self {
        Self {
            has_data: false,
            timestamp_last_heart_rate_update: Duration::from_secs(0),
            heart_rate_sum: 0,
            heart_rate_sum_count: 0,
            timestamp_last_geo_calculation: Duration::from_secs(0),
            latest_unprocessed_position: None,
            geo_calculations: VecDeque::with_capacity(60),
            _phantom: Default::default(),
        }
    }

    pub fn save_statistics(&mut self, file_id: &str) {
        let file_name = format!("{}.meta.toml", file_id);
        S::make_persistent(
            CURRENT_RECORDING_META_FILE_NAME,
            FileName::MetaData(&file_name),
        );

        *self = Statistics::new();
    }
    pub fn discard_statistics(&mut self) {
        S::discard(CURRENT_RECORDING_META_FILE_NAME);

        *self = Statistics::new();
    }

    pub fn start(&mut self, fusion: &Fusion) {
        if fusion.recording_state() != RecordingState::Idle {
            println!("Error! Starting when not in Idle state!");
            return;
        }
    }

    pub fn stop(&mut self, fusion: &Fusion, amd: &mut ActivityMetaData) -> Option<S> {
        if !self.has_data {
            return None;
        }

        for m in fusion.measurements(self.timestamp_last_geo_calculation) {
            self.update_position_measurement(m, amd);
        }
        self.update_speed_and_pace(amd);

        for m in fusion.measurements(self.timestamp_last_heart_rate_update) {
            self.update_heart_rate_average(&m.measurement, amd);
        }

        self.update_time_range_statistics(amd);

        self.store_meta_data(amd)
    }

    pub(super) fn store_meta_data(&mut self, amd: &mut ActivityMetaData) -> Option<S> {
        let mut data_file = S::new_tmp_storage(CURRENT_RECORDING_META_FILE_NAME);
        data_file.as_mut().map(|buf| {
            write!(buf, "{}", toml::to_string_pretty(&amd.rec).unwrap())
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();
        });

        data_file.take().and_then(|buf| buf.into_inner().ok())
    }

    pub fn process(&mut self, fusion: &Fusion, amd: &mut ActivityMetaData) {
        self.has_data = true;

        let measurements = fusion.measurements(self.timestamp_last_geo_calculation);
        if !measurements.is_empty() {
            for m in measurements {
                if m.pending {
                    break;
                } else if m.about_to_drop {
                    self.update_position_measurement(m, amd);
                } else if m.measurement.position.is_some() {
                    self.update_position_measurement(m, amd);
                } else {
                    break;
                }
            }

            self.update_speed_and_pace(amd);
        }

        for m in fusion.measurements(self.timestamp_last_heart_rate_update) {
            if m.pending {
                break;
            } else if m.about_to_drop {
                self.update_heart_rate_average(&m.measurement, amd);
            } else if !m.heart_rate_from_cache && m.measurement.heart_rate.is_some() {
                self.update_heart_rate_average(&m.measurement, amd);
            } else {
                break;
            }
        }

        self.update_time_range_statistics(amd);
    }

    fn update_position_measurement(&mut self, m: &TaggedMeasurement, amd: &mut ActivityMetaData) {
        match (
            self.latest_unprocessed_position.as_ref(),
            m.measurement.position.as_ref(),
        ) {
            (Some(p1), Some(p2)) if m.tag != Tag::SectionStart => {
                let stat = &mut amd.rec.statistics;
                let distance_p1_p2: u64 = (great_circle_distance(p1, p2) * 1000.).round() as u64;
                stat.distance.set(stat.distance.peek() + distance_p1_p2);
                amd.distance_60s
                    .set(amd.distance_60s.peek() + distance_p1_p2);

                let elevation_difference_p1_p2 =
                    ((p2.altitude - p1.altitude) * 1000.).round() as i64;
                if elevation_difference_p1_p2 >= 0 {
                    stat.elevation_up
                        .set(*stat.elevation_up.peek() + elevation_difference_p1_p2 as u64);
                } else {
                    stat.elevation_down
                        .set(*stat.elevation_down.peek() + elevation_difference_p1_p2.abs() as u64);
                }

                self.geo_calculations.push_back(GeoCalculation {
                    timestamp: m.measurement.timestamp,
                    distance: distance_p1_p2,
                });

                self.timestamp_last_geo_calculation = m.measurement.timestamp;
                self.latest_unprocessed_position = Some(*p2)
            }
            (_, Some(p2)) => {
                self.timestamp_last_geo_calculation = m.measurement.timestamp;
                self.latest_unprocessed_position = Some(*p2);
            }
            _ => {
                if m.tag == Tag::SectionStart {
                    self.timestamp_last_geo_calculation = m.measurement.timestamp;
                    self.latest_unprocessed_position = m.measurement.position;
                }
            }
        }
    }

    fn update_speed_and_pace(&mut self, amd: &mut ActivityMetaData) {
        let stat = &mut amd.rec.statistics;
        let duration = *stat.duration_active.peek();
        let distance = *stat.distance.peek();
        if duration > 0 {
            let speed_in_mm_per_second = distance as f64 / duration as f64;
            const TO_KM_PER_HOUR: f64 = 3600. / 1000000.;
            amd.speed_average
                .set(speed_in_mm_per_second * TO_KM_PER_HOUR);

            if speed_in_mm_per_second > 0. {
                const TO_SECONDS_PER_KM: f64 = 1000000.;
                amd.pace_average
                    .set(speed_in_mm_per_second.recip() * TO_SECONDS_PER_KM);
            }
        }
    }

    fn update_heart_rate_average(&mut self, measurement: &Measurement, amd: &mut ActivityMetaData) {
        measurement.heart_rate.map(|heart_rate| {
            self.timestamp_last_heart_rate_update = measurement.timestamp;
            self.heart_rate_sum += heart_rate as u64;
            self.heart_rate_sum_count += 1;
            amd.rec
                .statistics
                .heart_rate_average
                .set((self.heart_rate_sum / self.heart_rate_sum_count.max(1)) as u16);
        });
    }

    fn update_time_range_statistics(&mut self, amd: &mut ActivityMetaData) {
        if let Some(GeoCalculation {
            timestamp,
            distance: _,
        }) = self.geo_calculations.back()
        {
            let cutoff_timestamp_secs = timestamp.as_secs() - 60;
            while let Some(c) = self.geo_calculations.pop_front() {
                if c.timestamp.as_secs() >= cutoff_timestamp_secs {
                    self.geo_calculations.push_front(c);
                    break;
                }

                amd.distance_60s.set(*amd.distance_60s.peek() - c.distance);
            }
        }
    }
}

#[cfg(test)]
mod tests;
