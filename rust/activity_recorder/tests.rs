// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::aggregator::{ActivityMetaData, Aggregator};
use super::statistics::Statistics;
use super::storage::{FileName, Storage};
use super::{Action, Command, PositionInfo};

use chrono::{TimeZone, Utc};
use crossbeam_channel::unbounded;

use std::io::BufWriter;
use std::time::Duration;

pub(crate) trait ToDuration {
    fn as_secs(self) -> Duration;
}

impl ToDuration for i32 {
    fn as_secs(self) -> Duration {
        Duration::from_secs(self as u64)
    }
}

impl ToDuration for i64 {
    fn as_secs(self) -> Duration {
        Duration::from_secs(self as u64)
    }
}

impl ToDuration for u64 {
    fn as_secs(self) -> Duration {
        Duration::from_secs(self)
    }
}

impl Storage for Vec<u8> {
    type Backend = Self;

    fn new_tmp_storage(_file_name: &str) -> Option<BufWriter<Self>> {
        Some(BufWriter::new(Self::new()))
    }

    fn get_available_activity_id(timestamp_recording_start: Duration) -> String {
        format!(
            "{}-1",
            Utc.timestamp(timestamp_recording_start.as_secs() as i64, 0)
                .format("%Y-%m-%dT%H:%M:%SZ")
        )
    }

    fn make_persistent(_old_file_name: &str, _new_file_name: FileName) {}
    fn make_persistent_compressed(_old_file_name: &str, _new_file_name: FileName) {}

    fn discard(_file_name: &str) {}
}

pub(super) struct Sut {
    aggregator: Aggregator<Vec<u8>>,
}

impl Sut {
    pub(super) fn new() -> Self {
        let (outbox, _) = unbounded();
        Self {
            aggregator: Aggregator::new(outbox),
        }
    }

    pub(super) fn stats(&self) -> &Statistics<Vec<u8>> {
        &self.aggregator.stats
    }

    pub(super) fn amd(&self) -> &ActivityMetaData {
        &self.aggregator.amd
    }

    pub(super) fn start(&mut self, timestamp: Duration) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::Start("running".into()),
        });
    }

    pub(super) fn stop_stats(&mut self, timestamp: Duration) -> Option<Vec<u8>> {
        self.aggregator
            .fusion
            .stop(timestamp, &mut self.aggregator.amd);
        self.aggregator
            .stats
            .stop(&self.aggregator.fusion, &mut self.aggregator.amd)
    }

    pub(super) fn stop_data_writer(&mut self, timestamp: Duration) -> Option<Vec<u8>> {
        self.aggregator
            .fusion
            .stop(timestamp, &mut self.aggregator.amd);
        self.aggregator.writer.stop(&self.aggregator.fusion)
    }

    pub(super) fn pause(&mut self, timestamp: Duration) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::Pause,
        });
    }

    pub(super) fn resume(&mut self, timestamp: Duration) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::Resume,
        });
    }

    pub(super) fn next_section(&mut self, timestamp: Duration) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::NextSection,
        });
    }

    pub(super) fn position_update(&mut self, timestamp: Duration, position_info: PositionInfo) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::PositionUpdate(position_info),
        });
    }

    pub(super) fn heart_rate_update(&mut self, timestamp: Duration, heart_rate: u16) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::HeartRateUpdate(heart_rate),
        });
    }

    pub(super) fn tick(&mut self, timestamp: Duration) {
        self.aggregator.handle_cmd(Command {
            timestamp,
            action: Action::Tick,
        });
    }
}
