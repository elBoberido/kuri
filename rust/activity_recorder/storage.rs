// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::paths::{KURI_META_DATA_DIR, KURI_TMP_DATA_DIR, KURI_TRACK_DATA_DIR};
use crate::utils::timestamp_to_id;

use std::fs::{metadata, File, OpenOptions};
use std::io::{BufReader, BufWriter, Write};
use std::path::PathBuf;
use std::time::Duration;

pub const CURRENT_RECORDING_DATA_FILE_NAME: &'static str = "recording.track-raw.toml";
pub const CURRENT_RECORDING_META_FILE_NAME: &'static str = "recording.meta.toml";

pub enum FileName<'a> {
    MetaData(&'a str),
    TrackData(&'a str),
}

pub fn meta_and_track_data_path(activity_id: &str) -> (PathBuf, PathBuf) {
    let mut meta_path = KURI_META_DATA_DIR.clone();
    meta_path.push(&format!("{}.meta.toml", &activity_id));

    let mut track_path = KURI_TRACK_DATA_DIR.clone();
    track_path.push(&format!("{}.track-raw.toml.zst", &activity_id));

    (meta_path, track_path)
}

pub trait Storage: Write {
    type Backend: Write;
    fn new_tmp_storage(file_name: &str) -> Option<BufWriter<Self::Backend>>;
    fn get_available_activity_id(timestamp_recording_start: Duration) -> String;
    fn make_persistent(old_file_name: &str, new_file_name: FileName);
    fn make_persistent_compressed(old_file_name: &str, new_file_name: FileName);
    fn discard(file_name: &str);
}

impl Storage for File {
    type Backend = Self;

    fn new_tmp_storage(file_name: &str) -> Option<BufWriter<Self>> {
        std::fs::create_dir_all(KURI_TMP_DATA_DIR.as_path())
            .map_err(|e| println!("Error creating tmp data dir path: {}", e))
            .ok()?;
        let mut path = KURI_TMP_DATA_DIR.clone();
        path.push(file_name);

        let file = OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(path)
            .map_err(|e| println!("Error creating recording file: {}", e))
            .ok()?;
        Some(BufWriter::new(file))
    }

    fn get_available_activity_id(timestamp_recording_start: Duration) -> String {
        let mut activity_id = String::new();

        for n in 1.. {
            activity_id = timestamp_to_id(timestamp_recording_start, n);
            let (meta_path, track_path) = meta_and_track_data_path(&activity_id);

            match (metadata(meta_path), metadata(track_path)) {
                (Ok(m), Ok(t)) if m.is_file() || t.is_file() => continue,
                _ => break,
            }
        }

        activity_id
    }

    fn make_persistent(old_file_name: &str, new_file_name: FileName) {
        let mut old_file_path = KURI_TMP_DATA_DIR.clone();
        old_file_path.push(old_file_name);

        let (data_path, new_file_name) = match new_file_name {
            FileName::MetaData(file_name) => (KURI_META_DATA_DIR.clone(), file_name),
            FileName::TrackData(file_name) => (KURI_TRACK_DATA_DIR.clone(), file_name),
        };
        std::fs::create_dir_all(data_path.as_path())
            .map_err(|e| println!("Error creating dir path: {}", e))
            .ok();
        let mut new_file_path = data_path;
        new_file_path.push(new_file_name);

        std::fs::rename(old_file_path, new_file_path)
            .map_err(|e| println!("Error making file persistent: {}", e))
            .ok();
    }

    fn make_persistent_compressed(old_file_name: &str, new_file_name: FileName) {
        let mut old_file_path = KURI_TMP_DATA_DIR.clone();
        old_file_path.push(old_file_name);

        let (data_path, new_file_name) = match new_file_name {
            FileName::MetaData(file_name) => (KURI_META_DATA_DIR.clone(), file_name),
            FileName::TrackData(file_name) => (KURI_TRACK_DATA_DIR.clone(), file_name),
        };
        std::fs::create_dir_all(data_path.as_path())
            .map_err(|e| println!("Error creating dir path: {}", e))
            .ok();
        let mut new_file_path = data_path;
        new_file_path.push(new_file_name);

        let old_file = OpenOptions::new()
            .read(true)
            .open(old_file_path)
            .map_err(|e| println!("Error reading source file: {}", e))
            .ok();

        let new_file = OpenOptions::new()
            .create_new(true)
            .truncate(true)
            .write(true)
            .open(new_file_path)
            .map_err(|e| println!("Error creating file: {}", e))
            .ok();

        if let (Some(old_file), Some(new_file)) = (old_file, new_file) {
            zstd::stream::copy_encode(BufReader::new(old_file), BufWriter::new(new_file), 0)
                .map_err(|e| println!("Error making file persistent: {}", e))
                .map(|_| Self::discard(old_file_name))
                .ok();
        }
    }

    fn discard(file_name: &str) {
        let mut path = KURI_TMP_DATA_DIR.clone();
        path.push(file_name);
        std::fs::remove_file(path)
            .map_err(|e| println!("Error discarding file: {}", e))
            .ok();
    }
}
