// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::activity_recorder::data_writer::DataWriter;
use crate::activity_recorder::fusion::Fusion;
use crate::activity_recorder::statistics::Statistics;
use crate::activity_recorder::storage::Storage;
use crate::activity_recorder::{Action, Artifact, Command};
use crate::value::Value;

use crossbeam_channel::Sender;

pub(super) struct ActivityMetaData {
    pub(super) recording: Value<bool>,
    pub(super) pausing: Value<bool>,
    pub(super) section_count: Value<u64>,
    /// duration in seconds
    pub(super) duration_in_section: Value<u64>,
    /// distance in mm
    pub(super) distance_60s: Value<u64>,
    /// speed in km/h
    pub(super) speed_average: Value<f64>,
    /// pace in s/km
    pub(super) pace_average: Value<f64>,
    /// elevation in mm
    pub(super) elevation: Value<i64>,
    /// heart rate in bpm
    pub(super) heart_rate: Value<u16>,
    /// recorded meta data
    pub(super) rec: crate::records::ActivityMetaData,
}

impl Default for ActivityMetaData {
    fn default() -> Self {
        Self {
            recording: Default::default(),
            pausing: Default::default(),
            section_count: Value::new(1),
            duration_in_section: Default::default(),
            distance_60s: Default::default(),
            speed_average: Default::default(),
            pace_average: Default::default(),
            elevation: Default::default(),
            heart_rate: Default::default(),
            rec: Default::default(),
        }
    }
}

pub(super) struct Aggregator<S: Storage> {
    pub(super) amd: ActivityMetaData,
    pub(super) outbox: Sender<Artifact>,
    pub(super) fusion: Fusion,
    pub(super) writer: DataWriter<S>,
    pub(super) stats: Statistics<S>,
    pub(super) has_send_artifacts: Option<()>,
}

impl<S: Storage<Backend = S>> Aggregator<S> {
    pub(super) fn new(outbox: Sender<Artifact>) -> Self {
        Self {
            amd: ActivityMetaData::default(),
            outbox,
            fusion: Fusion::new(),
            writer: DataWriter::new(),
            stats: Statistics::new(),
            has_send_artifacts: None,
        }
    }

    pub(super) fn handle_cmd(&mut self, cmd: Command) {
        match cmd.action {
            Action::Start(activity_type) => {
                if activity_type.is_empty() {
                    println!("Error! Starting without activity type!");
                    return;
                }
                self.amd.rec.activity.r#type = activity_type;
                self.fusion.start(cmd.timestamp);
                self.writer.start(&self.fusion);
                self.stats.start(&self.fusion);
            }
            Action::Stop => {
                self.fusion.stop(cmd.timestamp, &mut self.amd);
                self.writer.stop(&self.fusion);
                self.stats.stop(&self.fusion, &mut self.amd);
            }
            Action::Pause => {
                self.fusion.pause(cmd.timestamp, &mut self.amd);
            }
            Action::Resume => {
                self.fusion.resume(cmd.timestamp, &mut self.amd);
            }
            Action::NextSection => {
                self.fusion.next_section(cmd.timestamp, &mut self.amd);
            }
            Action::PositionUpdate(position_info) => {
                self.fusion
                    .position_update(cmd.timestamp, position_info, &mut self.amd);
                self.writer.process(&self.fusion);
                self.stats.process(&self.fusion, &mut self.amd);
            }
            Action::HeartRateUpdate(heart_rate) => {
                self.fusion
                    .heart_rate_update(cmd.timestamp, heart_rate, &mut self.amd);
                self.writer.process(&self.fusion);
                self.stats.process(&self.fusion, &mut self.amd);
            }
            Action::Tick => {
                self.fusion.tick(cmd.timestamp, &mut self.amd);
                self.writer.process(&self.fusion);
                self.stats.process(&self.fusion, &mut self.amd);
            }
            Action::SaveRecording(activity_name, activity_description) => {
                let timestamp_recording_start = self.fusion.timestamp_recording_start();
                if timestamp_recording_start.as_secs() != 0 {
                    // store activity name and description
                    if !activity_name.is_empty() || !activity_description.is_empty() {
                        self.amd.rec.activity.name = activity_name;
                        self.amd.rec.activity.description = activity_description;

                        self.stats.store_meta_data(&mut self.amd);
                    }

                    let activity_id = S::get_available_activity_id(timestamp_recording_start);

                    self.writer.save_recording(&activity_id);
                    // a statistics file having an older filesystem timestamp than the recording would mean that the statistics are out of date
                    // -> store statistics after the recording
                    self.stats.save_statistics(&activity_id);

                    self.outbox
                        .send(Artifact::ActivityIdLastSave(activity_id))
                        .map_err(|e| println!("Could not read track: {}", e))
                        .ok();
                    self.has_send_artifacts = Some(());
                }
                self.fusion.reset();
                self.amd = ActivityMetaData::default();
            }
            Action::DiscardRecording => {
                self.writer.discard_recording();
                self.stats.discard_statistics();
                self.fusion.reset();
                self.amd = ActivityMetaData::default();
            }
        }
    }
}
