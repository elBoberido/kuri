// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::paths::{
    KURI_CONFIG_DIR, KURI_DEFAULT_EXPORT_DESTINATION_DIR, KURI_DEFAULT_IMPORT_SOURCE_DIR,
};

use serde::{Deserialize, Serialize};

use std::fs::{metadata, OpenOptions};
use std::io::{BufWriter, Read, Write};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
struct Info {
    version: u64,
}

impl Default for Info {
    fn default() -> Self {
        Self { version: 1 }
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct General {}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct History {
    activity_type_filter: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Recording {
    activity_type: Option<String>,
    position_accuracy_threshold: Option<u64>,
    show_map: Option<bool>,
    screen_blanking_off: Option<bool>,
    display_mode: Option<u64>,
    auto_save: Option<bool>,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
#[repr(u64)]
enum MapCenterMode {
    Position = 0,
    Track = 1,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Map {
    center_mode: Option<MapCenterMode>,
    style: Option<String>,
    cache_size: Option<u64>,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
#[repr(u64)]
enum HrmSource {
    None = 0,
    Device = 1,
    Service = 2,
}

#[derive(Serialize, Deserialize, Copy, Clone, Debug)]
#[serde(rename_all = "kebab-case")]
#[repr(u64)]
enum BluetoothType {
    BlePublic = 0,
    BleRandom = 1,
    Classic = 2,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct HRM {
    source: Option<HrmSource>,
    device_name: Option<String>,
    device_address: Option<String>,
    bluetooth_type: Option<BluetoothType>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Migration {
    legacy_activities_imported: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Import {
    source_folder: Option<String>,
    override_imported_activities: Option<bool>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Export {
    destination_folder: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Strava {
    user_name: Option<String>,
    country: Option<String>,
    access_token: Option<String>,
    refresh_token: Option<String>,
    expiration_time: Option<u64>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
struct Settings {
    #[serde(skip)]
    modified: bool,
    info: Info,
    #[serde(default)]
    general: General,
    #[serde(default)]
    history: History,
    #[serde(default)]
    recording: Recording,
    #[serde(default)]
    map: Map,
    #[serde(default)]
    hrm: HRM,
    #[serde(default)]
    migration: Migration,
    #[serde(default)]
    import: Import,
    #[serde(default)]
    export: Export,
    #[serde(default)]
    strava: Strava,
}

impl Settings {
    fn load() -> std::io::Result<Box<Self>> {
        let mut config_file = KURI_CONFIG_DIR.clone();
        config_file.push("kuri.toml");
        let _ = metadata(&config_file)?;

        let mut serialized = String::new();
        OpenOptions::new()
            .read(true)
            .open(config_file)?
            .read_to_string(&mut serialized)?;

        Ok(Box::new(toml::from_str::<Self>(&serialized)?))
    }

    fn save(&mut self) {
        if !self.modified {
            return;
        }

        std::fs::create_dir_all(KURI_CONFIG_DIR.as_path())
            .map_err(|e| println!("Error creating config dir path: {}", e))
            .ok();

        let mut config_file = KURI_CONFIG_DIR.clone();
        config_file.push("kuri.toml");

        let _ = OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(config_file)
            .map_err(|e| println!("Error creating config file: {}", e))
            .map(|file| {
                let mut buf = BufWriter::new(file);

                write!(buf, "{}", toml::to_string_pretty(&self).unwrap())
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
            });
    }

    fn history_activity_type_filter(&self) -> String {
        self.history
            .activity_type_filter
            .as_deref()
            .unwrap_or("allactivities")
            .to_string()
    }
    fn history_set_activity_type_filter(&mut self, activity_type: &str) {
        self.modified = true;
        self.history.activity_type_filter = Some(activity_type.to_string());
    }

    fn rec_activity_type(&self) -> String {
        self.recording
            .activity_type
            .as_deref()
            .unwrap_or("running")
            .to_string()
    }
    fn rec_set_activity_type(&mut self, activity_type: &str) {
        self.modified = true;
        self.recording.activity_type = Some(activity_type.to_string());
    }

    /// accuracy in meter
    fn rec_position_accuracy_threshold(&self) -> u64 {
        self.recording.position_accuracy_threshold.unwrap_or(40)
    }
    /// accuracy in meter
    fn rec_set_position_accuracy_threshold(&mut self, accuracy: u64) {
        self.modified = true;
        self.recording.position_accuracy_threshold = Some(accuracy);
    }

    fn rec_show_map(&self) -> bool {
        self.recording.show_map.unwrap_or(true)
    }
    fn rec_set_show_map(&mut self, value: bool) {
        self.modified = true;
        self.recording.show_map = Some(value);
    }

    fn rec_screen_blanking_off(&self) -> bool {
        self.recording.screen_blanking_off.unwrap_or(false)
    }
    fn rec_set_screen_blanking_off(&mut self, value: bool) {
        self.modified = true;
        self.recording.screen_blanking_off = Some(value);
    }

    fn rec_display_mode(&self) -> u64 {
        self.recording.display_mode.unwrap_or(0)
    }
    fn rec_set_display_mode(&mut self, value: u64) {
        self.modified = true;
        self.recording.display_mode = Some(value);
    }

    fn rec_auto_save(&self) -> bool {
        self.recording.auto_save.unwrap_or(false)
    }
    fn rec_set_auto_save(&mut self, value: bool) {
        self.modified = true;
        self.recording.auto_save = Some(value);
    }

    fn map_center_mode(&self) -> u64 {
        self.map.center_mode.unwrap_or(MapCenterMode::Position) as u64
    }
    fn map_set_center_mode(&mut self, value: u64) {
        self.modified = true;
        let value = match value {
            0 => MapCenterMode::Position,
            1 => MapCenterMode::Track,
            _ => return,
        };
        self.map.center_mode = Some(value);
    }

    fn map_style(&self) -> String {
        self.map
            .style
            .as_deref()
            .unwrap_or("mapbox://styles/mapbox/outdoors-v10")
            .to_string()
    }
    fn map_set_style(&mut self, style: &str) {
        self.modified = true;
        self.map.style = Some(style.to_string());
    }

    /// cache size in MB
    fn map_cache_size(&self) -> u64 {
        self.map.cache_size.unwrap_or(50)
    }
    /// cache size in MB
    fn map_set_cache_size(&mut self, value: u64) {
        self.modified = true;
        self.map.cache_size = Some(value);
    }

    fn hrm_source(&self) -> u64 {
        self.hrm.source.unwrap_or(HrmSource::None) as u64
    }
    fn hrm_set_source(&mut self, value: u64) {
        self.modified = true;
        let value = match value {
            0 => HrmSource::None,
            1 => HrmSource::Device,
            2 => HrmSource::Service,
            _ => return,
        };
        self.hrm.source = Some(value);
    }

    fn hrm_device_name(&self) -> String {
        self.hrm.device_name.as_deref().unwrap_or("").to_string()
    }
    fn hrm_set_device_name(&mut self, device_name: &str) {
        self.modified = true;
        self.hrm.device_name = Some(device_name.to_string());
    }

    fn hrm_device_address(&self) -> String {
        self.hrm.device_address.as_deref().unwrap_or("").to_string()
    }
    fn hrm_set_device_address(&mut self, device_address: &str) {
        self.modified = true;
        self.hrm.device_address = Some(device_address.to_string());
    }

    fn hrm_bluetooth_type(&self) -> u64 {
        self.hrm.bluetooth_type.unwrap_or(BluetoothType::BleRandom) as u64
    }
    fn hrm_set_bluetooth_type(&mut self, value: u64) {
        self.modified = true;
        let value = match value {
            0 => BluetoothType::BlePublic,
            1 => BluetoothType::BleRandom,
            2 => BluetoothType::Classic,
            _ => return,
        };
        self.hrm.bluetooth_type = Some(value);
    }

    fn migration_legacy_activities_imported(&self) -> bool {
        self.migration.legacy_activities_imported.unwrap_or(false)
    }
    fn migration_set_legacy_activities_imported(&mut self, value: bool) {
        self.modified = true;
        self.migration.legacy_activities_imported = Some(value);
    }

    fn import_source_folder(&self) -> String {
        self.import
            .source_folder
            .as_deref()
            .unwrap_or(
                KURI_DEFAULT_IMPORT_SOURCE_DIR
                    .to_str()
                    .expect("Valid default import source dir"),
            )
            .to_string()
    }

    fn import_override_imported_activities(&self) -> bool {
        self.import.override_imported_activities.unwrap_or(false)
    }
    fn import_set_override_imported_activities(&mut self, value: bool) {
        self.modified = true;
        self.import.override_imported_activities = Some(value);
    }

    fn export_destination_folder(&self) -> String {
        self.export
            .destination_folder
            .as_deref()
            .unwrap_or(
                KURI_DEFAULT_EXPORT_DESTINATION_DIR
                    .to_str()
                    .expect("Valid default export destination dir"),
            )
            .to_string()
    }

    fn obfuscate_deobfuscate(token: &str) -> String {
        let mut token = token.to_string();
        unsafe {
            let token = token.as_bytes_mut();
            for i in 0..token.len() / 2 {
                let left = 2 * i;
                let right = left + 1;
                let temp = token[left];
                token[left] = token[right];
                token[right] = temp;
            }
        }
        token
    }

    fn strava_client_id(&self) -> u64 {
        // NOTE: Please don't reuse this strava client ID for your app if you copy this code but create a separate account
        // https://developers.strava.com/docs/getting-started/#account
        // https://developers.strava.com/docs/authentication/
        // https://developers.strava.com/docs/reference/
        75580
    }

    fn strava_client_secret(&self) -> String {
        Self::obfuscate_deobfuscate("55d77b7326d6e26ae64ef9e1ffe2e2cde20fd674")
    }

    fn strava_user_name(&self) -> String {
        self.strava.user_name.as_deref().unwrap_or("").to_string()
    }
    fn strava_set_user_name(&mut self, user_name: &str) {
        self.modified = true;
        self.strava.user_name = Some(user_name.to_string());
    }

    fn strava_country(&self) -> String {
        self.strava.country.as_deref().unwrap_or("").to_string()
    }
    fn strava_set_country(&mut self, country: &str) {
        self.modified = true;
        self.strava.country = Some(country.to_string());
    }

    fn strava_access_token(&self) -> String {
        Self::obfuscate_deobfuscate(self.strava.access_token.as_deref().unwrap_or("")).to_string()
    }
    fn strava_set_access_token(&mut self, access_token: &str) {
        self.modified = true;
        self.strava.access_token = Some(Self::obfuscate_deobfuscate(access_token));
    }

    fn strava_refresh_token(&self) -> String {
        Self::obfuscate_deobfuscate(self.strava.refresh_token.as_deref().unwrap_or("")).to_string()
    }
    fn strava_set_refresh_token(&mut self, refresh_token: &str) {
        self.modified = true;
        self.strava.refresh_token = Some(Self::obfuscate_deobfuscate(refresh_token));
    }

    fn strava_expiration_time(&self) -> u64 {
        self.strava.expiration_time.unwrap_or(0)
    }
    fn strava_set_expiration_time(&mut self, expiration_time: u64) {
        self.modified = true;
        self.strava.expiration_time = Some(expiration_time);
    }
}

fn load_settings() -> Box<Settings> {
    Settings::load().unwrap_or(Box::new(Settings::default()))
}

#[cxx::bridge(namespace = "kuri::rs")]
mod ffi {
    extern "Rust" {
        type Settings;

        fn load_settings() -> Box<Settings>;
        fn save(self: &mut Settings);

        // ==== general ====

        // ==== history ====

        fn history_activity_type_filter(self: &Settings) -> String;
        fn history_set_activity_type_filter(self: &mut Settings, activity_type: &str);

        // ==== recording ====

        fn rec_activity_type(self: &Settings) -> String;
        fn rec_set_activity_type(self: &mut Settings, activity_type: &str);

        fn rec_position_accuracy_threshold(self: &Settings) -> u64;
        fn rec_set_position_accuracy_threshold(self: &mut Settings, accuracy: u64);

        fn rec_show_map(self: &Settings) -> bool;
        fn rec_set_show_map(self: &mut Settings, value: bool);

        fn rec_screen_blanking_off(self: &Settings) -> bool;
        fn rec_set_screen_blanking_off(self: &mut Settings, value: bool);

        fn rec_display_mode(self: &Settings) -> u64;
        fn rec_set_display_mode(self: &mut Settings, value: u64);

        fn rec_auto_save(self: &Settings) -> bool;
        fn rec_set_auto_save(self: &mut Settings, value: bool);

        // ==== map ====

        fn map_center_mode(self: &Settings) -> u64;
        fn map_set_center_mode(self: &mut Settings, value: u64);

        fn map_style(self: &Settings) -> String;
        fn map_set_style(self: &mut Settings, style: &str);

        fn map_cache_size(self: &Settings) -> u64;
        fn map_set_cache_size(self: &mut Settings, value: u64);

        // ==== hrm ====

        fn hrm_source(self: &Settings) -> u64;
        fn hrm_set_source(self: &mut Settings, value: u64);

        fn hrm_device_name(self: &Settings) -> String;
        fn hrm_set_device_name(self: &mut Settings, device_name: &str);

        fn hrm_device_address(self: &Settings) -> String;
        fn hrm_set_device_address(self: &mut Settings, device_address: &str);

        fn hrm_bluetooth_type(self: &Settings) -> u64;
        fn hrm_set_bluetooth_type(self: &mut Settings, value: u64);

        // ==== migration ====

        fn migration_legacy_activities_imported(self: &Settings) -> bool;
        fn migration_set_legacy_activities_imported(self: &mut Settings, value: bool);

        // ==== import ====

        fn import_source_folder(self: &Settings) -> String;

        fn import_override_imported_activities(self: &Settings) -> bool;
        fn import_set_override_imported_activities(self: &mut Settings, value: bool);

        // ==== export ====

        fn export_destination_folder(self: &Settings) -> String;

        // ==== strava ====

        fn strava_client_id(self: &Settings) -> u64;
        fn strava_client_secret(self: &Settings) -> String;

        fn strava_user_name(self: &Settings) -> String;
        fn strava_set_user_name(self: &mut Settings, user_name: &str);

        fn strava_country(self: &Settings) -> String;
        fn strava_set_country(self: &mut Settings, country: &str);

        fn strava_access_token(self: &Settings) -> String;
        fn strava_set_access_token(self: &mut Settings, access_token: &str);

        fn strava_refresh_token(self: &Settings) -> String;
        fn strava_set_refresh_token(self: &mut Settings, refresh_token: &str);

        fn strava_expiration_time(self: &Settings) -> u64;
        fn strava_set_expiration_time(self: &mut Settings, expiration_time: u64);
    }
}
