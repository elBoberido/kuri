// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::records::{Position, Statistics, TrackSection};
use crate::value::Value;

fn hav(theta: f64) -> f64 {
    (theta / 2.).sin().powi(2)
}

fn ahav(a: f64) -> f64 {
    2. * a.sqrt().asin()
}

/// Calculates the great circle distance in meter. The formula is also known as haversine on wikipedia.
pub fn great_circle_distance(p1: &Position, p2: &Position) -> f64 {
    const MEAN_EARTH_RADIUS: f64 = 6_371_008.7714;

    MEAN_EARTH_RADIUS
        * ahav(
            hav((p2.latitude - p1.latitude).to_radians())
                + hav((p2.longitude - p1.longitude).to_radians())
                    * p1.latitude.to_radians().cos()
                    * p2.latitude.to_radians().cos(),
        )
}

pub fn statistics_from_track(sections: &Vec<TrackSection>) -> Statistics {
    const INCLUDING_RANGE_CORRECTION: u64 = 1u64;
    let mut duration_active = 0u64;
    sections
        .iter()
        .for_each(|s| match (s.measurement.first(), s.measurement.last()) {
            (Some(first), Some(last)) => {
                duration_active += last.timestamp.as_secs() - first.timestamp.as_secs()
                    + INCLUDING_RANGE_CORRECTION;
            }
            _ => {}
        });

    let mut duration_pause = 0u64;
    sections.windows(2).for_each(|s| {
        let left = &s[0];
        let right = &s[1];
        let time_diff = right.measurement.first().unwrap().timestamp.as_secs()
            - left.measurement.last().unwrap().timestamp.as_secs();
        if time_diff == 0 {
            duration_active = duration_active.saturating_sub(INCLUDING_RANGE_CORRECTION);
        } else {
            duration_pause += time_diff.saturating_sub(INCLUDING_RANGE_CORRECTION);
        }
    });

    let mut last_position_in_section = None;
    let mut distance = 0u64;
    let mut elevation_up = 0u64;
    let mut elevation_down = 0u64;
    let mut heart_rate_count = 0u64;
    let mut heart_rate_sum = 0u64;
    sections.iter().for_each(|s| {
        last_position_in_section = None;
        s.measurement.iter().for_each(|m| {
            match (last_position_in_section, m.position) {
                (Some(last), Some(current)) => {
                    distance += (great_circle_distance(&last, &current) * 1000.).round() as u64;

                    let elevation_difference =
                        ((current.altitude - last.altitude) * 1000.).round() as i64;
                    if elevation_difference >= 0 {
                        elevation_up += elevation_difference as u64;
                    } else {
                        elevation_down += elevation_difference.abs() as u64;
                    }

                    last_position_in_section = Some(current);
                }
                (None, Some(current)) => {
                    last_position_in_section = Some(current);
                }
                _ => {}
            }

            m.heart_rate.map(|hr| {
                heart_rate_count += 1;
                heart_rate_sum += hr as u64;
            });
        })
    });

    Statistics {
        duration_active: Value::new(duration_active),
        duration_pause: Value::new(duration_pause),
        distance: Value::new(distance),
        heart_rate_average: Value::new((heart_rate_sum / heart_rate_count.max(1)) as u16),
        elevation_up: Value::new(elevation_up),
        elevation_down: Value::new(elevation_down),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::records::Measurement;

    use std::time::Duration;

    const TIMESTAMP: Duration = Duration::from_secs(1123580000);
    const TIMESTAMP_INCREMENT: Duration = Duration::from_secs(1);

    const HEART_RATE: u16 = 123;

    const POS_0: Position = Position {
        latitude: 13.11000,
        longitude: 42.11003,
        altitude: 73.36,
        horizontal_accuracy: 8,
        vertical_accuracy: 4,
    };
    const POS_1: Position = Position {
        latitude: 13.11001,
        longitude: 42.11004,
        altitude: 73.37,
        horizontal_accuracy: 7,
        vertical_accuracy: 3,
    };
    const POS_2: Position = Position {
        latitude: 13.11002,
        longitude: 42.11003,
        altitude: 73.38,
        horizontal_accuracy: 6,
        vertical_accuracy: 8,
    };
    const POS_3: Position = Position {
        latitude: 13.11004,
        longitude: 42.11005,
        altitude: 73.39,
        horizontal_accuracy: 3,
        vertical_accuracy: 2,
    };
    const POS_4: Position = Position {
        latitude: 13.11006,
        longitude: 42.11007,
        altitude: 73.21,
        horizontal_accuracy: 5,
        vertical_accuracy: 6,
    };

    const POS_5: Position = Position {
        latitude: 13.21004,
        longitude: 42.21009,
        altitude: 75.14,
        horizontal_accuracy: 3,
        vertical_accuracy: 7,
    };
    const POS_6: Position = Position {
        latitude: 13.21002,
        longitude: 42.21007,
        altitude: 75.15,
        horizontal_accuracy: 4,
        vertical_accuracy: 2,
    };
    const POS_7: Position = Position {
        latitude: 13.21004,
        longitude: 42.21009,
        altitude: 75.16,
        horizontal_accuracy: 5,
        vertical_accuracy: 7,
    };
    const POS_8: Position = Position {
        latitude: 13.21003,
        longitude: 42.21008,
        altitude: 75.17,
        horizontal_accuracy: 4,
        vertical_accuracy: 8,
    };
    const POS_9: Position = Position {
        latitude: 13.21002,
        longitude: 42.21007,
        altitude: 75.18,
        horizontal_accuracy: 3,
        vertical_accuracy: 5,
    };
    const POS_10: Position = Position {
        latitude: 13.21003,
        longitude: 42.21006,
        altitude: 75.19,
        horizontal_accuracy: 6,
        vertical_accuracy: 2,
    };

    const GREAT_CIRCLE_DISTANCE: u64 = 1552;

    #[test]
    fn great_circle_distance_delivers_correct_result() {
        let distance_in_mm = (great_circle_distance(&POS_0, &POS_1) * 1000.).round() as u64;
        assert_eq!(distance_in_mm, GREAT_CIRCLE_DISTANCE);
    }

    #[test]
    fn statistics_from_track_delivers_correct_result() {
        let mut track = Vec::<TrackSection>::new();

        let mut section = TrackSection {
            measurement: Vec::new(),
        };
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 0 * TIMESTAMP_INCREMENT,
            position: Some(POS_0),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 1 * TIMESTAMP_INCREMENT,
            position: Some(POS_1),
            heart_rate: Some(HEART_RATE),
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 2 * TIMESTAMP_INCREMENT,
            position: Some(POS_2),
            heart_rate: None,
        });
        track.push(section);

        let mut section = TrackSection {
            measurement: Vec::new(),
        };
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 2 * TIMESTAMP_INCREMENT,
            position: Some(POS_2),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 3 * TIMESTAMP_INCREMENT,
            position: Some(POS_3),
            heart_rate: Some(HEART_RATE + 10),
        });
        track.push(section);

        let mut section = TrackSection {
            measurement: Vec::new(),
        };
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 3 * TIMESTAMP_INCREMENT,
            position: Some(POS_3),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 4 * TIMESTAMP_INCREMENT,
            position: Some(POS_4),
            heart_rate: None,
        });
        track.push(section);

        let mut section = TrackSection {
            measurement: Vec::new(),
        };
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 8 * TIMESTAMP_INCREMENT,
            position: Some(POS_5),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 9 * TIMESTAMP_INCREMENT,
            position: Some(POS_6),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 10 * TIMESTAMP_INCREMENT,
            position: Some(POS_7),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 11 * TIMESTAMP_INCREMENT,
            position: Some(POS_8),
            heart_rate: None,
        });
        track.push(section);

        let mut section = TrackSection {
            measurement: Vec::new(),
        };
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 11 * TIMESTAMP_INCREMENT,
            position: Some(POS_8),
            heart_rate: None,
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 12 * TIMESTAMP_INCREMENT,
            position: Some(POS_9),
            heart_rate: Some(HEART_RATE - 10),
        });
        section.measurement.push(Measurement {
            timestamp: TIMESTAMP + 13 * TIMESTAMP_INCREMENT,
            position: Some(POS_10),
            heart_rate: None,
        });
        track.push(section);

        let stats = statistics_from_track(&track);

        assert_eq!(*stats.duration_active.peek(), 11);
        assert_eq!(*stats.duration_pause.peek(), 3);
        assert_eq!(*stats.distance.peek(), GREAT_CIRCLE_DISTANCE * 13);
        assert_eq!(*stats.heart_rate_average.peek(), HEART_RATE);
        assert_eq!(*stats.elevation_up.peek(), 80);
        assert_eq!(*stats.elevation_down.peek(), 180);
    }
}
