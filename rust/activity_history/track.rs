// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::paths::KURI_TRACK_DATA_DIR;
use crate::records::TrackRecord;

use crate::activity_history::ffi::Measurement;

use std::fs::OpenOptions;
use std::io::Read;

#[derive(Debug)]
pub struct Track {
    pub id: String,
    pub record: TrackRecord,
}

impl Track {
    pub fn read_track(id: String) -> Result<Self, std::io::Error> {
        let mut path = KURI_TRACK_DATA_DIR.clone();
        path.push(format!("{}.track-raw.toml.zst", id));

        let file_compressed = OpenOptions::new().read(true).open(path)?;

        let mut record_serialized = String::new();
        zstd::stream::Decoder::new(file_compressed)?.read_to_string(&mut record_serialized)?;

        Ok(Self {
            id,
            record: toml::from_str(&record_serialized)?,
        })
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn total_point_count(&self) -> u64 {
        self.record
            .section
            .as_slice()
            .into_iter()
            .map(|s| s.measurement.len() as u64)
            .sum()
    }

    pub fn section_count(&self) -> u64 {
        self.record.section.len() as u64
    }

    pub fn section_point_count(&self, section: u64) -> u64 {
        self.record.section[section as usize].measurement.len() as u64
    }

    pub fn measurement_at(&self, section: u64, index: u64) -> Measurement {
        let section = section as usize;
        let index = index as usize;
        let mut m = Measurement::default();

        let measurement = &self.record.section[section as usize].measurement[index as usize];

        m.timestamp = measurement.timestamp.as_millis() as i64;

        measurement.position.as_ref().map(|p| {
            m.position_valid = true;
            m.latitude = p.latitude;
            m.longitude = p.longitude;
            m.altitude = p.altitude;
            m.horizontal_accuracy = p.horizontal_accuracy;
            m.vertical_accuracy = p.vertical_accuracy;
        });

        measurement.heart_rate.map(|h| m.heart_rate = h);

        m
    }
}
