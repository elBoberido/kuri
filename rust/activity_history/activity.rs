// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::paths::KURI_META_DATA_DIR;
use crate::records::ActivityMetaData;

use super::IntervalSummaryId;

use std::fs::{read_dir, OpenOptions};
use std::io::{BufWriter, Read, Write};
use std::path::PathBuf;

#[derive(Debug)]
pub struct Activity {
    pub id: String,
    pub interval_id: IntervalSummaryId,
    pub meta: ActivityMetaData,
}

impl Activity {
    pub fn read_all_activities<F>(mut on_new_activity: F) -> Result<(), std::io::Error>
    where
        F: FnMut(Activity),
    {
        read_dir(KURI_META_DATA_DIR.as_path()).map(|entries| {
            let mut files: Vec<_> = entries.map(|e| e.expect("Valid file").path()).collect();
            files.sort();
            files.into_iter().for_each(|file| {
                Self::read_activity(file)
                    .map(|activity| {
                        on_new_activity(activity);
                    })
                    .map_err(|e| println!("Could not read activity meta data: {}", e))
                    .ok();
            });
        })
    }

    pub fn read_activity_by_id(id: &str) -> Result<Activity, std::io::Error> {
        let mut path = KURI_META_DATA_DIR.clone();
        path.push(format!("{}.meta.toml", id));

        Self::read_activity(path)
    }

    fn read_activity(file: PathBuf) -> Result<Activity, std::io::Error> {
        let id = file
            .file_name()
            .ok_or(std::io::Error::new(
                std::io::ErrorKind::Other,
                "File with invalid id",
            ))?
            .to_str()
            .ok_or(std::io::Error::new(
                std::io::ErrorKind::Other,
                "File with invalid id",
            ))?
            .strip_suffix(".meta.toml")
            .ok_or(std::io::Error::new(
                std::io::ErrorKind::Other,
                "File with invalid id",
            ))?
            .into();

        let mut amd_serialized = String::new();
        OpenOptions::new()
            .read(true)
            .open(file)?
            .read_to_string(&mut amd_serialized)?;

        let meta = toml::from_str::<ActivityMetaData>(&amd_serialized)?;
        let interval_id = IntervalSummaryId::from_start_time(&meta.info.start_time);

        Ok(Activity {
            id,
            interval_id,
            meta,
        })
    }

    pub fn save_changes(&mut self) {
        let mut path = KURI_META_DATA_DIR.clone();
        path.push(format!("{}.meta.toml", self.id));

        OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(path)
            .map(|file| {
                let mut buf = BufWriter::new(file);
                write!(buf, "{}", toml::to_string_pretty(&self.meta).unwrap())
                    .map_err(|e| println!("Error occured: {:?}", e))
                    .ok();
            })
            .map_err(|e| println!("Error opening activity meta data file: {}", e))
            .ok();
    }

    pub fn set_activity_type(&mut self, activity_type: &str) {
        self.meta.activity.r#type = activity_type.into();
    }

    pub fn set_activity_name(&mut self, activity_name: &str) {
        self.meta.activity.name = activity_name.into();
    }

    pub fn set_activity_description(&mut self, activity_description: &str) {
        self.meta.activity.description = activity_description.into();
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn interval_id(&self) -> IntervalSummaryId {
        self.interval_id
    }

    pub fn start_time(&self) -> &str {
        &self.meta.info.start_time
    }

    pub fn activity_type(&self) -> &str {
        &self.meta.activity.r#type
    }

    pub fn name(&self) -> &str {
        &self.meta.activity.name
    }

    pub fn description(&self) -> &str {
        &self.meta.activity.description
    }

    /// duration in seconds
    pub fn duration_active(&self) -> u64 {
        *self.meta.statistics.duration_active.peek()
    }

    /// duration in seconds
    pub fn duration_pause(&self) -> u64 {
        *self.meta.statistics.duration_pause.peek()
    }

    /// distance in mm
    pub fn distance(&self) -> u64 {
        *self.meta.statistics.distance.peek()
    }

    /// elevation in mm
    pub fn elevation_up(&self) -> u64 {
        *self.meta.statistics.elevation_up.peek()
    }

    /// elevation in mm
    pub fn elevation_down(&self) -> u64 {
        *self.meta.statistics.elevation_down.peek()
    }

    /// speed in km/h
    pub fn speed_average(&self) -> f64 {
        if self.duration_active() > 0 {
            let speed_in_mm_per_second = self.distance() as f64 / self.duration_active() as f64;
            const TO_KM_PER_HOUR: f64 = 3600. / 1000000.;
            speed_in_mm_per_second * TO_KM_PER_HOUR
        } else {
            0.
        }
    }

    /// pace in s/km
    pub fn pace_average(&self) -> f64 {
        if self.duration_active() > 0 {
            let speed_in_mm_per_second = self.distance() as f64 / self.duration_active() as f64;
            if speed_in_mm_per_second > 0. {
                const TO_SECONDS_PER_KM: f64 = 1000000.;
                speed_in_mm_per_second.recip() * TO_SECONDS_PER_KM
            } else {
                0.
            }
        } else {
            0.
        }
    }

    /// heart rate in bpm
    pub fn heart_rate_average(&self) -> u16 {
        *self.meta.statistics.heart_rate_average.peek()
    }
}
