// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

mod aggregator;
use crate::activity_recorder::aggregator::{ActivityMetaData, Aggregator};

mod data_writer;
mod fusion;
mod statistics;
pub mod storage;

use crate::events;

use chrono::Utc;
use crossbeam_channel::{bounded, unbounded, Receiver, Sender};

use std::fs::File;
use std::thread::{self, JoinHandle};
use std::time::Duration;

pub use ffi::{
    ActivityRecorder, ActivityRecorderEvents, AsyncEventsActivityRecorder, PositionInfo,
    SyncEventsActivityRecorder,
};

#[derive(Debug)]
enum Action {
    Start(String),
    Stop,
    Pause,
    Resume,
    NextSection,
    PositionUpdate(PositionInfo),
    HeartRateUpdate(u16),
    Tick,
    SaveRecording(String, String),
    DiscardRecording,
}

#[derive(Debug)]
struct Command {
    timestamp: Duration,
    action: Action,
}

#[derive(Debug)]
enum Artifact {
    ActivityIdLastSave(String),
}

pub struct ActivityRecorderStore {
    worker_queue: Option<Sender<Command>>,
    worker_th_jh: Option<JoinHandle<()>>,
    artifacts: Receiver<Artifact>,
    last_stored_activity_id: String,
    activity_type: String,
    activity_name: String,
    activity_description: String,
}

impl Drop for ActivityRecorderStore {
    fn drop(&mut self) {
        self.worker_queue = None;
        self.worker_th_jh.take().map(|worker_th_jh| {
            worker_th_jh
                .join()
                .map_err(|e| {
                    println!("Error joining worker thread: {:?}", e);
                    e
                })
                .ok()
        });
    }
}

pub fn new_activity_recorder(events: ActivityRecorderEvents) -> ActivityRecorder {
    ActivityRecorder::new(events)
}

impl ActivityRecorder {
    fn new(events: ActivityRecorderEvents) -> Self {
        let (sender, receiver) = bounded(100);
        let (outbox, inbox) = unbounded();
        let worker_th_jh = {
            let receiver = receiver.clone();
            let events = events.async_events;
            thread::spawn(move || {
                Self::command_handler(receiver, outbox, events);
            })
        };
        Self {
            store: Box::new(ActivityRecorderStore {
                worker_queue: Some(sender),
                worker_th_jh: Some(worker_th_jh),
                artifacts: inbox,
                last_stored_activity_id: "".into(),
                activity_type: "".into(),
                activity_name: "".into(),
                activity_description: "".into(),
            }),
            events: events.sync_events,
        }
    }

    fn command_handler(
        receiver: Receiver<Command>,
        outbox: Sender<Artifact>,
        mut events: AsyncEventsActivityRecorder,
    ) {
        let mut aggregator = Aggregator::<File>::new(outbox);
        while let Ok(cmd) = receiver.recv() {
            aggregator.handle_cmd(cmd);

            Self::notify(&mut events, &mut aggregator.amd);

            aggregator
                .has_send_artifacts
                .take()
                .map(|_| events.new_artifacts.pin_mut().notify());
        }
    }

    fn notify(events: &mut AsyncEventsActivityRecorder, amd: &mut ActivityMetaData) {
        amd.recording
            .when_changed(|&rec| events.recording.pin_mut().notify(rec));
        amd.pausing
            .when_changed(|&pausing| events.pausing.pin_mut().notify(pausing));
        amd.section_count
            .when_changed(|&count| events.sectionCount.pin_mut().notify(count));
        amd.duration_in_section
            .when_changed(|&dur| events.durationInSection.pin_mut().notify(dur));
        amd.distance_60s
            .when_changed(|&d| events.distance60s.pin_mut().notify(d));
        amd.speed_average
            .when_changed(|&s| events.speedAverage.pin_mut().notify(s));
        amd.pace_average
            .when_changed(|&p| events.paceAverage.pin_mut().notify(p));
        amd.elevation
            .when_changed(|&e| events.elevation.pin_mut().notify(e));
        amd.heart_rate
            .when_changed(|&hr| events.heartRate.pin_mut().notify(hr));

        let stat = &mut amd.rec.statistics;
        stat.duration_active
            .when_changed(|&dur| events.duration.pin_mut().notify(dur));
        stat.heart_rate_average
            .when_changed(|&hr| events.heartRateAverage.pin_mut().notify(hr));
        stat.distance
            .when_changed(|&d| events.distance.pin_mut().notify(d));
        stat.elevation_up
            .when_changed(|&e| events.elevationUp.pin_mut().notify(e));
        stat.elevation_down
            .when_changed(|&e| events.elevationDown.pin_mut().notify(e));
    }

    pub fn shutdown(&mut self) {
        self.store.worker_queue = None;
    }
    pub fn process_artifacts(&mut self) {
        while let Ok(artifact) = self.store.artifacts.try_recv() {
            match artifact {
                Artifact::ActivityIdLastSave(activity_id) => {
                    self.store.last_stored_activity_id = activity_id;
                    self.events.new_activity_available.pin_mut().notify();
                }
            }
        }
    }

    fn send(&mut self, action: Action) {
        let _ = self.store.worker_queue.as_mut().map(|sender| {
            let timestamp = Duration::from_secs(Utc::now().timestamp() as u64);
            sender.send(Command { timestamp, action })
        });
    }

    // TODO use a macro to create all this functions

    pub fn start(&mut self) {
        self.send(Action::Start(self.store.activity_type.clone()));
    }

    pub fn stop(&mut self) {
        self.send(Action::Stop);
    }

    pub fn pause(&mut self) {
        self.send(Action::Pause);
    }

    pub fn resume(&mut self) {
        self.send(Action::Resume);
    }

    pub fn next_section(&mut self) {
        self.send(Action::NextSection);
    }

    pub fn position_update(&mut self, position_info: PositionInfo) {
        self.send(Action::PositionUpdate(position_info));
    }

    pub fn heart_rate_update(&mut self, heart_rate: u16) {
        self.send(Action::HeartRateUpdate(heart_rate));
    }

    pub fn tick(&mut self) {
        self.send(Action::Tick);
    }

    pub fn set_activity_type(&mut self, activity_type: &str) {
        self.store.activity_type = activity_type.into();
    }

    pub fn set_activity_name(&mut self, activity_name: &str) {
        self.store.activity_name = activity_name.into();
    }

    pub fn set_activity_description(&mut self, activity_description: &str) {
        self.store.activity_description = activity_description.into();
    }

    pub fn save_recording(&mut self) {
        self.send(Action::SaveRecording(
            self.store.activity_name.clone(),
            self.store.activity_description.clone(),
        ));
    }

    pub fn discard_recording(&mut self) {
        self.send(Action::DiscardRecording);
    }

    pub fn last_stored_activity_id(&mut self) -> &str {
        &self.store.last_stored_activity_id
    }
}

pub fn new_activity_recorder_events() -> ActivityRecorderEvents {
    ActivityRecorderEvents {
        sync_events: SyncEventsActivityRecorder {
            new_activity_available: events::ffi::new_event_void(),
        },
        async_events: AsyncEventsActivityRecorder {
            new_artifacts: events::ffi::new_event_void(),
            recording: events::ffi::new_event_bool(),
            pausing: events::ffi::new_event_bool(),
            duration: events::ffi::new_event_u64(),
            sectionCount: events::ffi::new_event_u64(),
            durationInSection: events::ffi::new_event_u64(),
            heartRate: events::ffi::new_event_u16(),
            heartRateAverage: events::ffi::new_event_u16(),
            distance: events::ffi::new_event_u64(),
            distance60s: events::ffi::new_event_u64(),
            elevation: events::ffi::new_event_i64(),
            elevationUp: events::ffi::new_event_u64(),
            elevationDown: events::ffi::new_event_u64(),
            speedAverage: events::ffi::new_event_f64(),
            paceAverage: events::ffi::new_event_f64(),
        },
    }
}
unsafe impl Send for AsyncEventsActivityRecorder {}

#[cxx::bridge(namespace = "kuri::rs")]
mod ffi {
    pub struct ActivityRecorder {
        store: Box<ActivityRecorderStore>,
        events: SyncEventsActivityRecorder,
    }

    pub struct ActivityRecorderEvents {
        pub sync_events: SyncEventsActivityRecorder,
        pub async_events: AsyncEventsActivityRecorder,
    }

    pub struct SyncEventsActivityRecorder {
        pub new_activity_available: UniquePtr<EventVoid>,
    }

    pub struct AsyncEventsActivityRecorder {
        pub new_artifacts: UniquePtr<EventVoid>,
        pub recording: UniquePtr<EventBool>,
        pub pausing: UniquePtr<EventBool>,
        pub duration: UniquePtr<EventU64>,
        pub sectionCount: UniquePtr<EventU64>,
        pub durationInSection: UniquePtr<EventU64>,
        pub heartRate: UniquePtr<EventU16>,
        pub heartRateAverage: UniquePtr<EventU16>,
        pub distance: UniquePtr<EventU64>,
        pub distance60s: UniquePtr<EventU64>,
        pub elevation: UniquePtr<EventI64>,
        pub elevationUp: UniquePtr<EventU64>,
        pub elevationDown: UniquePtr<EventU64>,
        pub speedAverage: UniquePtr<EventF64>,
        pub paceAverage: UniquePtr<EventF64>,
    }

    #[derive(Debug, Clone, Copy)]
    pub struct PositionInfo {
        pub timestamp: i64,
        pub latitude: f64,
        pub longitude: f64,
        pub altitude: f32,
        pub horizontal_accuracy: u16,
        pub vertical_accuracy: u16,
    }

    extern "Rust" {
        type ActivityRecorderStore;

        fn new_activity_recorder_events() -> ActivityRecorderEvents;

        fn new_activity_recorder(events: ActivityRecorderEvents) -> ActivityRecorder;

        fn shutdown(self: &mut ActivityRecorder);

        fn process_artifacts(self: &mut ActivityRecorder);

        // TODO check if a macro could be used to create all of this functions
        fn start(self: &mut ActivityRecorder);
        fn stop(self: &mut ActivityRecorder);
        fn pause(self: &mut ActivityRecorder);
        fn resume(self: &mut ActivityRecorder);
        fn next_section(self: &mut ActivityRecorder);
        fn position_update(self: &mut ActivityRecorder, position_info: PositionInfo);
        fn heart_rate_update(self: &mut ActivityRecorder, heart_rate: u16);
        fn tick(self: &mut ActivityRecorder);
        fn set_activity_type(self: &mut ActivityRecorder, activity_type: &str);
        fn set_activity_name(self: &mut ActivityRecorder, activity_name: &str);
        fn set_activity_description(self: &mut ActivityRecorder, activity_description: &str);
        fn save_recording(self: &mut ActivityRecorder);
        fn discard_recording(self: &mut ActivityRecorder);

        fn last_stored_activity_id(self: &mut ActivityRecorder) -> &str;
    }

    #[namespace = "kuri::ffi"]
    unsafe extern "C++" {
        include!("kuri-rs/ffi/events.h");
        type EventVoid = crate::events::ffi::EventVoid;
        type EventBool = crate::events::ffi::EventBool;
        type EventU16 = crate::events::ffi::EventU16;
        type EventU64 = crate::events::ffi::EventU64;
        type EventI64 = crate::events::ffi::EventI64;
        type EventF64 = crate::events::ffi::EventF64;
    }
}

#[cfg(test)]
mod tests;
