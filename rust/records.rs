// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::value::Value;

use serde::{Deserialize, Deserializer, Serialize, Serializer};

use std::time::Duration;

// ==== meta data structs ====

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct MetaInfo {
    pub version: u64,
    pub source: String,
    pub start_time: String,
    pub import_sha256: Option<String>,
}

impl Default for MetaInfo {
    fn default() -> Self {
        Self {
            version: 1,
            source: "kuri".into(),
            start_time: String::new(),
            import_sha256: None,
        }
    }
}

pub type ActivityType = String;

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
pub struct Activity {
    pub r#type: ActivityType,
    pub name: String,
    pub description: String,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
pub struct Statistics {
    /// duration in seconds
    pub duration_active: Value<u64>,
    /// duration in seconds
    pub duration_pause: Value<u64>,
    /// distance in mm
    pub distance: Value<u64>,
    /// heart rate in bpm
    pub heart_rate_average: Value<u16>,
    /// elevation up in mm
    pub elevation_up: Value<u64>,
    /// elevation down in mm
    pub elevation_down: Value<u64>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "kebab-case")]
pub struct ActivityMetaData {
    pub info: MetaInfo,
    pub activity: Activity,
    pub statistics: Statistics,
}

// ==== track data structs ====

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct TrackInfo {
    pub version: u64,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone, Copy)]
#[serde(rename_all = "kebab-case")]
pub struct Position {
    pub latitude: f64,
    pub longitude: f64,
    pub altitude: f32,
    pub horizontal_accuracy: u16,
    pub vertical_accuracy: u16,
}

mod serialize_timestamp {
    use super::*;
    pub(super) fn serialize<S>(t: &Duration, s: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        s.serialize_u64(t.as_millis() as u64)
    }

    pub(super) fn deserialize<'de, D>(d: D) -> Result<Duration, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Duration::from_millis(u64::deserialize(d)?))
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
#[serde(rename_all = "kebab-case")]
pub struct Measurement {
    #[serde(with = "serialize_timestamp")]
    pub timestamp: Duration,
    #[serde(flatten)]
    pub position: Option<Position>,
    pub heart_rate: Option<u16>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct TrackSection {
    pub measurement: Vec<Measurement>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub struct TrackRecord {
    pub info: TrackInfo,
    pub section: Vec<TrackSection>,
}
