// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use crate::records::TrackSection;

use chrono::{TimeZone, Utc};

use std::fs::OpenOptions;
use std::io::Write;
use std::path::PathBuf;

pub fn track_to_gpx(
    name: &str,
    description: &str,
    r#type: &str,
    start_time: &str,
    sections: &[TrackSection],
) -> String {
    let mut gpx = String::with_capacity(100);
    const GPX_HEADER: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1"
version="1.1" Creator="Kuri"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1
http://www.topografix.com/GPX/1/1/gpx.xsd
http://www.garmin.com/xmlschemas/GpxExtensions/v3
http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd
http://www.garmin.com/xmlschemas/TrackPointExtension/v1
http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd"
xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
"#;

    gpx.push_str(GPX_HEADER);
    gpx.push_str(&format!("<metadata>\n"));
    gpx.push_str(&format!("<name><![CDATA[{}]]></name>\n", name));
    gpx.push_str(&format!("<desc><![CDATA[{}]]></desc>\n", description));
    gpx.push_str(&format!("<extensions>\n"));
    gpx.push_str(&format!(
        "<kuri version=\"1\" local_start_time=\"{}\" activity=\"{}\"/>\n",
        start_time, r#type
    ));
    gpx.push_str(&format!("</extensions>\n"));
    gpx.push_str(&format!("</metadata>\n"));
    gpx.push_str(&format!("<trk>\n"));

    sections.iter().for_each(|section| {
        gpx.push_str(&format!("<trkseg>\n"));
        section.measurement.iter().for_each(|m| {
            if let Some(p) = m.position {
                gpx.push_str(&format!(
                    "<trkpt lat=\"{}\" lon=\"{}\">\n",
                    p.latitude, p.longitude
                ));
                gpx.push_str(&format!(
                    "<time>{}.{:#03}Z</time>\n",
                    Utc.timestamp(m.timestamp.as_secs() as i64, 0)
                        .format("%Y-%m-%dT%H:%M:%S"),
                    m.timestamp.subsec_millis()
                ));
                gpx.push_str(&format!("<ele>{}</ele>\n", p.altitude));
                gpx.push_str(&format!("<extensions>\n"));
                gpx.push_str(&format!("<h_acc>{}</h_acc>\n", p.horizontal_accuracy));
                gpx.push_str(&format!("<v_acc>{}</v_acc>\n", p.vertical_accuracy));

                if let Some(hr) = m.heart_rate {
                    gpx.push_str(&format!("<gpxtpx:TrackPointExtension>\n"));
                    gpx.push_str(&format!("<gpxtpx:hr>{}</gpxtpx:hr>\n", hr));
                    gpx.push_str(&format!("</gpxtpx:TrackPointExtension>\n"));
                }

                gpx.push_str(&format!("</extensions>\n"));
                gpx.push_str(&format!("</trkpt>\n"));
            }
        });
        gpx.push_str(&format!("</trkseg>\n"));
    });

    gpx.push_str(&format!("</trk>\n"));
    gpx.push_str(&format!("</gpx>\n"));

    gpx
}

pub fn export_activity_to_path(
    path: PathBuf,
    activity_id: &str,
    name: &str,
    description: &str,
    r#type: &str,
    start_time: &str,
    sections: &[TrackSection],
) -> Result<(), std::io::Error> {
    let mut file_path = path.clone();
    file_path.push(&format!("{}.gpx", activity_id));

    for n in 1.. {
        if !file_path.is_file() {
            break;
        }

        file_path = path.clone();
        file_path.push(&format!("{}--{}.gpx", activity_id, n));
    }

    let mut file = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open(file_path)?;

    let gpx = track_to_gpx(name, description, r#type, start_time, sections);
    write!(file, "{}", gpx)?;

    Ok(())
}

#[cfg(test)]
mod tests;
