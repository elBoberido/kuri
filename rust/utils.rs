// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use chrono::{TimeZone, Utc};

use std::time::Duration;

pub fn timestamp_to_id(timestamp: Duration, serial_number: u64) -> String {
    format!(
        "{}-{}",
        Utc.timestamp(timestamp.as_secs() as i64, 0)
            .format("%Y-%m-%dT%H:%M:%SZ"),
        serial_number
    )
}
