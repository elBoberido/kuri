// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

pub mod activity;
pub use crate::activity_history::activity::Activity;

pub mod track;
pub use crate::activity_history::track::Track;

use crate::exporter;
use crate::importer::{self, OnActivityPresent};

use crate::events;
use crate::paths::{KURI_LEGACY_DATA_DIR, KURI_META_DATA_DIR, KURI_TRACK_DATA_DIR};

use crate::records::ActivityType;

use crossbeam_channel::{bounded, unbounded, Receiver, Sender};

use std::collections::hash_map::HashMap;
use std::path::PathBuf;
use std::thread::{self, JoinHandle};

pub use ffi::{
    ActivityHistory, ActivitySummary, ArtifactAndHistoryEvents, ArtifactEvent, HistoryEvents,
    IntervalSummaryId,
};

#[derive(Debug)]
enum Command {
    LoadAllActivities,
    LoadActivity(String),
    LoadTrack(String),
    ImportActivities {
        source_folder: String,
        on_activity_present: OnActivityPresent,
    },
    ExportActivities {
        destination_folder: String,
    },
    ExportActivity {
        destination_folder: String,
        id: String,
    },
    MigrateActivitiesFromLegacyPath,
    TrackToGpx {
        name: String,
        description: String,
        r#type: String,
        start_time: String,
        track: Option<Box<Track>>,
    },
}

#[derive(Debug, PartialEq, Eq)]
enum ActivityArtefactEventType {
    Loaded,
    Imported,
}

#[derive(Debug)]
enum Artifact {
    Activity(Box<Activity>, ActivityArtefactEventType),
    ActivityLoadingFinished,
    Track(Box<Track>),
    TrackLoadingFailed,
    ActivityImportFinished,
    ActivityExportFinished,
    ActivityMigrationFinished,
    GpxTrack {
        track: Option<Box<Track>>,
        gpx: Option<String>,
    },
}

pub struct ActivityHistoryStore {
    worker_queue: Option<Sender<Command>>,
    worker_th_jh: Option<JoinHandle<()>>,
    artifacts: Receiver<Artifact>,
    loading_finished: bool,
    activities: Vec<Box<Activity>>,
    activity_summary: HashMap<ActivityType, HashMap<IntervalSummaryId, ActivitySummary>>, // TODO check whether Cow<&str> works for ActivityType ... maybe `Cow<'static, &str>`
    track: Option<Box<Track>>,
    gpx_track: Option<String>,
}

impl Drop for ActivityHistoryStore {
    fn drop(&mut self) {
        self.worker_queue = None;
        self.worker_th_jh.take().map(|jh| {
            jh.join()
                .map_err(|e| {
                    println!("Error joining worker thread: {:?}", e);
                    e
                })
                .ok()
        });
    }
}

pub fn new_activity_history(events: ArtifactAndHistoryEvents) -> ActivityHistory {
    ActivityHistory::new(events)
}

impl ActivityHistory {
    pub fn new(events: ArtifactAndHistoryEvents) -> Self {
        let (sender, receiver) = bounded(100);
        let (outbox, inbox) = unbounded();
        let worker_th_jh = {
            let artifact_event = events.artifact;
            thread::spawn(move || {
                Self::command_handler(receiver, outbox, artifact_event);
            })
        };
        Self {
            store: Box::new(ActivityHistoryStore {
                worker_queue: Some(sender),
                worker_th_jh: Some(worker_th_jh),
                artifacts: inbox,
                loading_finished: false,
                activities: Vec::new(),
                activity_summary: HashMap::new(),
                track: None,
                gpx_track: None,
            }),
            events: events.history,
        }
    }

    fn command_handler(
        receiver: Receiver<Command>,
        outbox: Sender<Artifact>,
        mut event: ArtifactEvent,
    ) {
        let deliver_activity = |outbox: &Sender<Artifact>,
                                event: &mut ArtifactEvent,
                                activity,
                                activity_artifact_event_type| {
            outbox
                .send(Artifact::Activity(
                    Box::new(activity),
                    activity_artifact_event_type,
                ))
                .map_err(|e| println!("Could not send Activity: {}", e))
                .ok();
            event.new_artifacts.pin_mut().notify();
        };

        while let Ok(cmd) = receiver.recv() {
            match cmd {
                Command::LoadAllActivities => {
                    Activity::read_all_activities(|activity| {
                        deliver_activity(
                            &outbox,
                            &mut event,
                            activity,
                            ActivityArtefactEventType::Loaded,
                        );
                    })
                    .map_err(|e| println!("Could not load activities: {}", e))
                    .ok();
                    outbox
                        .send(Artifact::ActivityLoadingFinished)
                        .map_err(|e| println!("Could not send activity loading finished: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::LoadActivity(id) => {
                    Activity::read_activity_by_id(&id)
                        .map(|activity| {
                            deliver_activity(
                                &outbox,
                                &mut event,
                                activity,
                                ActivityArtefactEventType::Loaded,
                            );
                        })
                        .map_err(|e| println!("Could not read activity meta data: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::LoadTrack(id) => {
                    Track::read_track(id)
                        .map_or_else(
                            |_| outbox.send(Artifact::TrackLoadingFailed),
                            |track| outbox.send(Artifact::Track(Box::new(track))),
                        )
                        .map_err(|e| println!("Could not read track: {}", e))
                        .ok();

                    event.new_artifacts.pin_mut().notify();
                }
                Command::ImportActivities {
                    source_folder,
                    on_activity_present,
                } => {
                    importer::import_all_from_path(
                        source_folder.into(),
                        on_activity_present,
                        |activity| {
                            deliver_activity(
                                &outbox,
                                &mut event,
                                activity,
                                ActivityArtefactEventType::Imported,
                            );
                        },
                    )
                    .map_err(|e| println!("Could not import activities: {}", e))
                    .ok();
                    outbox
                        .send(Artifact::ActivityImportFinished)
                        .map_err(|e| println!("Could not send activity import finished: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::ExportActivities { destination_folder } => {
                    let destination_folder: PathBuf = destination_folder.into();
                    std::fs::create_dir_all(destination_folder.as_path())
                        .map_err(|e| println!("Error creating config dir path: {}", e))
                        .ok();

                    // although the activities are already loaded, they cannot be accessed from this thread without
                    // adding a mutex to the store and therefore it is better to just read them again and immediately export them
                    Activity::read_all_activities(|a| {
                        if let Ok(t) = Track::read_track(a.id.clone()) {
                            exporter::export_activity_to_path(
                                destination_folder.clone(),
                                &a.id,
                                &a.meta.activity.name,
                                &a.meta.activity.description,
                                &a.meta.activity.r#type,
                                &a.meta.info.start_time,
                                &t.record.section,
                            )
                            .map_err(|e| println!("Could not export activity '{}': {}", a.id, e))
                            .ok();
                        }
                    })
                    .map_err(|e| println!("Could not load activities for export: {}", e))
                    .ok();

                    outbox
                        .send(Artifact::ActivityExportFinished)
                        .map_err(|e| println!("Could not send activity export finished: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::ExportActivity {
                    destination_folder,
                    id,
                } => {
                    let destination_folder: PathBuf = destination_folder.into();
                    std::fs::create_dir_all(destination_folder.as_path())
                        .map_err(|e| println!("Error creating config dir path: {}", e))
                        .ok();

                    match (
                        Activity::read_activity_by_id(&id),
                        Track::read_track(id.clone()),
                    ) {
                        (Ok(a), Ok(t)) => {
                            exporter::export_activity_to_path(
                                destination_folder,
                                &id,
                                &a.meta.activity.name,
                                &a.meta.activity.description,
                                &a.meta.activity.r#type,
                                &a.meta.info.start_time,
                                &t.record.section,
                            )
                            .map_err(|e| println!("Could not export activity '{}': {}", id, e))
                            .ok();
                        }
                        _ => {
                            println!("Could not export activity '{}'", id);
                        }
                    }

                    outbox
                        .send(Artifact::ActivityExportFinished)
                        .map_err(|e| println!("Could not send activity export finished: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::MigrateActivitiesFromLegacyPath => {
                    importer::import_all_from_path(
                        KURI_LEGACY_DATA_DIR.clone(),
                        OnActivityPresent::Override,
                        |activity| {
                            deliver_activity(
                                &outbox,
                                &mut event,
                                activity,
                                ActivityArtefactEventType::Imported,
                            );
                        },
                    )
                    .map_err(|e| println!("Could not import activities: {}", e))
                    .ok();
                    outbox
                        .send(Artifact::ActivityMigrationFinished)
                        .map_err(|e| println!("Could not send activity migration finished: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
                Command::TrackToGpx {
                    name,
                    description,
                    r#type,
                    start_time,
                    track,
                } => {
                    let gpx = track.as_ref().map(|t| {
                        exporter::track_to_gpx(
                            &name,
                            &description,
                            &r#type,
                            &start_time,
                            &t.record.section,
                        )
                    });
                    outbox
                        .send(Artifact::GpxTrack { track, gpx })
                        .map_err(|e| println!("Could not convert track to GPX: {}", e))
                        .ok();
                    event.new_artifacts.pin_mut().notify();
                }
            }
        }
    }

    pub fn shutdown(&mut self) {
        self.store.worker_queue = None;
    }

    pub fn process_artifacts(&mut self) {
        let old_activity_total_count = self.activity_total_count();
        while let Ok(artifact) = self.store.artifacts.try_recv() {
            match artifact {
                Artifact::Activity(a, ActivityArtefactEventType::Loaded) => {
                    for &t in &["allactivities", a.activity_type()] {
                        let intervals = self.store.activity_summary.entry(t.into()).or_default();

                        for &i in &[IntervalSummaryId::entire(), a.interval_id] {
                            intervals.entry(i).or_default().add(&a);
                        }
                    }
                    self.store.activities.push(a);
                }
                Artifact::Activity(a, ActivityArtefactEventType::Imported) => {
                    let activity_summary = &mut self.store.activity_summary;
                    if let Some((index, activity)) = self
                        .store
                        .activities
                        .iter_mut()
                        .enumerate()
                        .find(|(_, activity)| activity.id == a.id)
                    {
                        for &t in &["allactivities", activity.activity_type()] {
                            activity_summary.get_mut(t.into()).map(|intervals| {
                                for &i in &[IntervalSummaryId::entire(), activity.interval_id] {
                                    intervals.get_mut(&i).map(|s| {
                                        s.remove(&activity);
                                    });
                                }
                            });
                        }

                        **activity = *a;
                        for &t in &["allactivities", activity.activity_type()] {
                            let intervals = activity_summary.entry(t.into()).or_default();

                            for &i in &[IntervalSummaryId::entire(), activity.interval_id] {
                                intervals.entry(i).or_default().add(&activity);
                            }
                        }

                        self.events.activity_changed.pin_mut().notify(index as u64);
                        self.events.activity_summary_changed.pin_mut().notify();
                    } else {
                        for &t in &["allactivities", a.activity_type()] {
                            let intervals = activity_summary.entry(t.into()).or_default();

                            for &i in &[IntervalSummaryId::entire(), a.interval_id] {
                                intervals.entry(i).or_default().add(&a);
                            }
                        }
                        self.store.activities.push(a);
                    }
                }
                Artifact::ActivityLoadingFinished => {
                    self.store.loading_finished = true;
                    self.events.activities_loading.pin_mut().notify(false);

                    let activity_total_count = self.activity_total_count() as u64;
                    self.events
                        .activity_total_count_changed
                        .pin_mut()
                        .notify(activity_total_count);
                    self.events.activity_summary_changed.pin_mut().notify();
                }
                Artifact::Track(t) => {
                    self.store.track = Some(t);
                    self.events.track_available.pin_mut().notify(true);
                }
                Artifact::TrackLoadingFailed => {
                    self.events.track_available.pin_mut().notify(false);
                }
                Artifact::ActivityImportFinished => {
                    self.events.activity_import_ongoing.pin_mut().notify(false);
                }
                Artifact::ActivityExportFinished => {
                    self.events.activity_export_ongoing.pin_mut().notify(false);
                }
                Artifact::ActivityMigrationFinished => {
                    self.events
                        .activity_migration_ongoing
                        .pin_mut()
                        .notify(false);
                }
                Artifact::GpxTrack { track, gpx } => {
                    self.store.track = track;
                    self.store.gpx_track = gpx;
                    self.events.gpx_track_available.pin_mut().notify();
                }
            }
        }

        let new_activity_total_count = self.activity_total_count();
        if self.store.loading_finished && new_activity_total_count != old_activity_total_count {
            self.events
                .activity_total_count_changed
                .pin_mut()
                .notify(new_activity_total_count as u64);
            self.events.activity_summary_changed.pin_mut().notify();
        }
    }

    fn send(&mut self, cmd: Command) {
        let _ = self
            .store
            .worker_queue
            .as_mut()
            .map(|sender| sender.send(cmd));
    }

    pub fn load_all_activities(&mut self) {
        self.store.activities.clear();
        self.events.activities_loading.pin_mut().notify(true);
        self.send(Command::LoadAllActivities);
    }

    pub fn load_activity(&mut self, id: &str) {
        self.send(Command::LoadActivity(id.into()));
    }

    pub fn delete_activity(&mut self, id: &str) {
        self.store
            .activities
            .as_slice()
            .into_iter()
            .position(|a| a.id == id)
            .map(|index| {
                let a = self.store.activities.remove(index);
                for &t in &["allactivities", a.activity_type()] {
                    self.store
                        .activity_summary
                        .get_mut(t.into())
                        .map(|intervals| {
                            for &i in &[IntervalSummaryId::entire(), a.interval_id] {
                                intervals.get_mut(&i).map(|s| {
                                    s.remove(&a);
                                });
                            }
                        });
                }
                self.events.activity_summary_changed.pin_mut().notify();

                let mut path = KURI_META_DATA_DIR.clone();
                path.push(format!("{}.meta.toml", id));
                std::fs::remove_file(path)
                    .map_err(|e| println!("Error discarding recording: {}", e))
                    .ok();
                let mut path = KURI_TRACK_DATA_DIR.clone();
                path.push(format!("{}.track-raw.toml.zst", id));
                std::fs::remove_file(path)
                    .map_err(|e| println!("Error discarding recording: {}", e))
                    .ok();
            });

        let activity_total_count = self.activity_total_count() as u64;
        self.events
            .activity_total_count_changed
            .pin_mut()
            .notify(activity_total_count);
    }

    pub fn load_track(&mut self, id: &str) {
        self.events.track_available.pin_mut().notify(false);
        self.send(Command::LoadTrack(id.into()));
    }

    pub fn discard_track(&mut self) {
        self.store.track = None;
        self.events.track_available.pin_mut().notify(false);
    }

    pub fn activity_total_count(&self) -> u64 {
        self.store.activities.len() as u64
    }

    pub fn activity_type_count(&self) -> u64 {
        // ignore the 'allactivities' key
        (self.store.activity_summary.len() as u64).saturating_sub(1)
    }

    pub fn activity_at(&self, index: u64) -> &Activity {
        &self.store.activities[index as usize]
    }

    pub fn activity_at_mut(&mut self, index: u64) -> &mut Activity {
        &mut self.store.activities[index as usize]
    }

    fn activity_summary(&self, activity_type: &str) -> ActivitySummary {
        self.activity_interval_summary(activity_type, IntervalSummaryId::entire())
    }

    fn activity_interval_summary(
        &self,
        activity_type: &str,
        interval_id: IntervalSummaryId,
    ) -> ActivitySummary {
        self.store
            .activity_summary
            .get(activity_type.into())
            .and_then(|intervals| intervals.get(&interval_id).map(|s| *s))
            .unwrap_or(ActivitySummary::default())
    }

    fn update_activity_summary(
        &mut self,
        activity: &Activity,
        activity_type_old: &str,
        activity_type_new: &str,
    ) {
        self.store
            .activity_summary
            .get_mut(activity_type_old.into())
            .map(|intervals| {
                for &i in &[IntervalSummaryId::entire(), activity.interval_id] {
                    intervals.get_mut(&i).map(|s| {
                        s.remove(&activity);
                    });
                }
            });

        let intervals = self
            .store
            .activity_summary
            .entry(activity_type_new.into())
            .or_default();

        for &i in &[IntervalSummaryId::entire(), activity.interval_id] {
            intervals.entry(i).or_default().add(&activity);
        }

        self.events.activity_summary_changed.pin_mut().notify();
    }

    pub fn track(&self) -> *const Track {
        self.store
            .track
            .as_ref()
            .map(|t| &**t as *const Track)
            .unwrap_or(std::ptr::null())
    }

    fn track_to_gpx(&mut self) {
        let track = self.store.track.take();
        if let Some(t) = track.as_ref() {
            for a in self.store.activities.iter() {
                if a.id == t.id {
                    self.send(Command::TrackToGpx {
                        name: a.meta.activity.name.clone(),
                        description: a.meta.activity.description.clone(),
                        r#type: a.meta.activity.r#type.clone(),
                        start_time: a.meta.info.start_time.clone(),
                        track,
                    });
                    break;
                }
            }
        }
    }

    fn get_gpx_track(&mut self) -> String {
        self.store.gpx_track.take().unwrap_or("".into())
    }

    pub fn import_activities(&mut self, source_folder: &str, override_existing: bool) {
        self.events.activity_import_ongoing.pin_mut().notify(true);
        self.send(Command::ImportActivities {
            source_folder: source_folder.into(),
            on_activity_present: if override_existing {
                OnActivityPresent::Override
            } else {
                OnActivityPresent::Skip
            },
        });
    }

    pub fn export_activities(&mut self, destination_folder: &str) {
        self.events.activity_export_ongoing.pin_mut().notify(true);
        self.send(Command::ExportActivities {
            destination_folder: destination_folder.into(),
        });
    }

    pub fn export_activity(&mut self, destination_folder: &str, id: &str) {
        self.events.activity_export_ongoing.pin_mut().notify(true);
        self.send(Command::ExportActivity {
            destination_folder: destination_folder.into(),
            id: id.into(),
        });
    }

    pub fn migrate_activities_from_legacy_path(&mut self) {
        if importer::has_laufhelden_activities() {
            self.events
                .activity_migration_ongoing
                .pin_mut()
                .notify(true);
            self.send(Command::MigrateActivitiesFromLegacyPath);
        }
    }
}

impl ActivitySummary {
    fn add(&mut self, activity: &Activity) {
        self.activity_count += 1;
        self.distance += activity.distance();
        self.duration += activity.duration_active();
    }

    fn remove(&mut self, activity: &Activity) {
        self.activity_count = self.activity_count.saturating_sub(1);
        self.duration = self.duration.saturating_sub(activity.duration_active());
        self.distance = self.distance.saturating_sub(activity.distance());
    }
}

impl IntervalSummaryId {
    const fn entire() -> Self {
        Self {
            year: -1,
            month: -1,
        }
    }

    const fn dummy() -> Self {
        Self { year: 0, month: 0 }
    }

    fn from_start_time(start_time: &str) -> Self {
        // expecting "yyyy-mm"
        if start_time.len() < 7 {
            Self::dummy()
        } else {
            match (
                start_time[0..=3].parse::<i32>(),
                start_time[5..=6].parse::<i32>(),
            ) {
                (Ok(year), Ok(month)) => Self { year, month },
                _ => Self::dummy(),
            }
        }
    }
}

pub fn new_artifact_and_history_events() -> ArtifactAndHistoryEvents {
    ArtifactAndHistoryEvents {
        artifact: ArtifactEvent {
            new_artifacts: events::ffi::new_event_void(),
        },
        history: HistoryEvents {
            activities_loading: events::ffi::new_event_bool(),
            activity_import_ongoing: events::ffi::new_event_bool(),
            activity_export_ongoing: events::ffi::new_event_bool(),
            activity_migration_ongoing: events::ffi::new_event_bool(),
            track_available: events::ffi::new_event_bool(),
            activity_total_count_changed: events::ffi::new_event_u64(),
            activity_summary_changed: events::ffi::new_event_void(),
            activity_changed: events::ffi::new_event_u64(),
            gpx_track_available: events::ffi::new_event_void(),
        },
    }
}

unsafe impl Send for ArtifactEvent {}

#[cxx::bridge(namespace = "kuri::rs")]
mod ffi {
    pub struct ActivityHistory {
        store: Box<ActivityHistoryStore>,
        events: HistoryEvents,
    }

    struct ArtifactEvent {
        pub new_artifacts: UniquePtr<EventVoid>,
    }

    struct HistoryEvents {
        pub activities_loading: UniquePtr<EventBool>,
        pub activity_import_ongoing: UniquePtr<EventBool>,
        pub activity_export_ongoing: UniquePtr<EventBool>,
        pub activity_migration_ongoing: UniquePtr<EventBool>,
        pub track_available: UniquePtr<EventBool>,
        pub activity_total_count_changed: UniquePtr<EventU64>,
        pub activity_summary_changed: UniquePtr<EventVoid>,
        pub activity_changed: UniquePtr<EventU64>,
        pub gpx_track_available: UniquePtr<EventVoid>,
    }

    pub struct ArtifactAndHistoryEvents {
        pub artifact: ArtifactEvent,
        pub history: HistoryEvents,
    }

    #[derive(Debug, Default, Clone, Copy)]
    pub struct Measurement {
        pub timestamp: i64,
        pub latitude: f64,
        pub longitude: f64,
        pub altitude: f32,
        pub horizontal_accuracy: u16,
        pub vertical_accuracy: u16,
        pub position_valid: bool,
        pub heart_rate: u16,
    }

    #[derive(Debug, Default, Clone, Copy)]
    struct ActivitySummary {
        activity_count: u64,
        duration: u64,
        distance: u64,
    }

    #[derive(Debug, Default, Clone, Copy, Hash, Eq, PartialEq)]
    struct IntervalSummaryId {
        year: i32,
        month: i32,
    }

    extern "Rust" {
        type ActivityHistoryStore;
        type Activity;
        type Track;

        fn new_artifact_and_history_events() -> ArtifactAndHistoryEvents;

        fn new_activity_history(events: ArtifactAndHistoryEvents) -> ActivityHistory;

        fn shutdown(self: &mut ActivityHistory);

        fn process_artifacts(self: &mut ActivityHistory);

        fn load_all_activities(self: &mut ActivityHistory);
        fn load_activity(self: &mut ActivityHistory, id: &str);
        fn delete_activity(self: &mut ActivityHistory, id: &str);
        fn load_track(self: &mut ActivityHistory, id: &str);
        fn discard_track(self: &mut ActivityHistory);

        fn activity_total_count(self: &ActivityHistory) -> u64;
        fn activity_type_count(self: &ActivityHistory) -> u64;
        fn activity_at(self: &ActivityHistory, index: u64) -> &Activity;
        fn activity_at_mut(self: &mut ActivityHistory, index: u64) -> &mut Activity;
        fn activity_summary(self: &ActivityHistory, activity_type: &str) -> ActivitySummary;
        fn activity_interval_summary(
            self: &ActivityHistory,
            activity_type: &str,
            interval_id: IntervalSummaryId,
        ) -> ActivitySummary;
        fn update_activity_summary(
            self: &mut ActivityHistory,
            activity: &Activity,
            activity_type_old: &str,
            activity_type_new: &str,
        );
        fn track(self: &ActivityHistory) -> *const Track;

        fn track_to_gpx(self: &mut ActivityHistory);
        fn get_gpx_track(self: &mut ActivityHistory) -> String;

        fn import_activities(
            self: &mut ActivityHistory,
            source_folder: &str,
            override_existing: bool,
        );

        fn export_activities(self: &mut ActivityHistory, destination_folder: &str);
        fn export_activity(self: &mut ActivityHistory, destination_folder: &str, id: &str);

        fn migrate_activities_from_legacy_path(self: &mut ActivityHistory);

        fn id(self: &Activity) -> &str;
        fn interval_id(self: &Activity) -> IntervalSummaryId;
        fn start_time(self: &Activity) -> &str;
        fn activity_type(self: &Activity) -> &str;
        fn name(self: &Activity) -> &str;
        fn description(self: &Activity) -> &str;
        fn duration_active(self: &Activity) -> u64;
        fn duration_pause(self: &Activity) -> u64;
        fn distance(self: &Activity) -> u64;
        fn elevation_up(self: &Activity) -> u64;
        fn elevation_down(self: &Activity) -> u64;
        fn speed_average(self: &Activity) -> f64;
        fn pace_average(self: &Activity) -> f64;
        fn heart_rate_average(self: &Activity) -> u16;

        fn save_changes(self: &mut Activity);
        fn set_activity_type(self: &mut Activity, activity_type: &str);
        fn set_activity_name(self: &mut Activity, activity_name: &str);
        fn set_activity_description(self: &mut Activity, activity_description: &str);

        fn id(self: &Track) -> &str;
        fn total_point_count(self: &Track) -> u64;
        fn section_count(self: &Track) -> u64;
        fn section_point_count(self: &Track, section: u64) -> u64;
        fn measurement_at(self: &Track, section: u64, index: u64) -> Measurement;
    }
    #[namespace = "kuri::ffi"]
    unsafe extern "C++" {
        include!("kuri-rs/ffi/events.h");
        type EventVoid = crate::events::ffi::EventVoid;
        type EventBool = crate::events::ffi::EventBool;
        type EventU64 = crate::events::ffi::EventU64;
    }
}
