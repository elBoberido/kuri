// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(transparent)]
pub struct Value<T> {
    value: T,
    #[serde(skip, default = "value_changed_default")]
    changed: bool,
}

fn value_changed_default() -> bool {
    true
}

impl<T: Default> Default for Value<T> {
    fn default() -> Self {
        Self {
            value: Default::default(),
            changed: value_changed_default(),
        }
    }
}

impl<T> Value<T> {
    pub fn new(value: T) -> Self {
        Self {
            value,
            changed: true,
        }
    }

    pub fn set(&mut self, value: T) {
        self.value = value;
        self.changed = true;
    }

    pub fn peek(&self) -> &T {
        &self.value
    }

    pub fn when_changed<F>(&mut self, f: F)
    where
        F: FnOnce(&T),
    {
        if self.changed {
            f(&self.value);
            self.changed = false;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn values_signal_change_on_initial_value() {
        const TEST_VALUE: u8 = 42;
        let mut sut = Value::<u8>::new(TEST_VALUE);

        assert_eq!(sut.changed, true);
        assert_eq!(sut.value, TEST_VALUE);

        let mut signaled = false;
        sut.when_changed(|&value| {
            signaled = true;
            assert_eq!(value, TEST_VALUE);
        });
        assert_eq!(signaled, true);
    }

    #[test]
    fn values_does_not_signal_change_a_second_time() {
        const TEST_VALUE: u8 = 13;
        let mut sut = Value::<u8>::new(TEST_VALUE);

        sut.when_changed(|_| {});

        let mut signaled = false;
        sut.when_changed(|_| {
            signaled = true;
        });
        assert_eq!(signaled, false);
    }

    #[test]
    fn values_signal_change_after_change() {
        const TEST_VALUE: u8 = 73;
        let mut sut = Value::<u8>::new(37);

        sut.when_changed(|_| {});

        sut.set(TEST_VALUE);
        let mut signaled = false;
        sut.when_changed(|&value| {
            signaled = true;
            assert_eq!(value, TEST_VALUE);
        });
        assert_eq!(signaled, true);
    }

    #[test]
    fn serialize_only_value() {
        #[derive(Serialize, Deserialize, Debug, Default)]
        struct Nested {
            data: Value<u8>,
        }
        const TEST_VALUE: u8 = 37;
        let sut = Nested {
            data: Value::new(TEST_VALUE),
        };
        let serialized = toml::to_string_pretty(&sut).unwrap();

        assert_eq!(serialized, format!("data = {}\n", TEST_VALUE));
    }

    #[test]
    fn deserialize_value_with_change_flag_set() {
        #[derive(Serialize, Deserialize, Debug, Default)]
        struct Nested {
            data: Value<u8>,
        }
        const TEST_VALUE: u8 = 73;
        // let sut = Nested{ data: Value::new(TEST_VALUE)};
        let mut sut: Nested = toml::from_str(&format!("data = {}\n", TEST_VALUE)).unwrap();

        let mut signaled = false;
        sut.data.when_changed(|&value| {
            signaled = true;
            assert_eq!(value, TEST_VALUE);
        });
        assert_eq!(signaled, true);
    }
}
