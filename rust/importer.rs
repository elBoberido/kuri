// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

mod gpx;

use crate::activity_history::activity::Activity;
use crate::activity_recorder::storage::{meta_and_track_data_path, FileName, Storage};
use crate::paths::KURI_LEGACY_DATA_DIR;
use crate::records::{ActivityMetaData, TrackRecord, TrackSection};
use crate::utils::timestamp_to_id;

use once_cell::sync::Lazy;

use std::collections::hash_map::HashMap;
use std::ffi::OsStr;
use std::fs::{metadata, read_dir, File, OpenOptions};
use std::io::{Read, Write};
use std::path::PathBuf;
use std::time::Duration;

#[derive(Debug, Copy, Clone)]
pub enum OnActivityPresent {
    Skip,
    Override,
}

pub static KURI_LEGACY_ACTIVITY_START_TIMES: Lazy<HashMap<String, String>> = Lazy::new(|| {
    let mut acceleration_helper_path = KURI_LEGACY_DATA_DIR.clone();
    acceleration_helper_path.push("AccelerationHelper.dat");

    let mut serialized = String::new();
    if let Ok(_) = OpenOptions::new()
        .read(true)
        .open(&acceleration_helper_path)
        .and_then(|mut file| file.read_to_string(&mut serialized))
    {
        get_laufhelden_activity_start_times(&serialized)
    } else {
        HashMap::new()
    }
});

fn get_laufhelden_activity_start_times(serialized: &str) -> HashMap<String, String> {
    let mut lookup_table = HashMap::new();

    let mut file_name = None;

    for line in serialized.lines() {
        line.split_once(':')
            .map(|(n, value)| match n.parse::<u32>() {
                Ok(1) => file_name = Some(value),
                Ok(4) => {
                    file_name
                        .map(|file_name| {
                            lookup_table.insert(file_name.trim().into(), value.trim().into())
                        })
                        .take();
                }
                _ => (),
            });
    }

    lookup_table
}

pub fn has_laufhelden_activities() -> bool {
    metadata(&*KURI_LEGACY_DATA_DIR)
        .map(|metadata| metadata.is_dir())
        .unwrap_or(false)
}

pub fn import_all_from_path<F>(
    path: PathBuf,
    on_present: OnActivityPresent,
    mut on_new_activity: F,
) -> Result<(), std::io::Error>
where
    F: FnMut(Activity),
{
    read_dir(path).map(|entries| {
        let mut files: Vec<_> = entries.map(|e| e.expect("Valid file").path()).collect();
        files.sort();
        files.into_iter().for_each(|file| {
            import_file(file, on_present)
                .and_then(|activity_id| {
                    let activity = Activity::read_activity_by_id(&activity_id)?;
                    on_new_activity(activity);
                    Ok(())
                })
                .map_err(|e| println!("Could not import file: {}", e))
                .ok();
        });
    })
}

fn import_file(file: PathBuf, on_present: OnActivityPresent) -> Result<String, std::io::Error> {
    match file.extension().and_then(OsStr::to_str) {
        Some("gpx") => gpx::import(file, on_present),
        _ => Err(std::io::Error::new(
            std::io::ErrorKind::Other,
            "File with unknown extension",
        )),
    }
}

fn pause_to_section_correction(sections: Vec<TrackSection>) -> Vec<TrackSection> {
    let mut corrected_sections = Vec::new();
    let mut sections: Vec<_> = sections.into_iter().rev().collect();
    sections.pop().map(|s| corrected_sections.push(s));

    while sections.len() > 0 {
        sections.pop().map(|mut s| {
            if let Some(last) = corrected_sections.last() {
                match (last.measurement.last(), s.measurement.first()) {
                    (Some(m_last), Some(m_current))
                        if m_current.timestamp.as_secs() - m_last.timestamp.as_secs() > 0
                            && m_current.timestamp.as_secs() - m_last.timestamp.as_secs() <= 2 =>
                    {
                        s.measurement.insert(0, *m_last);
                    }
                    _ => {}
                }
            }
            corrected_sections.push(s);
        });
    }

    corrected_sections
}

fn store_activity(
    source_file_name: &str,
    timestamp_start: Duration,
    meta_data: ActivityMetaData,
    track: TrackRecord,
    on_present: OnActivityPresent,
) -> Result<String, std::io::Error> {
    let mut activity_id = String::new();
    for n in 1.. {
        activity_id = timestamp_to_id(timestamp_start, n);
        let (meta_path, track_path) = meta_and_track_data_path(&activity_id);

        match (metadata(&meta_path), metadata(&track_path)) {
            (Ok(m), Ok(t)) if m.is_file() || t.is_file() => {
                // check if the activity corresponds to the currently imported one
                if m.is_file() {
                    let mut amd_serialized = String::new();
                    OpenOptions::new()
                        .read(true)
                        .open(&meta_path)?
                        .read_to_string(&mut amd_serialized)?;

                    let present_amd = toml::from_str::<ActivityMetaData>(&amd_serialized)?;
                    let present_sha256 = present_amd.info.import_sha256.as_ref();

                    match (meta_data.info.import_sha256.as_ref(), present_sha256) {
                        (Some(new), Some(present)) if new == present => {}
                        _ => continue,
                    }

                    match on_present {
                        OnActivityPresent::Override => {
                            std::fs::remove_file(meta_path).ok();
                            std::fs::remove_file(track_path).ok();
                            break;
                        }
                        OnActivityPresent::Skip => Err(std::io::Error::new(
                            std::io::ErrorKind::Other,
                            format!(
                                "Already imported! Skipping import of '{}'",
                                source_file_name
                            ),
                        ))?,
                    }
                }
                continue;
            }
            _ => break,
        }
    }

    const IMPORT_META_FILE_NAME: &str = "gpx-import.meta.toml";
    const IMPORT_TRACK_FILE_NAME: &str = "gpx-import.track-raw.toml";

    let meta_file = File::new_tmp_storage(IMPORT_META_FILE_NAME);
    let track_file = File::new_tmp_storage(IMPORT_TRACK_FILE_NAME);

    match (meta_file, track_file) {
        (Some(mut meta_file), Some(mut track_file)) => {
            write!(meta_file, "{}", toml::to_string_pretty(&meta_data).unwrap())
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();

            write!(track_file, "{}", toml::to_string_pretty(&track).unwrap())
                .map_err(|e| println!("Error occured: {:?}", e))
                .ok();

            meta_file.flush()?;
            track_file.flush()?;

            File::make_persistent_compressed(
                IMPORT_TRACK_FILE_NAME,
                FileName::TrackData(&format!("{}.track-raw.toml.zst", &activity_id)),
            );
            File::make_persistent(
                IMPORT_META_FILE_NAME,
                FileName::MetaData(&format!("{}.meta.toml", &activity_id)),
            );
        }
        _ => {
            File::discard(IMPORT_META_FILE_NAME);
            File::discard(IMPORT_TRACK_FILE_NAME);
            Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Error creating import files for '{}'", source_file_name),
            ))?
        }
    }

    Ok(activity_id)
}

#[cfg(test)]
mod tests;
