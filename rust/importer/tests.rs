// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::*;

const ACCELLERATOR_HELPER: &str = r#"
1: running-Fr. Sep. 13 14:37:42 2022-1.1km.gpx
2:
3:
4: 2022-09-13T14:37:42
5:
6:
7:
8:
9:
10:
11:

1: walking-Fr. Sep. 20 15:42:37 2022-1.1km.gpx
4: 2022-09-20T15:42:37
"#;

#[test]
fn laufhelden_activity_start_times_works() {
    let lookup_table = get_laufhelden_activity_start_times(ACCELLERATOR_HELPER);

    assert_eq!(lookup_table.len(), 2);
    assert_eq!(
        lookup_table.get("running-Fr. Sep. 13 14:37:42 2022-1.1km.gpx"),
        Some(&"2022-09-13T14:37:42".into())
    );
    assert_eq!(
        lookup_table.get("walking-Fr. Sep. 20 15:42:37 2022-1.1km.gpx"),
        Some(&"2022-09-20T15:42:37".into())
    );
}
