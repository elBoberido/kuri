// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::{
    pause_to_section_correction, store_activity, OnActivityPresent,
    KURI_LEGACY_ACTIVITY_START_TIMES,
};
use crate::records::{
    Activity, ActivityMetaData, Measurement, MetaInfo, TrackInfo, TrackRecord, TrackSection,
};
use crate::statistics::statistics_from_track;

use chrono::{TimeZone, Utc};

use quick_xml::events::{BytesStart, Event};
use quick_xml::reader::Reader;

use sha2::{Digest, Sha256};

use std::borrow::Cow;
use std::fs::OpenOptions;
use std::io::Read;
use std::path::PathBuf;
use std::time::Duration;

// TODO make this generic over the storage type
pub fn import(file: PathBuf, on_present: OnActivityPresent) -> Result<String, std::io::Error> {
    let mut gpx = String::new();
    OpenOptions::new()
        .read(true)
        .open(&file)?
        .read_to_string(&mut gpx)?;

    let mut hasher = Sha256::new();
    hasher.update(&gpx);
    let sha256 = format!("{:x}", hasher.finalize());

    let (mut info, activity, section) = read_gpx(&gpx).map_err(|_| {
        std::io::Error::new(
            std::io::ErrorKind::Other,
            format!("Error parsing '{}'", file.display()),
        )
    })?;
    info.import_sha256 = Some(sha256);

    let source_file_name =
        file.file_name()
            .and_then(|name| name.to_str())
            .ok_or(std::io::Error::new(
                std::io::ErrorKind::Other,
                format!("Error extracting file name of '{}'", file.display()),
            ))?;

    let timestamp_start = section
        .first()
        .and_then(|ts| ts.measurement.first())
        .map(|m| m.timestamp)
        .ok_or(std::io::Error::new(
            std::io::ErrorKind::Other,
            format!(
                "Error getting timestamp of first measurement in '{}'",
                file.display()
            ),
        ))?;

    if info.start_time.is_empty() {
        info.start_time = get_start_time(source_file_name, &info.source)
            .unwrap_or(get_start_time_fallback(timestamp_start));
    }

    let meta_data = ActivityMetaData {
        info,
        activity,
        statistics: statistics_from_track(&section),
    };

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    store_activity(
        source_file_name,
        timestamp_start,
        meta_data,
        track,
        on_present,
    )
}

fn get_start_time(source_file_name: &str, source: &str) -> Option<String> {
    match source {
        "laufhelden" => KURI_LEGACY_ACTIVITY_START_TIMES
            .get(source_file_name)
            .map(|start_time| start_time.clone()),
        "kuri" => None,
        "meerun" => None,
        "sports-tracker" => None,
        "stravagpx" => None,
        _ => None,
    }
}

/// fallback for unknown sources
/// use the timestamp of the first track point as start time
/// unfortunately the timezone offset is not available
fn get_start_time_fallback(timestamp_start: Duration) -> String {
    // GPX timestamps are always in UTC
    Utc.timestamp(timestamp_start.as_secs() as i64, 0)
        .format("%Y-%m-%dT%H:%M:%S")
        .to_string()
}

fn activity_type_mapping(source: &str, activity_type: String) -> String {
    if activity_type.is_empty() {
        return "unknown".into();
    }
    match source {
        "laufhelden" => activity_type,
        "kuri" => activity_type,
        "meerun" => activity_type,
        "sports-tracker" => "unknown".into(),
        "stravagpx" => "unknown".into(),
        _ => "unknown".into(),
    }
}

fn read_gpx(gpx: &str) -> Result<(MetaInfo, Activity, Vec<TrackSection>), quick_xml::Error> {
    let mut reader = Reader::from_str(gpx);
    reader.trim_text(true).expand_empty_elements(true);

    match reader.read_event()? {
        Event::Decl(decl) => match decl.encoding() {
            Some(Ok(Cow::Borrowed(b"utf-8")))
            | Some(Ok(Cow::Borrowed(b"UTF-8")))
            | Some(Ok(Cow::Borrowed(b"iso-8859-1")))
            | Some(Ok(Cow::Borrowed(b"ISO-8859-1"))) => (),
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unsupported encoding".into(),
            ))?,
        },
        _ => Err(quick_xml::Error::UnexpectedToken(
            "Expected xml declaration".into(),
        ))?,
    }

    let mut source: Option<String> = None;

    match reader.read_event()? {
        Event::Start(element) => match element.name().as_ref() {
            b"gpx" => {
                for attribute in element.attributes() {
                    let attribute = attribute?;
                    match attribute.key.as_ref() {
                        b"Creator" | b"creator" => {
                            source = Some(
                                attribute
                                    .decode_and_unescape_value(&reader)?
                                    .to_lowercase()
                                    .replace(" ", "-"),
                            );
                        }
                        _ => continue,
                    }
                }
            }
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error while expecting start of 'gpx'".into(),
            ))?,
        },
        _ => Err(quick_xml::Error::UnexpectedToken(
            "Unknown error while trying to get start of 'gpx'".into(),
        ))?,
    }

    let (mut info, mut activity) = read_metadata(&mut reader)?;
    info.source = source.unwrap_or("unknown".into());
    let (activity_alternative, sections) = read_tracks(&mut reader)?;
    let sections = pause_to_section_correction(sections);

    if activity.r#type.is_empty() {
        activity.r#type = activity_alternative.r#type;
    }
    activity.r#type = activity_type_mapping(&info.source, activity.r#type);

    if activity.name.is_empty() {
        activity.name = activity_alternative.name;
    }
    if activity.description.is_empty() {
        activity.description = activity_alternative.description;
    }

    match reader.read_event() {
        Ok(Event::Eof) => (),
        _ => Err(quick_xml::Error::UnexpectedToken(
            "Unknown error in 'metadata'".into(),
        ))?,
    }

    Ok((info, activity, sections))
}

fn read_text(reader: &mut Reader<&[u8]>) -> Result<String, quick_xml::Error> {
    let mut text = String::new();
    loop {
        match reader.read_event()? {
            Event::Text(t) => {
                text = t.unescape()?.into();
            }
            Event::CData(t) => {
                text = t.escape()?.unescape()?.into();
            }
            Event::End(_) => {
                break;
            }
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error in while reading text".into(),
            ))?,
        }
    }

    Ok(text)
}

fn read_metadata(reader: &mut Reader<&[u8]>) -> Result<(MetaInfo, Activity), quick_xml::Error> {
    let mut info = MetaInfo::default();

    let mut activity = Activity {
        r#type: "".into(),
        name: "".into(),
        description: "".into(),
    };

    loop {
        match reader.read_event()? {
            Event::Start(element) => match element.name().as_ref() {
                b"metadata" => continue,
                b"name" => {
                    activity.name = read_text(reader)?;
                }
                b"desc" => {
                    activity.description = read_text(reader)?;
                }
                b"kuri" => {
                    for attribute in element.attributes() {
                        let attribute = attribute?;
                        match attribute.key.as_ref() {
                            b"local_start_time" => {
                                info.start_time =
                                    attribute.decode_and_unescape_value(&reader)?.into();
                            }
                            b"activity" => {
                                activity.r#type =
                                    attribute.decode_and_unescape_value(&reader)?.into();
                            }
                            _ => continue,
                        }
                    }
                    reader.read_to_end(element.name())?;
                }
                b"meerun" => {
                    for attribute in element.attributes() {
                        let attribute = attribute?;
                        match attribute.key.as_ref() {
                            b"activity" => {
                                activity.r#type =
                                    attribute.decode_and_unescape_value(&reader)?.into();
                            }
                            _ => continue,
                        }
                    }
                    reader.read_to_end(element.name())?;
                }
                b"time" => {
                    info.start_time = read_text(reader)?;
                }
                b"extensions" => continue,
                _ => {
                    reader.read_to_end(element.name())?;
                }
            },
            Event::End(element) => match element.name().as_ref() {
                b"extensions" => continue,
                b"metadata" => break,
                _ => Err(quick_xml::Error::UnexpectedToken(
                    "Unexpected end event in 'metadata'".into(),
                ))?,
            },
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error in 'metadata'".into(),
            ))?,
        }
    }

    Ok((info, activity))
}

fn read_tracks(
    reader: &mut Reader<&[u8]>,
) -> Result<(Activity, Vec<TrackSection>), quick_xml::Error> {
    let mut activity = Activity {
        r#type: "".into(),
        name: "".into(),
        description: "".into(),
    };

    let mut sections = Vec::new();

    loop {
        match reader.read_event()? {
            Event::Start(element) => match element.name().as_ref() {
                b"trk" => {
                    let (a, mut sec) = read_track_segments(reader)?;
                    if activity.r#type.is_empty() {
                        activity.r#type = a.r#type;
                    }
                    if activity.name.is_empty() {
                        activity.name = a.name;
                    }
                    if activity.description.is_empty() {
                        activity.description = a.description;
                    }
                    Vec::append(&mut sections, &mut sec);
                }
                _ => {
                    // ignore unknown elements
                    reader.read_to_end(element.name())?;
                }
            },
            Event::End(element) => match element.name().as_ref() {
                b"trk" => continue,
                b"gpx" => break,
                _ => Err(quick_xml::Error::UnexpectedToken(
                    "Unexpected end event at 'trk'".into(),
                ))?,
            },
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error at 'trk'".into(),
            ))?,
        }
    }

    Ok((activity, sections))
}

fn read_track_segments(
    reader: &mut Reader<&[u8]>,
) -> Result<(Activity, Vec<TrackSection>), quick_xml::Error> {
    let mut activity = Activity {
        r#type: "".into(),
        name: "".into(),
        description: "".into(),
    };

    let mut sections = Vec::new();

    loop {
        match reader.read_event()? {
            Event::Start(element) => match element.name().as_ref() {
                b"name" => {
                    activity.name = read_text(reader)?;
                }
                b"type" => {
                    activity.r#type = read_text(reader)?;
                }
                b"trkseg" => {
                    let ts = read_track_points(reader)?;
                    if !ts.measurement.is_empty() {
                        sections.push(ts);
                    }
                }
                _ => {
                    // ignore unknown elements
                    reader.read_to_end(element.name())?;
                }
            },
            Event::End(element) => match element.name().as_ref() {
                b"trkseg" => continue,
                b"trk" => break,
                _ => Err(quick_xml::Error::UnexpectedToken(
                    "Unexpected end event at 'trkseg'".into(),
                ))?,
            },
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error at 'trkseg'".into(),
            ))?,
        }
    }

    Ok((activity, sections))
}

fn read_track_points(reader: &mut Reader<&[u8]>) -> Result<TrackSection, quick_xml::Error> {
    let mut measurement = Vec::new();

    loop {
        match reader.read_event()? {
            Event::Start(element) => match element.name().as_ref() {
                b"trkpt" => {
                    let m = read_track_point(reader, element)?;
                    if m.timestamp.as_secs() > 0 {
                        measurement.push(m);
                    }
                }
                _ => {
                    // ignore unknown elements
                    reader.read_to_end(element.name())?;
                }
            },
            Event::End(element) => match element.name().as_ref() {
                b"trkpt" => continue,
                b"trkseg" => break,
                _ => Err(quick_xml::Error::UnexpectedToken(
                    "Unexpected end event at 'trkpt'".into(),
                ))?,
            },
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error at 'trkpt'".into(),
            ))?,
        }
    }

    Ok(TrackSection { measurement })
}

fn read_track_point(
    reader: &mut Reader<&[u8]>,
    element: BytesStart,
) -> Result<Measurement, quick_xml::Error> {
    let mut measurement = Measurement {
        timestamp: Duration::from_secs(0),
        position: None,
        heart_rate: None,
    };

    for attribute in element.attributes() {
        let attribute = attribute?;
        match attribute.key.as_ref() {
            b"lat" => {
                attribute
                    .decode_and_unescape_value(&reader)?
                    .parse::<f64>()
                    .map(|latitude| {
                        measurement
                            .position
                            .get_or_insert(Default::default())
                            .latitude = latitude;
                    })
                    .ok();
            }
            b"lon" => {
                attribute
                    .decode_and_unescape_value(&reader)?
                    .parse::<f64>()
                    .map(|longitude| {
                        measurement
                            .position
                            .get_or_insert(Default::default())
                            .longitude = longitude;
                    })
                    .ok();
            }
            _ => continue,
        }
    }

    loop {
        match reader.read_event()? {
            Event::Start(element) => match element.name().as_ref() {
                b"time" => {
                    let mut date_time = reader.read_text(element.name())?.to_string();
                    const DATE_TIME_WITHOUT_MILLIS: usize = 19;
                    let millis_string = date_time.split_off(DATE_TIME_WITHOUT_MILLIS);
                    let millis_string = millis_string
                        .trim_start_matches(|c: char| !c.is_ascii_digit())
                        .trim_end_matches(|c: char| !c.is_ascii_digit());
                    let millis_string = &millis_string[0..3.min(millis_string.len())];
                    let millis = millis_string.parse::<u32>().unwrap_or(0);
                    let millis = match millis_string.len() {
                        1 => millis * 100,
                        2 => millis * 10,
                        _ => millis,
                    };
                    Utc.datetime_from_str(&date_time, "%Y-%m-%dT%H:%M:%S")
                        .map(|date_time| {
                            const MILIS_TO_NANOS: u32 = 1_000_000;
                            measurement.timestamp = Duration::new(
                                date_time.timestamp() as u64,
                                millis * MILIS_TO_NANOS,
                            );
                        })
                        .ok();
                }
                b"ele" => {
                    reader
                        .read_text(element.name())?
                        .parse::<f32>()
                        .map(|elevation| {
                            measurement
                                .position
                                .get_or_insert(Default::default())
                                .altitude = elevation;
                        })
                        .ok();
                }
                b"h_acc" => {
                    reader
                        .read_text(element.name())?
                        .parse::<f32>()
                        .map(|hacc| {
                            measurement
                                .position
                                .get_or_insert(Default::default())
                                .horizontal_accuracy = hacc as u16;
                        })
                        .ok();
                }
                b"v_acc" => {
                    reader
                        .read_text(element.name())?
                        .parse::<f32>()
                        .map(|vacc| {
                            measurement
                                .position
                                .get_or_insert(Default::default())
                                .vertical_accuracy = vacc as u16;
                        })
                        .ok();
                }
                b"gpxtpx:hr" | b"ns3:hr" => {
                    reader
                        .read_text(element.name())?
                        .parse::<u16>()
                        .map(|heart_rate| {
                            if heart_rate > 0 {
                                measurement.heart_rate = Some(heart_rate);
                            }
                        })
                        .ok();
                }
                b"extensions" => continue,
                b"gpxtpx:TrackPointExtension" | b"ns3:TrackPointExtension" => continue,
                _ => {
                    // ignore unknown elements
                    reader.read_to_end(element.name())?;
                }
            },
            Event::End(element) => match element.name().as_ref() {
                b"trkpt" => break,
                b"extensions" => continue,
                b"gpxtpx:TrackPointExtension" | b"ns3:TrackPointExtension" => continue,
                _ => Err(quick_xml::Error::UnexpectedToken(
                    "Unexpected end event in 'trkpt'".into(),
                ))?,
            },
            _ => Err(quick_xml::Error::UnexpectedToken(
                "Unknown error in 'trkpt'".into(),
            ))?,
        }
    }

    Ok(measurement)
}

#[cfg(test)]
mod tests;
