// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::*;

#[test]
fn reading_kuri_gpx_export() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1"
version="1.1" Creator="Kuri"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1
http://www.topografix.com/GPX/1/1/gpx.xsd
http://www.garmin.com/xmlschemas/GpxExtensions/v3
http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd
http://www.garmin.com/xmlschemas/TrackPointExtension/v1
http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd"
xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
<metadata>
<name><![CDATA[My Activity Name]]></name>
<desc><![CDATA[A track recorded with Kuri]]></desc>
<extensions>
<kuri version="1" local_start_time="2023-07-26T01:50:03" activity="walking"/>
</extensions>
</metadata>
<trk>
<trkseg>
<trkpt lat="52.514076751230604" lon="13.359494837008492">
<time>2023-07-25T23:50:03.065Z</time>
<ele>79.03501</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.51404424748072" lon="13.359468562811937">
<time>2023-07-25T23:50:04.227Z</time>
<ele>75.68567</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.514010504096866" lon="13.359443682242134">
<time>2023-07-25T23:50:05.080Z</time>
<ele>82.58422</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
</trkseg>
<trkseg>
<trkpt lat="52.514010504096866" lon="13.359443682242134">
<time>2023-07-25T23:50:05.080Z</time>
<ele>82.58422</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.51397641600614" lon="13.35941871672512">
<time>2023-07-25T23:50:06.081Z</time>
<ele>82.425446</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
</trkseg>
<trkseg>
<trkpt lat="52.51397641600614" lon="13.35941871672512">
<time>2023-07-25T23:50:06.081Z</time>
<ele>82.425446</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.513938309777906" lon="13.359400891509448">
<time>2023-07-25T23:50:07.070Z</time>
<ele>76.39603</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
</trkseg>
<trkseg>
<trkpt lat="52.513785510560545" lon="13.359327616303652">
<time>2023-07-25T23:50:11.074Z</time>
<ele>75.29118</ele>
<extensions>
<h_acc>11</h_acc>
<v_acc>11</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.513739006513525" lon="13.359338311036671">
<time>2023-07-25T23:50:12.105Z</time>
<ele>80.50628</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.513701127855214" lon="13.359315857690245">
<time>2023-07-25T23:50:13.074Z</time>
<ele>82.367</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.513658743120644" lon="13.359313782252377">
<time>2023-07-25T23:50:14.061Z</time>
<ele>80.563934</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
</trkseg>
<trkseg>
<trkpt lat="52.513658743120644" lon="13.359313782252377">
<time>2023-07-25T23:50:14.061Z</time>
<ele>80.563934</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.51361733275802" lon="13.359306813536614">
<time>2023-07-25T23:50:15.068Z</time>
<ele>79.95338</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.51357506054602" lon="13.35930893120682">
<time>2023-07-25T23:50:16.081Z</time>
<ele>78.51857</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
</trkseg>
</trk>
</gpx>"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "kuri");

    assert_eq!(&activity.r#type, "walking");
    assert_eq!(&activity.name, "My Activity Name");
    assert_eq!(&activity.description, "A track recorded with Kuri");

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1690329003065
latitude = 52.514076751230604
longitude = 13.359494837008492
altitude = 79.03501
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1690329004227
latitude = 52.51404424748072
longitude = 13.359468562811937
altitude = 75.68567
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1690329005080
latitude = 52.514010504096866
longitude = 13.359443682242134
altitude = 82.58422
horizontal-accuracy = 9
vertical-accuracy = 9

[[section]]
[[section.measurement]]
timestamp = 1690329005080
latitude = 52.514010504096866
longitude = 13.359443682242134
altitude = 82.58422
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1690329006081
latitude = 52.51397641600614
longitude = 13.35941871672512
altitude = 82.425446
horizontal-accuracy = 10
vertical-accuracy = 10

[[section]]
[[section.measurement]]
timestamp = 1690329006081
latitude = 52.51397641600614
longitude = 13.35941871672512
altitude = 82.425446
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1690329007070
latitude = 52.513938309777906
longitude = 13.359400891509448
altitude = 76.39603
horizontal-accuracy = 10
vertical-accuracy = 10

[[section]]
[[section.measurement]]
timestamp = 1690329011074
latitude = 52.513785510560545
longitude = 13.359327616303652
altitude = 75.29118
horizontal-accuracy = 11
vertical-accuracy = 11

[[section.measurement]]
timestamp = 1690329012105
latitude = 52.513739006513525
longitude = 13.359338311036671
altitude = 80.50628
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1690329013074
latitude = 52.513701127855214
longitude = 13.359315857690245
altitude = 82.367
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1690329014061
latitude = 52.513658743120644
longitude = 13.359313782252377
altitude = 80.563934
horizontal-accuracy = 9
vertical-accuracy = 9

[[section]]
[[section.measurement]]
timestamp = 1690329014061
latitude = 52.513658743120644
longitude = 13.359313782252377
altitude = 80.563934
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1690329015068
latitude = 52.51361733275802
longitude = 13.359306813536614
altitude = 79.95338
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1690329016081
latitude = 52.51357506054602
longitude = 13.35930893120682
altitude = 78.51857
horizontal-accuracy = 9
vertical-accuracy = 9
"#
    );
}

#[test]
fn reading_legacy_gpx_with_one_track_point_works() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" Creator="Laufhelden" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
    <metadata>
        <name>Minimal</name>
        <desc>Just one track point</desc>
        <extensions>
            <meerun uid="4eef0612e799a28a" activity="walking" filtered="false" interval="1" elevationCorrected="false" manualPause="true" autoPause="false" autoPauseSensitivity="medium" gpsPause="false" createLapOnPause="false"/>
        </extensions>
    </metadata>
    <trk>
        <trkseg>
            <trkpt lat="52.5136261139637" lon="13.3612956020728">
                <time>2023-02-04T16:13:07Z</time>
                <ele>80.5790127180698</ele>
                <extensions>
                    <h_acc>10.3557777404785</h_acc>
                    <v_acc>10.3557777404785</v_acc>
                    <gpxtpx:TrackPointExtension>
                        <gpxtpx:hr>97</gpxtpx:hr>
                    </gpxtpx:TrackPointExtension>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
</gpx>"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "laufhelden");

    assert_eq!(&activity.r#type, "walking");
    assert_eq!(&activity.name, "Minimal");
    assert_eq!(&activity.description, "Just one track point");

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527187000
latitude = 52.5136261139637
longitude = 13.3612956020728
altitude = 80.57901
horizontal-accuracy = 10
vertical-accuracy = 10
heart-rate = 97
"#
    );
}

#[test]
fn reading_legacy_gpx_with_multiple_track_point_works() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" Creator="Laufhelden" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
    <metadata>
        <name>Simple</name>
        <desc>Just a few track points</desc>
        <extensions>
            <meerun uid="4eef0612e799a28a" activity="running" filtered="false" interval="1" elevationCorrected="false" manualPause="true" autoPause="false" autoPauseSensitivity="medium" gpsPause="false" createLapOnPause="false"/>
        </extensions>
    </metadata>
    <trk>
        <trkseg>
            <trkpt lat="52.5136261139637" lon="13.3612956020728">
                <time>2023-02-04T16:13:07Z</time>
                <ele>80.5790127180698</ele>
                <extensions>
                    <h_acc>10.3557777404785</h_acc>
                    <v_acc>10.3557777404785</v_acc>
                    <gpxtpx:TrackPointExtension>
                        <gpxtpx:hr>73</gpxtpx:hr>
                    </gpxtpx:TrackPointExtension>
                </extensions>
            </trkpt>
            <trkpt lat="52.5136656704478" lon="13.3612768385694">
                <time>2023-02-04T16:13:08Z</time>
                <ele>78.1007074959461</ele>
                <extensions>
                    <h_acc>9.07877063751221</h_acc>
                    <v_acc>9.07877063751221</v_acc>
                </extensions>
            </trkpt>
            <trkpt lat="52.5137110956903" lon="13.3612903972476">
                <time>2023-02-04T16:13:09Z</time>
                <ele>76.712428249327</ele>
                <extensions>
                    <h_acc>11.2644109725952</h_acc>
                    <v_acc>11.2644109725952</v_acc>
                    <gpxtpx:TrackPointExtension>
                        <gpxtpx:hr>75</gpxtpx:hr>
                    </gpxtpx:TrackPointExtension>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
</gpx>"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "laufhelden");

    assert_eq!(&activity.r#type, "running");
    assert_eq!(&activity.name, "Simple");
    assert_eq!(&activity.description, "Just a few track points");

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527187000
latitude = 52.5136261139637
longitude = 13.3612956020728
altitude = 80.57901
horizontal-accuracy = 10
vertical-accuracy = 10
heart-rate = 73

[[section.measurement]]
timestamp = 1675527188000
latitude = 52.5136656704478
longitude = 13.3612768385694
altitude = 78.10071
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1675527189000
latitude = 52.5137110956903
longitude = 13.3612903972476
altitude = 76.712425
horizontal-accuracy = 11
vertical-accuracy = 11
heart-rate = 75
"#
    );
}

#[test]
fn reading_legacy_gpx_with_multiple_track_segments_works() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" Creator="Laufhelden" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
    <metadata>
        <name>Multiple segments</name>
        <desc>A few track points distributed over multiple segments</desc>
        <extensions>
            <meerun uid="4eef0612e799a28a" activity="running" filtered="false" interval="1" elevationCorrected="false" manualPause="true" autoPause="false" autoPauseSensitivity="medium" gpsPause="false" createLapOnPause="false"/>
        </extensions>
    </metadata>
    <trk>
        <trkseg>
            <trkpt lat="52.5139871112196" lon="13.3611832356091">
                <time>2023-02-04T16:13:44Z</time>
                <ele>75.7215911370613</ele>
                <extensions>
                    <h_acc>10.8653793334961</h_acc>
                    <v_acc>10.8653793334961</v_acc>
                    <gpxtpx:TrackPointExtension>
                        <gpxtpx:hr>91</gpxtpx:hr>
                    </gpxtpx:TrackPointExtension>
                </extensions>
            </trkpt>
            <trkpt lat="52.5140162567814" lon="13.3611496444849">
                <time>2023-02-04T16:13:45Z</time>
                <ele>78.0694146217681</ele>
                <extensions>
                    <h_acc>9.41915416717529</h_acc>
                    <v_acc>9.41915416717529</v_acc>
                    <gpxtpx:TrackPointExtension>
                        <gpxtpx:hr>0</gpxtpx:hr>
                    </gpxtpx:TrackPointExtension>
                </extensions>
            </trkpt>
        </trkseg>
        <trkseg>
            <trkpt lat="52.5142796509205" lon="13.3609418979247">
                <time>2023-02-04T16:13:53Z</time>
                <ele>82.338315404485</ele>
                <extensions>
                    <h_acc>10.9895296096802</h_acc>
                    <v_acc>10.9895296096802</v_acc>
                    <gpxtpx:TrackPointExtension>
                        <gpxtpx:hr>93</gpxtpx:hr>
                    </gpxtpx:TrackPointExtension>
                </extensions>
            </trkpt>
            <trkpt lat="52.5142956823771" lon="13.3609008687397">
                <time>2023-02-04T16:13:54Z</time>
                <ele>81.3314618065469</ele>
                <extensions>
                    <h_acc>9.70725631713867</h_acc>
                    <v_acc>9.70725631713867</v_acc>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
</gpx>"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "laufhelden");

    assert_eq!(&activity.r#type, "running");
    assert_eq!(&activity.name, "Multiple segments");
    assert_eq!(
        &activity.description,
        "A few track points distributed over multiple segments"
    );

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527224000
latitude = 52.5139871112196
longitude = 13.3611832356091
altitude = 75.72159
horizontal-accuracy = 10
vertical-accuracy = 10
heart-rate = 91

[[section.measurement]]
timestamp = 1675527225000
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 9
vertical-accuracy = 9

[[section]]
[[section.measurement]]
timestamp = 1675527233000
latitude = 52.5142796509205
longitude = 13.3609418979247
altitude = 82.33832
horizontal-accuracy = 10
vertical-accuracy = 10
heart-rate = 93

[[section.measurement]]
timestamp = 1675527234000
latitude = 52.5142956823771
longitude = 13.3609008687397
altitude = 81.33146
horizontal-accuracy = 9
vertical-accuracy = 9
"#
    );
}

#[test]
fn reading_gpx_with_multiple_tracks_works() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" Creator="Whatever" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
    <metadata>
        <name>Multiple tracks</name>
        <desc>A few track points distributed over multiple tracks</desc>
    </metadata>
    <trk>
        <name>Test</name>
        <trkseg>
            <trkpt lat="52.5139871112196" lon="13.3611832356091">
                <time>2023-02-04T16:13:44Z</time>
                <ele>75.7215911370613</ele>
                <extensions>
                    <h_acc>10.8653793334961</h_acc>
                    <v_acc>10.8653793334961</v_acc>
                </extensions>
            </trkpt>
            <trkpt lat="52.5140162567814" lon="13.3611496444849">
                <time>2023-02-04T16:13:45Z</time>
                <ele>78.0694146217681</ele>
                <extensions>
                    <h_acc>9.41915416717529</h_acc>
                    <v_acc>9.41915416717529</v_acc>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
    <trk>
        <trkseg>
            <trkpt lat="52.5142796509205" lon="13.3609418979247">
                <time>2023-02-04T16:13:53Z</time>
                <ele>82.338315404485</ele>
                <extensions>
                    <h_acc>10.9895296096802</h_acc>
                    <v_acc>10.9895296096802</v_acc>
                </extensions>
            </trkpt>
            <trkpt lat="52.5142956823771" lon="13.3609008687397">
                <time>2023-02-04T16:13:54Z</time>
                <ele>81.3314618065469</ele>
                <extensions>
                    <h_acc>9.70725631713867</h_acc>
                    <v_acc>9.70725631713867</v_acc>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
</gpx>"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "whatever");

    assert_eq!(&activity.r#type, "unknown");
    assert_eq!(&activity.name, "Multiple tracks");
    assert_eq!(
        &activity.description,
        "A few track points distributed over multiple tracks"
    );

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527224000
latitude = 52.5139871112196
longitude = 13.3611832356091
altitude = 75.72159
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1675527225000
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 9
vertical-accuracy = 9

[[section]]
[[section.measurement]]
timestamp = 1675527233000
latitude = 52.5142796509205
longitude = 13.3609418979247
altitude = 82.33832
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1675527234000
latitude = 52.5142956823771
longitude = 13.3609008687397
altitude = 81.33146
horizontal-accuracy = 9
vertical-accuracy = 9
"#
    );
}

#[test]
fn reading_gpx_and_section_correction_works() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" Creator="Whatever" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
    <metadata>
        <name>Multiple tracks</name>
        <desc>A few track points distributed over multiple tracks</desc>
    </metadata>
    <trk>
        <name>Test</name>
        <trkseg>
            <trkpt lat="52.5139871112196" lon="13.3611832356091">
                <time>2023-02-04T16:13:44Z</time>
                <ele>75.7215911370613</ele>
                <extensions>
                    <h_acc>10.8653793334961</h_acc>
                    <v_acc>10.8653793334961</v_acc>
                </extensions>
            </trkpt>
            <trkpt lat="52.5140162567814" lon="13.3611496444849">
                <time>2023-02-04T16:13:45Z</time>
                <ele>78.0694146217681</ele>
                <extensions>
                    <h_acc>9.41915416717529</h_acc>
                    <v_acc>9.41915416717529</v_acc>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
    <trk>
        <trkseg>
            <trkpt lat="52.5142796509205" lon="13.3609418979247">
                <time>2023-02-04T16:13:46Z</time>
                <ele>82.338315404485</ele>
                <extensions>
                    <h_acc>10.9895296096802</h_acc>
                    <v_acc>10.9895296096802</v_acc>
                </extensions>
            </trkpt>
            <trkpt lat="52.5142956823771" lon="13.3609008687397">
                <time>2023-02-04T16:13:47Z</time>
                <ele>81.3314618065469</ele>
                <extensions>
                    <h_acc>9.70725631713867</h_acc>
                    <v_acc>9.70725631713867</v_acc>
                </extensions>
            </trkpt>
        </trkseg>
    </trk>
</gpx>"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "whatever");

    assert_eq!(&activity.r#type, "unknown");
    assert_eq!(&activity.name, "Multiple tracks");
    assert_eq!(
        &activity.description,
        "A few track points distributed over multiple tracks"
    );

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527224000
latitude = 52.5139871112196
longitude = 13.3611832356091
altitude = 75.72159
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1675527225000
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 9
vertical-accuracy = 9

[[section]]
[[section.measurement]]
timestamp = 1675527225000
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1675527226000
latitude = 52.5142796509205
longitude = 13.3609418979247
altitude = 82.33832
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1675527227000
latitude = 52.5142956823771
longitude = 13.3609008687397
altitude = 81.33146
horizontal-accuracy = 9
vertical-accuracy = 9
"#
    );
}

#[test]
fn reading_gpx_from_strava() {
    const GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx creator="StravaGPX" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
 <metadata>
  <time>2023-03-07T18:25:03Z</time>
 </metadata>
 <trk>
  <name>StravaGPX Track</name>
  <type>9</type>
  <trkseg>
   <trkpt lat="52.1644780" lon="21.0309470">
    <ele>98.3</ele>
    <time>2023-03-07T18:25:03Z</time>
    <extensions>
     <gpxtpx:TrackPointExtension>
      <gpxtpx:hr>106</gpxtpx:hr>
     </gpxtpx:TrackPointExtension>
    </extensions>
   </trkpt>
   <trkpt lat="52.1644550" lon="21.0309600">
    <ele>98.3</ele>
    <time>2023-03-07T18:25:04Z</time>
    <extensions>
     <gpxtpx:TrackPointExtension>
      <gpxtpx:hr>106</gpxtpx:hr>
     </gpxtpx:TrackPointExtension>
    </extensions>
   </trkpt>
   <trkpt lat="52.1644360" lon="21.0309790">
    <ele>98.2</ele>
    <time>2023-03-07T18:25:05Z</time>
    <extensions>
     <gpxtpx:TrackPointExtension>
      <gpxtpx:hr>106</gpxtpx:hr>
     </gpxtpx:TrackPointExtension>
    </extensions>
   </trkpt>
   <trkpt lat="52.1644170" lon="21.0310000">
    <ele>98.2</ele>
    <time>2023-03-07T18:25:06Z</time>
    <extensions>
     <gpxtpx:TrackPointExtension>
      <gpxtpx:hr>106</gpxtpx:hr>
     </gpxtpx:TrackPointExtension>
    </extensions>
   </trkpt>
  </trkseg>
 </trk>
</gpx>
"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "stravagpx");

    assert_eq!(&activity.r#type, "unknown");
    assert_eq!(&activity.name, "StravaGPX Track");
    assert_eq!(&activity.description, "");

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1678213503000
latitude = 52.164478
longitude = 21.030947
altitude = 98.3
horizontal-accuracy = 0
vertical-accuracy = 0
heart-rate = 106

[[section.measurement]]
timestamp = 1678213504000
latitude = 52.164455
longitude = 21.03096
altitude = 98.3
horizontal-accuracy = 0
vertical-accuracy = 0
heart-rate = 106

[[section.measurement]]
timestamp = 1678213505000
latitude = 52.164436
longitude = 21.030979
altitude = 98.2
horizontal-accuracy = 0
vertical-accuracy = 0
heart-rate = 106

[[section.measurement]]
timestamp = 1678213506000
latitude = 52.164417
longitude = 21.031
altitude = 98.2
horizontal-accuracy = 0
vertical-accuracy = 0
heart-rate = 106
"#
    );
}

#[test]
fn reading_gpx_from_sports_tracker() {
    const GPX: &str = r#"<?xml version="1.0" encoding="ISO-8859-1"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1"
version="1.1" creator="Sports Tracker"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1
http://www.topografix.com/GPX/1/1/gpx.xsd
http://www.garmin.com/xmlschemas/GpxExtensions/v3
http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd
http://www.garmin.com/xmlschemas/TrackPointExtension/v1
http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd"
xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1"
xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3">
<metadata>
<name><![CDATA[04-02-2023 16:02]]></name>
<desc><![CDATA[Cycling]]></desc>
<author><name><![CDATA[Foo Bar]]></name></author>
<link href="www.sports-tracker.com">
<text>Sports Tracker</text>
</link>
<time>2023-02-04T16:13:42.89</time>
</metadata>
<trk>
<name><![CDATA[04-02-2023 16:02]]></name>
<trkseg>
<trkpt lat="52.5139871112196" lon="13.3611832356091">
<ele>75.7215911370613</ele>
<desc>Speed 1.4 km/h Distance 0.00 km</desc>
<time>2023-02-04T16:13:44.17</time>
<name>1</name>
<sym>Crossing</sym>
<extensions><gpxtpx:TrackPointExtension><gpxtpx:hr>91</gpxtpx:hr></gpxtpx:TrackPointExtension></extensions>
</trkpt>
<trkpt lat="52.5140162567814" lon="13.3611496444849">
<ele>78.0694146217681</ele>
<desc>Speed 5.4 km/h Distance 0.00 km</desc>
<time>2023-02-04T16:13:45.59</time>
<name>2</name>
<sym>Dot</sym>
</trkpt>
<trkpt lat="52.5142796509205" lon="13.3609418979247">
<ele>82.338315404485</ele>
<desc>Speed 5.5 km/h Distance 0.01 km</desc>
<time>2023-02-04T16:13:46.28</time>
<name>3</name>
<sym>Dot</sym>
</trkpt>
<trkpt lat="52.5142956823771" lon="13.3609008687397">
<ele>81.3314618065469</ele>
<desc>Speed 5.0 km/h Distance 0.01 km</desc>
<time>2023-02-04T16:13:47.08</time>
<name>4</name>
<sym>Crossing</sym>
</trkpt>
</trkseg>
</trk>
</gpx>
"#;

    let (info, activity, section) = read_gpx(GPX).unwrap();

    assert_eq!(&info.source, "sports-tracker");

    assert_eq!(&activity.r#type, "unknown");
    assert_eq!(&activity.name, "04-02-2023 16:02");
    assert_eq!(&activity.description, "Cycling");

    let track = TrackRecord {
        info: TrackInfo { version: 1 },
        section,
    };

    assert_eq!(
        toml::to_string_pretty(&track).unwrap(),
        r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527224170
latitude = 52.5139871112196
longitude = 13.3611832356091
altitude = 75.72159
horizontal-accuracy = 0
vertical-accuracy = 0
heart-rate = 91

[[section.measurement]]
timestamp = 1675527225590
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 0
vertical-accuracy = 0

[[section.measurement]]
timestamp = 1675527226280
latitude = 52.5142796509205
longitude = 13.3609418979247
altitude = 82.33832
horizontal-accuracy = 0
vertical-accuracy = 0

[[section.measurement]]
timestamp = 1675527227080
latitude = 52.5142956823771
longitude = 13.3609008687397
altitude = 81.33146
horizontal-accuracy = 0
vertical-accuracy = 0
"#
    );
}
