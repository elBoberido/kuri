// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use super::*;

use crate::records::TrackRecord;

#[test]
fn export_track_with_single_section_works() {
    const SERIALIZED_TRACK: &str = r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527224013
latitude = 52.5139871112196
longitude = 13.3611832356091
altitude = 75.72159
horizontal-accuracy = 10
vertical-accuracy = 10
heart-rate = 91

[[section.measurement]]
timestamp = 1675527224973
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 9
vertical-accuracy = 9

[[section.measurement]]
timestamp = 1675527226042
latitude = 52.5142796509205
longitude = 13.3609418979247
altitude = 82.33832
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1675527227037
latitude = 52.5142956823771
longitude = 13.3609008687397
altitude = 81.33146
horizontal-accuracy = 9
vertical-accuracy = 9
"#;

    const EXPECTED_GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1"
version="1.1" Creator="Kuri"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1
http://www.topografix.com/GPX/1/1/gpx.xsd
http://www.garmin.com/xmlschemas/GpxExtensions/v3
http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd
http://www.garmin.com/xmlschemas/TrackPointExtension/v1
http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd"
xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
<metadata>
<name><![CDATA[Simple]]></name>
<desc><![CDATA[Just a few track points]]></desc>
<extensions>
<kuri version="1" local_start_time="2023-02-04T17:13:44" activity="running"/>
</extensions>
</metadata>
<trk>
<trkseg>
<trkpt lat="52.5139871112196" lon="13.3611832356091">
<time>2023-02-04T16:13:44.013Z</time>
<ele>75.72159</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
<gpxtpx:TrackPointExtension>
<gpxtpx:hr>91</gpxtpx:hr>
</gpxtpx:TrackPointExtension>
</extensions>
</trkpt>
<trkpt lat="52.5140162567814" lon="13.3611496444849">
<time>2023-02-04T16:13:44.973Z</time>
<ele>78.06941</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.5142796509205" lon="13.3609418979247">
<time>2023-02-04T16:13:46.042Z</time>
<ele>82.33832</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.5142956823771" lon="13.3609008687397">
<time>2023-02-04T16:13:47.037Z</time>
<ele>81.33146</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
</trkseg>
</trk>
</gpx>
"#;

    let track: TrackRecord = toml::from_str(SERIALIZED_TRACK).expect("deserialization failed");

    let gpx = track_to_gpx(
        "Simple",
        "Just a few track points",
        "running",
        "2023-02-04T17:13:44",
        &track.section,
    );

    assert_eq!(gpx, EXPECTED_GPX);
}

#[test]
fn export_track_with_multiple_sections_works() {
    const SERIALIZED_TRACK: &str = r#"[info]
version = 1

[[section]]
[[section.measurement]]
timestamp = 1675527224013
latitude = 52.5139871112196
longitude = 13.3611832356091
altitude = 75.72159
horizontal-accuracy = 10
vertical-accuracy = 10
heart-rate = 91

[[section.measurement]]
timestamp = 1675527225737
latitude = 52.5140162567814
longitude = 13.3611496444849
altitude = 78.06941
horizontal-accuracy = 9
vertical-accuracy = 9

[[section]]
[[section.measurement]]
timestamp = 1675527233042
latitude = 52.5142796509205
longitude = 13.3609418979247
altitude = 82.33832
horizontal-accuracy = 10
vertical-accuracy = 10

[[section.measurement]]
timestamp = 1675527234037
latitude = 52.5142956823771
longitude = 13.3609008687397
altitude = 81.33146
horizontal-accuracy = 9
vertical-accuracy = 9
"#;

    const EXPECTED_GPX: &str = r#"<?xml version="1.0" encoding="UTF-8"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1"
version="1.1" Creator="Kuri"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.topografix.com/GPX/1/1
http://www.topografix.com/GPX/1/1/gpx.xsd
http://www.garmin.com/xmlschemas/GpxExtensions/v3
http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd
http://www.garmin.com/xmlschemas/TrackPointExtension/v1
http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd"
xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3"
xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1">
<metadata>
<name><![CDATA[Simple]]></name>
<desc><![CDATA[Just a few track points]]></desc>
<extensions>
<kuri version="1" local_start_time="2023-02-04T17:13:44" activity="running"/>
</extensions>
</metadata>
<trk>
<trkseg>
<trkpt lat="52.5139871112196" lon="13.3611832356091">
<time>2023-02-04T16:13:44.013Z</time>
<ele>75.72159</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
<gpxtpx:TrackPointExtension>
<gpxtpx:hr>91</gpxtpx:hr>
</gpxtpx:TrackPointExtension>
</extensions>
</trkpt>
<trkpt lat="52.5140162567814" lon="13.3611496444849">
<time>2023-02-04T16:13:45.737Z</time>
<ele>78.06941</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
</trkseg>
<trkseg>
<trkpt lat="52.5142796509205" lon="13.3609418979247">
<time>2023-02-04T16:13:53.042Z</time>
<ele>82.33832</ele>
<extensions>
<h_acc>10</h_acc>
<v_acc>10</v_acc>
</extensions>
</trkpt>
<trkpt lat="52.5142956823771" lon="13.3609008687397">
<time>2023-02-04T16:13:54.037Z</time>
<ele>81.33146</ele>
<extensions>
<h_acc>9</h_acc>
<v_acc>9</v_acc>
</extensions>
</trkpt>
</trkseg>
</trk>
</gpx>
"#;

    let track: TrackRecord = toml::from_str(SERIALIZED_TRACK).expect("deserialization failed");

    let gpx = track_to_gpx(
        "Simple",
        "Just a few track points",
        "running",
        "2023-02-04T17:13:44",
        &track.section,
    );

    assert_eq!(gpx, EXPECTED_GPX);
}
