// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © Contributors to the kuri project
// SPDX-FileContributor: Mathias Kraus

use dirs::{config_dir, data_dir, download_dir, home_dir};
use once_cell::sync::Lazy;
use std::path::PathBuf;

pub static KURI_TRACK_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/track/";
    let mut kuri_data_dir = data_dir().expect("Path to data dir");
    kuri_data_dir.push(KURI_PATH);
    kuri_data_dir
});

pub static KURI_META_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/meta/";
    let mut kuri_data_dir = data_dir().expect("Path to data dir");
    kuri_data_dir.push(KURI_PATH);
    kuri_data_dir
});

pub static KURI_TMP_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/tmp/";
    let mut kuri_data_dir = data_dir().expect("Path to data dir");
    kuri_data_dir.push(KURI_PATH);
    kuri_data_dir
});

pub static KURI_LEGACY_DATA_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const LEGACY_PATH: &'static str = "Laufhelden/";
    let mut legacy_data_dir = home_dir().expect("Path to data dir");
    legacy_data_dir.push(LEGACY_PATH);
    legacy_data_dir
});

pub static KURI_CONFIG_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "org.kuri/kuri/";
    let mut kuri_config_dir = config_dir().expect("Path to config dir");
    kuri_config_dir.push(KURI_PATH);
    kuri_config_dir
});

pub static KURI_DEFAULT_IMPORT_SOURCE_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "kuri/import";
    let mut kuri_import_dir = download_dir().expect("Path to import dir");
    kuri_import_dir.push(KURI_PATH);
    kuri_import_dir
});

pub static KURI_DEFAULT_EXPORT_DESTINATION_DIR: Lazy<PathBuf> = Lazy::new(|| {
    const KURI_PATH: &'static str = "kuri/export";
    let mut kuri_export_dir = download_dir().expect("Path to export dir");
    kuri_export_dir.push(KURI_PATH);
    kuri_export_dir
});
