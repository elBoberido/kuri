/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import QtQml 2.2

import harbour.kuri 1.0

import "../components"
import "../tools"

RecordPageField {
    id: root

    property int heartRate: ActivityRecorder.heartRate

    label: qsTr("Heart Rate")
    value: heartRate <= 0 || heartRate >= 9999 ? "---" : heartRate
    unit: qsTr("bpm")

    CustomButton {
        anchors.fill: parent
        // TODO this is hacky and should be refactored into the HRM Device
        visible: bRecordDialogRequestHRM && (!bHRMConnected && !bHRMConnecting)

        property var displayColors: RecordPageColorTheme.colors

        backgroundColor: displayColors.sectionButton
        textColor: "white"

        text: qsTr("Reconnect HRM")

        onClicked: {
            HrmDevice.disconnectFromDevice();
            sHeartRate = "";
            sBatteryLevel = "";
            ActivityRecorder.heartRateUpdate(0);
            HrmDevice.setBluetoothType(Settings.bluetoothType);
            HrmDevice.scanServices(Settings.hrmDeviceAddress);
        }
    }
}
