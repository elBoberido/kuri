/*
 * Copyright (C) 2023 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../components/"

Page {
    id: root

    property var activity: undefined

    allowedOrientations: Orientation.Portrait

    Component.onCompleted: Window.activeChanged.connect(updateCharts)
    Component.onDestruction: Window.activeChanged.disconnect(updateCharts)

    function updateCharts() {
        if (Window.active) {
            primaryChart.updateChart();
            secondaryChart.updateChart();
            heartRateChart.updateChart();
        }
    }

    ColumnLayout {
        anchors.fill: parent

        PageHeader
        {
            id: header
            Layout.fillWidth: true
            title: qsTr("Charts")
        }

        Item {
            id: chartView
            Layout.fillHeight: true
            Layout.fillWidth: true

            property var dataRangeX: ({min: 0, max: 4})

            property string lineType: "bezier"
            property int lineWidth: Theme.pixelRatio * 4
            property bool showDots: false

            property string fontFamily: Theme.fontFamily
            property int titleFontSize: Theme.fontSizeSmall
            property int labelFontSize: Theme.fontSizeExtraSmall

            property string titleColor: Theme.highlightColor
            property string labelColor: Theme.secondaryHighlightColor
            property string frameColor: Theme.secondaryColor
            property string cursorColor: Theme.secondaryHighlightColor

            function chartDataReady() {
                primaryChart.dataSets = ActivityHistoryTrack.getChartSpeedPoints();
                secondaryChart.dataSets = ActivityHistoryTrack.getChartElevationPoints();
                heartRateChart.dataSets = ActivityHistoryTrack.getChartHeartRatePoints();

                primaryChart.dataRangeX.max = ActivityHistoryTrack.chartDataPointRangeEnd;
                secondaryChart.dataRangeX.max = ActivityHistoryTrack.chartDataPointRangeEnd;
                heartRateChart.dataRangeX.max = ActivityHistoryTrack.chartDataPointRangeEnd;

                primaryChart.updateChart();
                secondaryChart.updateChart();
                heartRateChart.updateChart();
            }

            ColumnLayout {
                anchors.fill: parent

                Chart {
                    id: primaryChart
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    chartTitle: qsTr("Speed") + " [km/h]"
                    dataSets: []
                    dataRangeX: chartView.dataRangeX
                    showLabelY: true
                    showTopBottomFrame: true
                    lineType: chartView.lineType
                    lineWidth: chartView.lineWidth
                    showDots: chartView.showDots

                    fontFamily: chartView.fontFamily
                    titleFontSize: chartView.titleFontSize
                    labelFontSize: chartView.labelFontSize

                    titleColor: chartView.titleColor
                    labelColor: chartView.labelColor
                    frameColor: chartView.frameColor
                    cursorColor: chartView.cursorColor

                    lineColor: "#228bc0"

                    cursorPosition: (slider.value - slider.minimumValue) / (slider.maximumValue - slider.minimumValue)

                    Component.onCompleted: {
                        ActivityHistoryTrack.chartDataReady.connect(chartView.chartDataReady);
                        ActivityHistoryTrack.emitChartDataReady();
                    }

                    Component.onDestruction: ActivityHistoryTrack.chartDataReady.disconnect(chartView.chartDataReady)
                }

                Chart {
                    id: secondaryChart
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    chartTitle: qsTr("Elevation") + " [m]"
                    dataSets: []
                    dataRangeX: chartView.dataRangeX
                    showLabelY: true
                    showTopBottomFrame: true
                    lineType: chartView.lineType
                    lineWidth: chartView.lineWidth
                    showDots: chartView.showDots

                    fontFamily: chartView.fontFamily
                    titleFontSize: chartView.titleFontSize
                    labelFontSize: chartView.labelFontSize

                    titleColor: chartView.titleColor
                    labelColor: chartView.labelColor
                    frameColor: chartView.frameColor
                    cursorColor: chartView.cursorColor

                    lineColor: "#22c071"

                    cursorPosition: (slider.value - slider.minimumValue) / (slider.maximumValue - slider.minimumValue)
                }

                Chart {
                    id: heartRateChart
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    visible: activity.heartRateAverage > 0

                    chartTitle: qsTr("Heart Rate") + " [bpm]"
                    dataSets: []
                    dataRangeX: chartView.dataRangeX
                    showLabelY: true
                    showTopBottomFrame: true
                    lineType: chartView.lineType
                    lineWidth: chartView.lineWidth
                    showDots: chartView.showDots

                    fontFamily: chartView.fontFamily
                    titleFontSize: chartView.titleFontSize
                    labelFontSize: chartView.labelFontSize

                    titleColor: chartView.titleColor
                    labelColor: chartView.labelColor
                    frameColor: chartView.frameColor
                    cursorColor: chartView.cursorColor

                    lineColor: "#c022c0"

                    cursorPosition: (slider.value - slider.minimumValue) / (slider.maximumValue - slider.minimumValue)
                }

                // TODO add bar chart for average speed for 0.5, 1, 2, 5 and section average speed
            }
        }

        TrackSlider {
            id: slider
            Layout.fillWidth: true

            minimumValue: 0
            maximumValue: ActivityHistoryTrack.totalPointCount < 2 ? 1 : ActivityHistoryTrack.totalPointCount - 1

            value: ActivityHistoryTrack.currentIndex

            onValueChanged: ActivityHistoryTrack.currentIndex = slider.value.toFixed(0)

            Component.onCompleted: ActivityHistoryTrack.currentIndexChanged.connect(setValue)
            Component.onDestruction: ActivityHistoryTrack.currentIndexChanged.disconnect(setValue)

            function setValue(index) {
                slider.value = ActivityHistoryTrack.currentIndex
            }
        }

        RowLayout {
            id: pageIndicator
            Layout.fillWidth: true
            spacing: radiusSmall

            readonly property int radiusSmall: Theme.paddingSmall
            readonly property int radius: 1.6 * radiusSmall

            Item {
                Layout.fillWidth: true
                height: 7 * pageIndicator.radiusSmall
            }

            Rectangle {
                radius: pageIndicator.radiusSmall
                width: 2 * radius
                height: width
                Layout.alignment: Qt.AlignVCenter

                color: Theme.secondaryColor
            }

            Rectangle {
                radius: pageIndicator.radius
                width: 2 * radius
                height: width
                Layout.alignment: Qt.AlignVCenter

                color: Theme.primaryColor
            }

            Item {
                Layout.fillWidth: true
            }
        }
    }
}
