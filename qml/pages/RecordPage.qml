/*
 * Copyright (C) 2017 Jens Drescher
 * Copyright (C) 2017 - 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0
import Nemo.DBus 2.0

import harbour.kuri 1.0

import "../components"
import "../fields"
import "../tools"

import "../tools/SharedResources.js" as SharedResources
import "../fields/FieldLayout.js" as FieldLayout
import "../fields/FieldUtils.js" as FieldUtils

Page {
    id: page
    allowedOrientations: Orientation.Portrait

    backNavigation: !(ActivityRecorder.recording || map.maximized || lockScreen.visible)

    property var displayColors: RecordPageColorTheme.colors

    DBusInterface {
        id: dbusHRM

        service: "org.sailfishos.heartrate"
        iface: "org.sailfishos.heartrate"
        path: "/"

        Component.onCompleted: {
            console.log("#### dbusHRM completed");
        }
    }

    // only for debugging
    //DBusAdaptor {
        //id: dbusHRMSimulator

        //service: "org.sailfishos.heartrate"
        //iface: "org.sailfishos.heartrate"
        //path: "/"

        //property int simulatedHeartRate: 120

        //function start() {
        //}
        //function stop() {
        //}
        //function heartRate() {
            //simulatedHeartRate += 1;
            //if (simulatedHeartRate > 130) {
                //simulatedHeartRate = 110;
            //}
            //return simulatedHeartRate;
        //}
        //function batteryLevel() {
            //return 42;
        //}
    //}

    Component.onCompleted: {
        RecordPageColorTheme.id = Settings.displayMode;
    }

    onStatusChanged: {
        switch (status) {
            case PageStatus.Inactive:
                // nothing to do, move along
                break;
            case PageStatus.Activating:
                GeoPositionInfo.startUpdates();

                if (Settings.useHRMservice) {
                    dbusHRM.call("start");
                } else if (Settings.hrmDeviceAddress !== "" && Settings.useHRMdevice && bRecordDialogRequestHRM === false) {
                    HrmDevice.setBluetoothType(Settings.bluetoothType);
                    HrmDevice.scanServices(Settings.hrmDeviceAddress);
                    bRecordDialogRequestHRM = true;
                }

                ActivityRecorder.newTrackPoint.connect(newTrackPoint);

                updateTimer.running = true;
                map.mapStyle = Settings.mapStyle
                break;
            case PageStatus.Active:
                // nothing to do, move along
                break;
            case PageStatus.Deactivating:
                updateTimer.running = false;

                GeoPositionInfo.stopUpdates();

                if (Settings.useHRMservice) {
                    dbusHRM.call("stop");
                } else if (Settings.hrmDeviceAddress !== "" && Settings.useHRMdevice) {
                    bRecordDialogRequestHRM = false;
                }

                ActivityRecorder.newTrackPoint.disconnect(newTrackPoint);
                break;
        }
    }

    function newTrackPoint(point) {
        map.addMapPoint(point);
        map.updateTrack();
    }

    function showSaveDialog()
    {
        ActivityRecorder.stop();
        if (bHRMConnected || bHRMConnecting) { HrmDevice.disconnectFromDevice(); }

        //If autosave is active...
        if (Settings.enableAutosave)
        {
            console.log("Autosaving workout");

            ActivityRecorder.setActivityName(SharedResources.arrayLookupWorkoutTableByName[Settings.activityType].labeltext);
            ActivityRecorder.setActivityDescription("");
            ActivityRecorder.saveRecording();

            //We must return here to the mainpage.
            pageStack.pop(vMainPageObject, PageStackAction.Immediate);
        }
        else
        {
            var dialog = pageStack.push(Qt.resolvedUrl("SaveDialog.qml"));
            dialog.accepted.connect(function()
            {
                console.log("Saving workout");

                ActivityRecorder.setActivityName(dialog.name);
                ActivityRecorder.setActivityDescription(dialog.description);
                ActivityRecorder.saveRecording();

                //We must return here to the mainpage.
                pageStack.pop(vMainPageObject, PageStackAction.Immediate);
            })
            dialog.rejected.connect(function()
            {
                console.log("Cancel workout");

                ActivityRecorder.discardRecording();

                //We must return here to the mainpage.
                pageStack.pop(vMainPageObject, PageStackAction.Immediate);
            })
        }
    }

    Timer {
        id: updateTimer
        interval: 1000;
        running: false;
        repeat: true

        onTriggered: {
            timeView.update();

            if (Settings.useHRMservice)
            {
                dbusHRM.typedCall("heartRate", [], function(result) {
                    sHeartRate = result;
                }, function() {//error ocurred
                    Settings.useHRMservice = false;
                    fncShowMessage("Error",qsTr("HRM service not found"), 5000);
                });
                dbusHRM.typedCall("batteryLevel", [], function(result) {
                    // this is in percent
                    sBatteryLevel = result;
                });

                ActivityRecorder.heartRateUpdate(parseInt(sHeartRate));
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: displayColors.background
    }


    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: page.height
        visible: !lockScreen.visible

        PullDownMenu
        {
            MenuItem
            {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsMenu.qml"))
            }
            MenuItem
            {
                text: qsTr("Switch Color Theme")
                onClicked:
                {
                    RecordPageColorTheme.nextTheme();
                    Settings.displayMode = RecordPageColorTheme.id;
                }
            }
            MenuItem
            {
                text: mapView.visible ?  qsTr("Hide Map") :  qsTr("Show Map")
                onClicked:
                {
                    mapView.visible = !mapView.visible
                    Settings.showMapRecordPage = mapView.visible;
                }
            }
            MenuItem
            {
                text: qsTr("Lock Screen")
                onClicked: lockScreen.visible = !lockScreen.visible
            }
        }

        ColumnLayout {
            spacing: 0
            anchors.leftMargin: Theme.paddingMedium
            anchors.rightMargin: Theme.paddingMedium
            anchors.bottomMargin: Theme.paddingMedium
            anchors.fill: parent

            RowLayout {
                id: statusBar
                Layout.fillWidth: true
                Layout.columnSpan: 2
                spacing: Theme.paddingMedium

                GPSIndicator {
                    id: gps
                    Layout.fillWidth: true

                    height: Theme.itemSizeExtraSmall / 5 * 3
                    indicatorWidth: page.width / 5

                    accuracy: GeoPositionInfo.accuracy
                    accuracyThreshold: Settings.positionAccuracyThreshold
                }

                Rectangle {
                    id: timeView

                    width: timeLabel.width
                    height: Theme.itemSizeExtraSmall / 5 * 3
                    color: "transparent"

                    function update() {
                        var date = new Date();
                        timeLabel.text = FieldUtils.pad(date.getHours().toString(), 2) + ":" + FieldUtils.pad(date.getMinutes().toString(), 2) + ":" + FieldUtils.pad(date.getSeconds().toString(), 2) + " ";
                    }

                    Text {
                        id: timeLabel
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeExtraSmall
                        color: displayColors.secondary

                        text: ""

                        Component.onCompleted: timeView.update()
                    }
                }

                HRMBatteryIndicator {
                    id: hrmBatteryIndicator
                    Layout.fillWidth: true

                    height: Theme.itemSizeExtraSmall / 5 * 3
                    indicatorWidth: page.width / 5

                    batteryLevel: sBatteryLevel === "" ? -1. : parseInt(sBatteryLevel)
                }
            }

            Separator
            {
                Layout.fillWidth: true
                Layout.columnSpan: 2

                color: displayColors.secondaryHighlight
                horizontalAlignment: Qt.AlignHCenter
                visible: false
            }

            Item {
                id: mainView
                Layout.fillWidth: true
                Layout.fillHeight: true

                GridLayout {
                    anchors.fill: parent
                    columns: 2
                    rowSpacing: 0
                    columnSpacing: 0

                    RecordPageFieldLoader {
                        id: field_0_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_0_1.visible ? 1 : 2

                        row: 0
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_0_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_0_0.visible ? 1 : 2

                        row: 0
                        column: 1
                    }

                    Separator
                    {
                        Layout.fillWidth: true
                        Layout.columnSpan: 2

                        color: displayColors.secondaryHighlight
                        horizontalAlignment: Qt.AlignHCenter

                        visible: field_1_0.visible || field_1_1.visible
                    }

                    RecordPageFieldLoader {
                        id: field_1_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_1_1.visible ? 1 : 2

                        row: 1
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_1_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_1_0.visible ? 1 : 2

                        row: 1
                        column: 1
                    }

                    Separator
                    {
                        id: separator
                        Layout.fillWidth: true
                        Layout.columnSpan: 2

                        color: displayColors.secondaryHighlight
                        horizontalAlignment: Qt.AlignHCenter

                        visible: field_2_0.visible || field_2_1.visible
                    }

                    RecordPageFieldLoader {
                        id: field_2_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_2_1.visible ? 1 : 2

                        row: 2
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_2_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_2_0.visible ? 1 : 2

                        row: 2
                        column: 1
                    }

                    Separator
                    {
                        Layout.fillWidth: true
                        Layout.columnSpan: 2

                        color: displayColors.secondaryHighlight
                        horizontalAlignment: Qt.AlignHCenter

                        visible: field_3_0.visible || field_3_1.visible
                    }

                    RecordPageFieldLoader {
                        id: field_3_0

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_3_1.visible ? 1 : 2

                        row: 3
                        column: 0
                    }

                    RecordPageFieldLoader {
                        id: field_3_1

                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.columnSpan: field_3_0.visible ? 1 : 2

                        row: 3
                        column: 1
                    }
                }

                Rectangle {
                    id: mapView
                    width: page.width
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: mapAtTop ? parent.top : undefined
                    anchors.bottom: mapAtBottom ? parent.bottom : undefined
                    visible: Settings.showMapRecordPage

                    states: [
                        State {
                            when: map.maximized
                            ParentChange { target: mapView; parent: page }
                            PropertyChanges { target: mapView; anchors.bottomMargin: 0; height: parent.height }
                        },
                        State {
                            when: !map.maximized
                            ParentChange { target: mapView; parent: mainView }
                            PropertyChanges { target: mapView; anchors.bottomMargin: Theme.paddingMedium; height: parent.height / 2 }
                        }
                    ]

                    //TODO: opacity transition on visibility change
                    //TODO: position transition on mouse release

                    property int moveStartPosition: 0
                    property bool moving: false

                    property bool mapAtTop: false
                    property bool mapAtBottom: true

                    readonly property int yMin: 0
                    readonly property int yMax: parent.height - height - Theme.paddingMedium
                    readonly property int yThreshold: yMin + (yMax - yMin) / 2

                    MouseArea {
                        anchors.fill: parent
                        preventStealing: true
                        onPressed: {
                            mapView.moving = true;
                            mapView.moveStartPosition = mouse.y;
                            mapView.mapAtTop = false;
                            mapView.mapAtBottom = false;
                        }
                        onReleased: {
                            mapView.moving = false;
                            if(mapView.y < mapView.yThreshold) {
                                mapView.mapAtTop = true;
                            } else {
                                mapView.mapAtBottom = true;
                            }
                        }
                        onMouseYChanged: {
                            if(mapView.moving) {
                                mapView.y = mapView.y - (mapView.moveStartPosition - mouse.y);
                                if (mapView.y < mapView.yMin) {
                                    mapView.y = mapView.yMin;
                                } else if (mapView.y > mapView.yMax) {
                                    mapView.y = mapView.yMax;
                                }
                            }
                        }
                    }

                    Map {
                        id: map
                        anchors.fill: parent

                        mapMode: mapModeTracking

                        mapGesturesEnabled: map.maximized
                        showCenterPositionButton: map.maximized

                        currentPosition: GeoPositionInfo.currentPosition

                        onShowSettings: pageStack.push(Qt.resolvedUrl("MapSettingsPage.qml"))
                    }
                }
            }

            CustomButton {
                id: startButton
                Layout.fillWidth: true

                backgroundColor: displayColors.startButton
                textColor: "white"

                text: qsTr("Start")

                onClicked: {
                    keepPressed = true
                    ActivityRecorder.start();
                }

                // for some reasons a Connection to ActivityRecorder did not work
                property bool recording: ActivityRecorder.recording
                onRecordingChanged: {
                    if(startButton.pressed) {
                        startButton.visible = false
                        startButton.keepPressed = false

                        newTrackPoint(ActivityRecorder.latestTrackPoint(TrackPoint.Start));
                    }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                spacing: Theme.paddingMedium
                visible: !startButton.visible

                CustomButton {
                    id: sectionButton
                    Layout.fillWidth: true

                    backgroundColor: enabled ? displayColors.sectionButton : "gray"
                    textColor: "white"

                    text: qsTr("Section")

                    onClicked: {
                        ActivityRecorder.nextSection();
                        newTrackPoint(ActivityRecorder.latestTrackPoint(TrackPoint.SectionCrossing));
                    }
                }
                CustomButton {
                    id: pauseButton
                    Layout.preferredWidth: pauseButton.height

                    backgroundColor: ActivityRecorder.pausing ? displayColors.startButton : displayColors.pauseButton
                    textColor: "white"

                    fontSize: ActivityRecorder.pausing ? Theme.fontSizeLarge : Theme.fontSizeExtraLarge

                    text: ActivityRecorder.pausing ? "\u25B6" : "II"

                    onClicked: {
                        if (ActivityRecorder.pausing) {
                            ActivityRecorder.resume();
                            newTrackPoint(ActivityRecorder.latestTrackPoint(TrackPoint.Resume));
                        } else {
                            ActivityRecorder.pause();
                            newTrackPoint(ActivityRecorder.latestTrackPoint(TrackPoint.Pause));
                        }
                    }
                }
                CustomButton {
                    id: stopButton
                    Layout.fillWidth: true

                    slide: true
                    backgroundColor: displayColors.stopButton
                    textColor: "white"

                    text: qsTr("Stop")
                    textLeft: "🡅"
                    textRight: "🡅"

                    onClicked: {
                        newTrackPoint(ActivityRecorder.latestTrackPoint(TrackPoint.Stop));
                        showSaveDialog();
                    }
                }
            }
        }
    }

    RecordPageLockScreen {
        id: lockScreen
        anchors.fill: parent
        color: displayColors.lockScreen
        unlockButtonColor: displayColors.primary
        unlockButtonHighlightColor: displayColors.highlight
        visible: false
    }
}
