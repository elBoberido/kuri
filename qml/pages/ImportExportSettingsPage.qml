/*
 * Copyright (C) 2023 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.0
import QtQuick.Layouts 1.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

Page
{
    allowedOrientations: Orientation.Portrait

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}

        ColumnLayout
        {
            id: column
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: Theme.horizontalPageMargin
            anchors.rightMargin: Theme.horizontalPageMargin
            spacing: Theme.paddingLarge

            PageHeader
            {
                Layout.fillWidth: true
                title: qsTr("Import/Export")
            }

            SectionHeader
            {
                Layout.fillWidth: true
                text: qsTr("Export")
            }
            TextField
            {
                Layout.fillWidth: true
                readOnly: true
                label: qsTr("Destination folder")
                text: Settings.exportDestinationFolder
            }
            Button
            {
                Layout.fillWidth: true
                text: qsTr("Export")
                enabled: !ActivityHistory.activityExportOngoing
                onClicked: {
                    ActivityHistory.activityExportOngoingChanged.connect(showExportFinished);
                    ActivityHistory.exportActivities();
                }

                function showExportFinished() {
                    if (!ActivityHistory.activityExportOngoing) {
                        ActivityHistory.activityExportOngoingChanged.disconnect(showExportFinished);
                        fncShowMessage("Info", qsTr("Activity export finished"), 5000);
                    }
                }
            }

            SectionHeader
            {
                Layout.fillWidth: true
                text: qsTr("Import")
            }
            TextField
            {
                Layout.fillWidth: true
                readOnly: true
                label: qsTr("Source folder")
                text: Settings.importSourceFolder
            }
            TextSwitch
            {
                Layout.fillWidth: true
                text: qsTr("Override existing")
                description: qsTr("Already imported activities will be overridden")
                Component.onCompleted: {
                    checked = Settings.overrideImportedActivities;
                    checkedChanged.connect(function() {
                        Settings.overrideImportedActivities = checked;
                    });
                }
            }
            Button
            {
                Layout.fillWidth: true
                text: qsTr("Import")
                enabled: !ActivityHistory.activityImportOngoing
                onClicked: {
                    ActivityHistory.activityImportOngoingChanged.connect(showImportFinished);
                    ActivityHistory.importActivities();
                }

                function showImportFinished() {
                    if (!ActivityHistory.activityImportOngoing) {
                        ActivityHistory.activityImportOngoingChanged.disconnect(showImportFinished);
                        fncShowMessage("Info", qsTr("Activity import finished"), 5000);
                    }
                }
            }

            SectionHeader
            {
                Layout.fillWidth: true
                text: qsTr("Migration")
            }
            Label
            {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
                text: qsTr("Previously migrated activities will be overridden")
            }
            Button
            {
                Layout.fillWidth: true
                text: qsTr("Redo migration")
                enabled: !ActivityHistory.activityMigrationOngoing
                onClicked: {
                    ActivityHistory.activityMigrationOngoingChanged.connect(showMigrationFinished);
                    ActivityHistory.redoActivityMigrationFromLegacyPath();
                }

                function showMigrationFinished() {
                    if (!ActivityHistory.activityMigrationOngoing) {
                        ActivityHistory.activityMigrationOngoingChanged.disconnect(showMigrationFinished);
                        fncShowMessage("Info", qsTr("Activity migration finished"), 5000);
                    }
                }
            }
        }

    }

    BusyIndicator
    {
        anchors.centerIn: parent

        visible: true

        running: ActivityHistory.activityImportOngoing | ActivityHistory.activityExportOngoing | ActivityHistory.activityMigrationOngoing
        size: BusyIndicatorSize.Large
    }
}
