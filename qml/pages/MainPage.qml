/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools/SharedResources.js" as SharedResources

Page
{
    id: mainPage

    allowedOrientations: Orientation.Portrait

    Component.onCompleted:
    {
        console.log("First Active MainPage");
    }

    onStatusChanged:
    {
        //This is loaded everytime the page is displayed
        if (status === PageStatus.Active)
        {
            console.log("Active MainPage");

            //stop positioning
            GeoPositionInfo.stopUpdates();

            //Save the object of this page for back jumps
            vMainPageObject = pageStack.currentPage;
            console.log("vMainPageObject: " + vMainPageObject.toString());
        }
    }

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: mainPage.height
        contentWidth: mainPage.width

        PullDownMenu
        {
            id: menu
            MenuItem
            {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem
            {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("SettingsMenu.qml"))
            }
            MenuItem
            {
                text: qsTr("My Strava activities")
                visible: stravaAPI.linked
                onClicked: {

                    var dialog = pageStack.push(Qt.resolvedUrl("MyStravaActivities.qml"));
                }
            }
            MenuItem
            {
                text: qsTr("Start activity")
                onClicked: pageStack.push(Qt.resolvedUrl("PreRecordPage.qml"))
            }
        }

        ColumnLayout
        {
            spacing: 0
            anchors.fill: parent
            width: parent.width

            PageHeader
            {
                id: pageHeader
                title: "Kuri"

                RowLayout
                {
                    id: filterSelection
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: Theme.paddingLarge

                    width: parent.width
                    spacing: Theme.paddingSmall

                    visible: ActivityHistory.hasVariousActivityTypes || (cmbWorkoutFilter.currentIndex !== 0 && activityHistoryList.count === 0)

                    Item
                    {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        height: Theme.iconSizeSmallPlus
                        width: Theme.iconSizeSmallPlus

                        Image
                        {
                            anchors.fill: parent
                            fillMode: Image.PreserveAspectFit
                            source: SharedResources.arrayWorkoutTypesFilterMainPage[cmbWorkoutFilter.currentIndex].icon
                        }
                    }

                    ComboBox
                    {
                        id: cmbWorkoutFilter
                        Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft
                        Layout.fillWidth: true
                        menu: ContextMenu
                        {
                            Repeater
                            {
                                id: idRepeaterFilterWorkout
                                model: SharedResources.arrayWorkoutTypesFilterMainPage;
                                MenuItem { text: modelData.labeltext }
                            }
                        }
                        Component.onCompleted: {
                            currentIndex = SharedResources.arrayWorkoutTypesFilterMainPage.map(function(e) {
                                return e.name;
                            }).indexOf(ActivityHistory.activityTypeFilter);

                            currentIndexChanged.connect(function() {
                                ActivityHistory.activityTypeFilter = SharedResources.arrayWorkoutTypesFilterMainPage[currentIndex].name;
                            });
                        }
                    }
                }
            }

            GridLayout
            {
                id: mainHeader
                width: parent.width
                columns: 2
                rowSpacing: 0
                columnSpacing: Theme.paddingSmall

                Item
                {
                    Layout.fillWidth: true
                    height: Theme.iconSizeMedium
                    Image
                    {
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "../icons/journey-yellow.png"
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.iconSizeMedium
                    Image
                    {
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "../icons/time-green.png"
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.iconSizeMedium
                    visible: false
                    Image
                    {
                        anchors.fill: parent
                        fillMode: Image.PreserveAspectFit
                        source: "../icons/fire-orange.png"
                    }
                }

                Item
                {
                    Layout.fillWidth: true
                    height: Theme.fontSizeSmall * 2
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        property real distanceInKm: ActivityHistory.activityDistanceFromFilter / 1000000.
                        property int decimals: (distanceInKm < 10) ? 2 : ((distanceInKm < 100) ? 1 : 0)

                        text: distanceInKm.toFixed(decimals) + " " +  qsTr("km");
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.primaryColor
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.fontSizeSmall * 2
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: (ActivityHistory.activityDurationFromFilter / (60 * 60)).toFixed(1) + " " + qsTr("hrs")
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.primaryColor
                    }
                }
                Item
                {
                    Layout.fillWidth: true
                    height: Theme.fontSizeSmall * 2
                    visible: false
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: "here comes the calories"
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.primaryColor
                    }
                }
                Item {
                    Layout.fillWidth: true
                    Layout.columnSpan: 2
                    height: Theme.fontSizeMedium * 2
                    Label
                    {
                        anchors.fill: parent
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        truncationMode: TruncationMode.Fade

                        text: qsTr("%1 activities").arg(activityHistoryList.count)
                        font.pixelSize: Theme.fontSizeMedium
                        color: Theme.primaryColor
                    }
                }
            }

            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }

            SilicaListView
            {
                Layout.fillHeight: true
                width: parent.width
                id: activityHistoryList
                clip: true
                model: ActivityHistory

                section.property: "intervalSummaryId"
                section.delegate: Item {
                    width: parent.width
                    height: Theme.fontSizeSmall * 2

                    RowLayout
                    {
                        anchors.fill: parent
                        anchors.leftMargin: Theme.paddingMedium
                        anchors.rightMargin: Theme.paddingMedium
                        spacing: Theme.paddingMedium

                        Text
                        {
                            Layout.fillWidth: true
                            height: parent.height
                            horizontalAlignment: Text.AlignLeft
                            verticalAlignment: Text.AlignVCenter
                            elide: Text.ElideRight

                            text: ActivityHistory.intervalSummaryName(section)
                            font.pixelSize: Theme.fontSizeSmall
                            color: Theme.primaryColor
                        }
                        Text
                        {
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignVCenter

                            text: StringFormatter.secondsToHoursMinutesSeconds(ActivityHistory.intervalSummaryDuration(section))
                            font.pixelSize: Theme.fontSizeExtraSmall
                            color: Theme.primaryColor
                        }
                        Text
                        {
                            horizontalAlignment: Text.AlignRight
                            verticalAlignment: Text.AlignVCenter

                            text: (ActivityHistory.intervalSummaryDistance(section) / 1000000.).toFixed(2) + " " + qsTr("km")
                            font.pixelSize: Theme.fontSizeExtraSmall
                            color: Theme.primaryColor
                        }
                    }
                }

                delegate: ListItem
                {
                    id: activityListItem
                    width: parent.width
                    opacity: migrationInfo.visible ? 0.5 : 1.0

                    ListView.onRemove: animateRemoval()
                    menu: ContextMenu
                    {
                        MenuItem
                        {
                            text: qsTr("Edit")
                            onClicked: pageStack.push(Qt.resolvedUrl("EditActivityDialog.qml"), { activity: model })
                        }
                        MenuItem
                        {
                            text: qsTr("Remove")
                            onClicked: remorseAction(qsTr("Removing activity..."), activityListItem.deleteActivity)
                        }
                    }

                    function deleteActivity()
                    {
                        ActivityHistory.deleteActivity(activityId, index);
                    }

                    GridLayout
                    {
                        anchors.fill: parent
                        anchors.leftMargin: Theme.paddingMedium
                        anchors.rightMargin: Theme.paddingMedium
                        columns: 2
                        rowSpacing: 0
                        columnSpacing: Theme.paddingMedium
                        Item
                        {
                            Layout.rowSpan: 2
                            Layout.fillHeight: true
                            width: Theme.iconSizeSmall * 1.5

                            Image
                            {
                                anchors.fill: parent
                                fillMode: Image.PreserveAspectFit
                                source: activityType === "" ? "" : SharedResources.arrayLookupWorkoutTableByName[activityType].icon
                            }
                        }
                        Item
                        {
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            RowLayout
                            {
                                anchors.fill: parent
                                spacing: Theme.paddingMedium

                                Text
                                {
                                    Layout.fillWidth: true
                                    height: parent.height
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                    elide: Text.ElideRight

                                    text: dateTimeShort
                                    font.pixelSize: Theme.fontSizeSmall
                                    color: Theme.highlightColor
                                }
                                Text
                                {
                                    horizontalAlignment: Text.AlignRight
                                    verticalAlignment: Text.AlignVCenter

                                    text: StringFormatter.secondsToHoursMinutesSeconds(duration)
                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: "green"
                                }
                                Text
                                {
                                    horizontalAlignment: Text.AlignRight
                                    verticalAlignment: Text.AlignVCenter

                                    text: (distance / 1000000.).toFixed(2) + " " + qsTr("km")
                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: Theme.highlightColor
                                }
                            }
                        }
                        Item
                        {
                            Layout.fillWidth: true
                            Layout.fillHeight: true

                            RowLayout
                            {
                                anchors.fill: parent
                                spacing: Theme.paddingMedium

                                Text
                                {
                                    Layout.fillWidth: true
                                    height: parent.height
                                    horizontalAlignment: Text.AlignLeft
                                    verticalAlignment: Text.AlignVCenter
                                    elide: Text.ElideRight

                                    text: name==="" ? SharedResources.arrayLookupWorkoutTableByName[activityType].labeltext : name
                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: Theme.primaryColor
                                }
                                Text
                                {
                                    horizontalAlignment: Text.AlignRight
                                    verticalAlignment: Text.AlignVCenter

                                    text: speedAverage.toFixed(1) + " " + qsTr("km/h")
                                    font.pixelSize: Theme.fontSizeExtraSmall
                                    color: Theme.secondaryColor
                                }
                            }
                        }
                    }

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("DetailedViewPage.qml"), { activity: model });
                        pageStack.pushAttached(Qt.resolvedUrl("ChartViewPage.qml"), { activity: model });
                    }
                }

                Item {
                    id: migrationInfo
                    anchors.fill: parent
                    visible: activityMigratonBusyIndicator.running

                    Text {
                        anchors.centerIn: parent

                        font.pixelSize: Theme.fontSizeLarge
                        color: Theme.secondaryColor

                        text: qsTr("Migrating old activities")
                    }

                    BusyIndicator
                    {
                        id: activityMigratonBusyIndicator
                        anchors.centerIn: parent

                        visible: true

                        running: ActivityHistory.activityMigrationOngoing
                        size: BusyIndicatorSize.Large
                    }
                }
            }
        }
    }
}
