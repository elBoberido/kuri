/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

import "../tools/SharedResources.js" as SharedResources

Page
{
    id: page

    allowedOrientations: Orientation.Portrait

    property bool bLockFirstPageLoad: true

    onStatusChanged:
    {
        //This is loaded only the first time the page is displayed
        if (status === PageStatus.Active && bLockFirstPageLoad)
        {
            bLockFirstPageLoad = false;

            console.log("First Active PreRecordPage");

            pageStack.pushAttached(Qt.resolvedUrl("RecordPage.qml"));
        }

        //This is loaded everytime the page is displayed
        if (status === PageStatus.Active)
        {
            console.log("PreRecordPage active");

            if (bHRMConnected)
            {
                bRecordDialogRequestHRM = false;
                HrmDevice.disconnectFromDevice();
            }

            //We might returned from record page and HR reconnect is still active. Switch it off.
            if (bRecordDialogRequestHRM)
                bRecordDialogRequestHRM = false;
        }

        if (status === PageStatus.Inactive)
        {
            console.log("PreRecordPage inactive");

        }
    }

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}

        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("Let's go!")
            }
            Row
            {
                spacing: Theme.paddingSmall
                width:parent.width;
                Image
                {
                    id: imgWorkoutImage
                    height: parent.width / 8
                    width: parent.width / 8
                    fillMode: Image.PreserveAspectFit
                }
                ComboBox
                {
                    width: (parent.width / 8) * 7
                    label: qsTr("Activity:")
                    menu: ContextMenu
                    {
                        Repeater
                        {
                            model: SharedResources.arrayWorkoutTypes;
                            MenuItem { text: modelData.labeltext }
                        }
                    }
                    Component.onCompleted: {
                        currentIndex = SharedResources.arrayWorkoutTypes.map(function(e) {
                            return e.name;
                        }).indexOf(Settings.activityType);

                        imgWorkoutImage.source = SharedResources.arrayWorkoutTypes[currentIndex].icon;
                        ActivityRecorder.setActivityType(SharedResources.arrayWorkoutTypes[currentIndex].name);

                        currentIndexChanged.connect(function() {
                            imgWorkoutImage.source = SharedResources.arrayWorkoutTypes[currentIndex].icon;
                            Settings.activityType = SharedResources.arrayWorkoutTypes[currentIndex].name;
                            ActivityRecorder.setActivityType(SharedResources.arrayWorkoutTypes[currentIndex].name);
                        });
                    }
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }

            TextSwitch
            {
                text: qsTr("Use HRM service if available")
                description: qsTr("Use heart rate monitor from another application e.g. Amazfish")
                Component.onCompleted: {
                    checked = Settings.useHRMservice;
                    checkedChanged.connect(function() {
                        Settings.useHRMservice = checked;
                    });
                }
            }

            TextSwitch
            {
                text: qsTr("Use HRM device")
                description: qsTr("Use heart rate monitor in this activity.")
                enabled: (Settings.hrmDeviceAddress !== "")
                Component.onCompleted: {
                    checked = (Settings.hrmDeviceAddress === "") ? false : Settings.useHRMdevice;
                    checkedChanged.connect(function() {
                        Settings.useHRMdevice = checked;
                    });
                }
            }

            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            TextSwitch
            {
                text: qsTr("Disable screen blanking")
                description: qsTr("Disable screen blanking when recording.")
                Component.onCompleted: {
                    checked = Settings.disableScreenBlanking;
                    checkedChanged.connect(function() {
                        Settings.disableScreenBlanking = checked;
                    });
                }
            }
        }
    }
}
