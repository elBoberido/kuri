/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.5
import Sailfish.Silica 1.0

Page
{
    allowedOrientations: Orientation.Portrait

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: content.height

        VerticalScrollDecorator {}

        Column
        {
            id: content
            anchors.top: parent.top
            width: parent.width

            PageHeader
            {
                title: qsTr("About Kuri")
            }
            Label
            {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.WordWrap
                maximumLineCount: 2
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Sport Tracker application for Sailfish OS")
            }
            Item
            {
                width: parent.width
                height: Theme.paddingLarge
            }
            Item
            {
                width : parent.width / 3
                height: parent.width / 3
                anchors.horizontalCenter: parent.horizontalCenter
                Image
                {
                    anchors.fill: parent
                    source: "../kuri.png"
                }
            }
            Item
            {
                width: parent.width
                height: Theme.paddingLarge
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("Version") + ": " + Qt.application.version
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("License: GPLv3")
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: "\u00A9 2020-2023 Mathias Kraus"
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: "\u00A9 2017-2019 Jens Drescher"
            }
            SectionHeader
            {
                text: qsTr("Contributors")
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: "Åke Engelbrektson"
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: "Bartłomiej \"Kormil\" Seliga"
            }
            Label
            {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeSmall
                text: "友橘(Youju)"
            }
            SectionHeader
            {
                text: qsTr("Credits")
            }
            Label
            {
                width: parent.width - 2 * Theme.paddingLarge
                wrapMode: Text.WordWrap
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeSmall
                text: qsTr("jdrescher2006 (Laufhelden maintainer), Simona (Rena maintainer) and all the contributors to Laufhelden and Rena")
            }
            SectionHeader
            {
                text: qsTr("Source code")
            }
            Label
            {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeSmall
                linkColor: Theme.secondaryHighlightColor
                property string urlstring: "https://gitlab.com/elBoberido/kuri"
                text: "<a href=\"" + urlstring + "\">" +  urlstring + "<\a>"
                wrapMode: Text.Wrap
                onLinkActivated: Qt.openUrlExternally(link)
            }
            SectionHeader
            {
                text: qsTr("Icons")
            }
            Label
            {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeSmall
                linkColor: Theme.secondaryHighlightColor
                property string urlstring: "https://icons8.com"
                text: "<a href=\"" + urlstring + "\">" +  urlstring + "<\a>"
                wrapMode: Text.Wrap
                onLinkActivated: Qt.openUrlExternally(link)
            }
            SectionHeader
            {
                text: qsTr("Feedback, bugs")
            }
            Label
            {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeSmall
                linkColor: Theme.secondaryHighlightColor
                property string urlstring: "https://gitlab.com/elBoberido/kuri/-/issues"
                text: "<a href=\"" + urlstring + "\">" +  urlstring + "<\a>"
                wrapMode: Text.Wrap
                onLinkActivated: Qt.openUrlExternally(link)
            }
            Item
            {
                width: parent.width
                height: Theme.paddingLarge
            }
        }
    }
}


