/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.5
import QtQuick.Layouts 1.1
import Sailfish.Silica 1.0
import harbour.kuri 1.0
import QtPositioning 5.2
import "../tools/SharedResources.js" as SharedResources
import "../components/"

Page
{
    id: root
    allowedOrientations: Orientation.Portrait

    readonly property bool mapMaximized: mapLoader.status === Loader.Ready && mapLoader.item.maximized

    // No back/forward navigation if the map is maximized
    backNavigation: !mapMaximized
    forwardNavigation: ActivityHistoryTrack.available && !mapMaximized

    property var activity: undefined

    Component.onCompleted: ActivityHistoryTrack.load(activity.activityId)
    Component.onDestruction: ActivityHistoryTrack.discard()

    onStatusChanged: { if (status === PageStatus.Active) { mapLoader.active = true; } }

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: root.height
        contentWidth: root.width

        VerticalScrollDecorator {}

        PullDownMenu
        {
            id: menu
            visible: !mapMaximized

            MenuItem
            {
                text: qsTr("Edit")
                onClicked: pageStack.push(Qt.resolvedUrl("EditActivityDialog.qml"), { activity: activity })
            }
            MenuItem
            {
                text: qsTr("Export")
                enabled: !ActivityHistory.activityExportOngoing
                onClicked: {
                    ActivityHistory.activityExportOngoingChanged.connect(showExportFinished);
                    ActivityHistory.exportActivity(activity.activityId);
                }

                function showExportFinished() {
                    if (!ActivityHistory.activityExportOngoing) {
                        ActivityHistory.activityExportOngoingChanged.disconnect(showExportFinished);
                        fncShowMessage("Info", qsTr("Activity export finished"), 5000);
                    }
                }
            }
            MenuItem
            {
                text: qsTr("Send to Strava")
                visible: stravaAPI.linked
                onClicked: pageStack.push(Qt.resolvedUrl("StravaUploadPage.qml"), { activity: activity })
            }
        }

        ColumnLayout
        {
            spacing: 0
            anchors.fill: parent

            PageHeader
            {
                Layout.fillWidth: true
                visible: !mapMaximized

                title: qsTr("Overview")
            }

            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: truncated ? Text.AlignLeft : Text.AlignHCenter
                visible: !mapMaximized

                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                elide: Text.ElideRight

                text: activity.dateTimeLong
            }
            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: Text.AlignHCenter
                visible: !mapMaximized

                font.pixelSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                wrapMode: Text.WordWrap

                text: activity.name==="" ? SharedResources.arrayLookupWorkoutTableByName[activity.activityType].labeltext : activity.name
            }
            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: Text.AlignHCenter
                visible: !mapMaximized && !(text === "")

                font.pixelSize: Theme.fontSizeSmall
                color: Theme.primaryColor
                wrapMode: Text.WordWrap

                text: activity.description
            }

            RowLayout {
                visible: !mapMaximized
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                InfoItem
                {
                    id: durationField
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Duration")
                    value: StringFormatter.secondsToHoursMinutesSeconds(activity.duration)
                    unit: qsTr("h:m:s")
                    condensed: false
                }
                InfoItem
                {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Distance")
                    value: (activity.distance / 1000000.).toFixed(2)
                    unit: qsTr("km")
                    condensed: false
                }
            }

            Separator
            {
                visible: !mapMaximized
                Layout.fillWidth: true

                color: Theme.secondaryHighlightColor
                horizontalAlignment: Qt.AlignHCenter
            }

            GridLayout {
                columns: 3
                rowSpacing: 0
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                visible: !mapMaximized

                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Speed ⌀")
                    value: activity.speedAverage.toFixed(1)
                    unit: qsTr("km/h")
                }
                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Pace ⌀")
                    value: StringFormatter.secondsToMinutesSeconds(activity.paceAverage)
                    unit: qsTr("min/km")
                }
                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Heart Rate ⌀")
                    value: activity.heartRateAverage > 0 ? activity.heartRateAverage : "---"
                    unit: "bpm"
                }
                Separator {
                    Layout.fillWidth: true
                    Layout.columnSpan: 3

                    color: Theme.secondaryHighlightColor
                    horizontalAlignment: Qt.AlignHCenter
                }
                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Pause")
                    value: activity.pause > 0 ? StringFormatter.secondsToHoursMinutesSeconds(activity.pause) : "---"
                    unit: qsTr("h:m:s")
                }
                InfoItem
                {
                    visible: false
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Elevation 🠝")
                    value: (activity.elevationUp / 1000.).toFixed(0)
                    unit: qsTr("m")
                }
                InfoItem
                {
                    visible: false
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Elevation 🠟")
                    value: (activity.elevationDown / 1000.).toFixed(0)
                    unit: qsTr("m")
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignCenter

                Text {
                    anchors.centerIn: parent

                    visible: busyIndicator.running

                    font.pixelSize: Theme.fontSizeLarge
                    color: Theme.secondaryColor

                    text: qsTr("Map")
                }

                BusyIndicator
                {
                    id: busyIndicator
                    anchors.centerIn: parent

                    visible: true

                    running: !mapLoader.visible
                    size: BusyIndicatorSize.Large
                }

                Loader {
                    id: mapLoader
                    anchors.fill: parent

                    active: false
                    asynchronous: false
                    visible: status === Loader.Ready

                    sourceComponent: Component {
                        Map {
                            id: map
                            anchors.fill: parent

                            mapGesturesEnabled: map.maximized

                            onShowSettings: pageStack.push(Qt.resolvedUrl("MapSettingsPage.qml"))

                            onMapCompleted: {
                                ActivityHistoryTrack.newTrackPoint.connect(newTrackPoint);
                                ActivityHistoryTrack.emitTrackPoints();
                            }

                            onMapDestruction: {
                                ActivityHistoryTrack.newTrackPoint.disconnect(newTrackPoint);
                            }

                            function newTrackPoint(point) {
                                map.setMapPoint(point);
                            }

                            Binding { target: map; property: "currentPosition"; value: ActivityHistoryTrack.currentPosition }
                        }
                    }
                }
            }

            TrackSlider {
                id: slider
                Layout.fillWidth: true

                minimumValue: 0
                maximumValue: ActivityHistoryTrack.totalPointCount < 2 ? 1 : ActivityHistoryTrack.totalPointCount - 1

                onValueChanged: ActivityHistoryTrack.currentIndex = slider.value.toFixed(0)

                Component.onCompleted: ActivityHistoryTrack.currentIndexChanged.connect(setValue)
                Component.onDestruction: ActivityHistoryTrack.currentIndexChanged.disconnect(setValue)

                function setValue(index) {
                    slider.value = ActivityHistoryTrack.currentIndex
                }
            }

            // this functions as a spacer to align the TrackSlider from above with the one from DiagramViewPage, which has a spacing set to the ColumnLayout
            Item {
                Layout.fillWidth: true
                height: Theme.paddingSmall
            }

            RowLayout {
                id: pageIndicator
                Layout.fillWidth: true
                spacing: radiusSmall

                readonly property int radiusSmall: Theme.paddingSmall
                readonly property int radius: 1.6 * radiusSmall

                Item {
                    Layout.fillWidth: true
                    height: 7 * pageIndicator.radiusSmall
                }

                Rectangle {
                    radius: pageIndicator.radius
                    width: 2 * radius
                    height: width
                    Layout.alignment: Qt.AlignVCenter

                    color: Theme.primaryColor
                }

                Rectangle {
                    radius: pageIndicator.radiusSmall
                    width: 2 * radius
                    height: width
                    Layout.alignment: Qt.AlignVCenter

                    color: Theme.secondaryColor
                }

                Item {
                    Layout.fillWidth: true
                }
            }
        }
    }
}
