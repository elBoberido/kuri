/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import Sailfish.Silica 1.0

import harbour.kuri 1.0

Page {
    id: page
    allowedOrientations: Orientation.Portrait

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}
        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("General settings")
            }
            // TODO: re-enable when properly implemented in the whole code base
            // ComboBox
            // {
            //     label: qsTr("Unit of measurement")
            //     description: qsTr("Note that this setting will be applied after restart of the application.")
            //     menu: ContextMenu
            //     {
            //         MenuItem { text: qsTr("Metric") }
            //         MenuItem { text: qsTr("Imperial") }
            //     }
            //     Component.onCompleted: {
            //         currentIndex = Settings.measureSystem;
            //         currentIndexChanged.connect(function() {
            //             Settings.measureSystem = currentIndex;
            //         });
            //     }
            // }
            // Separator
            // {
            //     color: Theme.highlightColor
            //     width: parent.width
            //     horizontalAlignment: Qt.AlignHCenter
            // }
            // Separator
            // {
            //     color: Theme.highlightColor
            //     width: parent.width
            //     horizontalAlignment: Qt.AlignHCenter
            // }
            TextSwitch
            {
                text: qsTr("Enable autosave")
                description: qsTr("No need to enter activity name at end of activity.")
                Component.onCompleted: {
                    checked = Settings.enableAutosave;
                    checkedChanged.connect(function() {
                        Settings.enableAutosave = checked;
                    });
                }
            }
        }
    }
}
