/*
 * Copyright (C) 2023 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.5
import QtQuick.Layouts 1.1

import Sailfish.Silica 1.0

Item {
    id: root

    property int textWidth: 0
    property string text: ""
    property string unit: ""

    width: Theme.paddingSmall + textWidth + Theme.paddingSmall + itemUnit.width

    RowLayout {
        anchors.fill: parent
        spacing: Theme.paddingSmall

        //horizontal spacer
        Item {
            Layout.fillWidth: true
            height: 1
        }
        Text {
            id: itemValue
            Layout.alignment: Qt.AlignBaseline | Qt.AlignRight

            font.pixelSize: Theme.fontSizeExtraSmall
            color: Theme.primaryColor

            text: root.text
        }
        Text {
            id: itemUnit
            Layout.alignment: Qt.AlignBaseline | Qt.AlignLeft
            visible: text != ""

            font.pixelSize: Theme.fontSizeTiny
            color: Theme.secondaryColor

            text: root.unit
        }
    }
}
