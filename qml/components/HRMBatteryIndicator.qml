/*
 * Copyright (C) 2020 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

Item {
    id: root

    property int indicatorWidth: 120
    property real batteryLevel: -1

    RowLayout {
        spacing: 0
        anchors.centerIn: parent

        Text {
            id: label
            font.pixelSize: Theme.fontSizeExtraSmall
            color: displayColors.secondary

            text: indicator.visible ? qsTr("HRM: ") : qsTr("HRM off")
        }

        Rectangle {
            id: indicator
            visible: batteryLevel >= 0.
            width: indicatorWidth

            height: root.height / 4
            color: "black"
            border.color: "#424242"
            border.width: 1

            Rectangle {
                anchors.left: parent.left
                height: parent.height

                property color strengthColor: batteryLevel < 25. ? "red" : (batteryLevel < 50. ? "orange" : batteryLevel < 75. ? "yellow" : "green")

                width: batteryLevel > 0. ? (indicatorWidth * batteryLevel / 100.) : 0
                color: strengthColor
            }
        }
    }
}
