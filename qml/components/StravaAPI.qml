/*
 * Copyright (C) 2022 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import Amber.Web.Authorization 1.0

import harbour.kuri 1.0

OAuth2AcPkce
{
    id: root

    property bool linked: Settings.stravaExpirationTime > 0

    clientId: Settings.stravaClientId
    clientSecret: Settings.stravaClientSecret

    // https://developers.strava.com/docs/getting-started/#account
    // https://developers.strava.com/docs/authentication/
    // https://developers.strava.com/docs/reference/
    scopes: ["activity:write", "activity:read_all"]
    scopesSeparator: ","
    authorizationEndpoint: "https://www.strava.com/oauth/authorize"
    tokenEndpoint: "https://www.strava.com/oauth/token"

    onReceivedAuthorizationCode: console.log("Got auth code, about to request token.");

    onReceivedAccessToken: {
        console.log("Access Token expires at: " + token.expires_at);

        Settings.stravaAccessToken = token.access_token;
        Settings.stravaRefreshToken = token.refresh_token;
        Settings.stravaExpirationTime = token.expires_at;

        var athlete = token.athlete;
        if (athlete.username != undefined) {
            Settings.stravaUserName = athlete.username;
        } else if (athlete.firstname != undefined && athlete.lastname != undefined) {
            Settings.stravaUserName = athlete.firstname + " " + athlete.lastname;
        } else {
            Settings.stravaUserName = "";
        }

        if (athlete.country != undefined) {
            Settings.stravaCountry = athlete.country;
        } else {
            Settings.stravaCountry = "";
        }

        root.linked = true;
    }

    onErrorOccurred: {
        console.log("Strava OAuth2 Error: " + error.code + " = " + error.message + " : " + error.httpCode);

        fncShowMessage("Error", qsTr("Strava authentication error: ") + error.message, 5000);
    }

    function setDefaultRequestHeaders(xmlhttp, token) {
        xmlhttp.setRequestHeader('Accept-Encoding', 'text');
        xmlhttp.setRequestHeader('Connection', 'keep-alive');
        xmlhttp.setRequestHeader('Pragma', 'no-cache');
        xmlhttp.setRequestHeader('Cache-Control', 'no-cache');
        xmlhttp.setRequestHeader('Authorization', "Bearer " + token);

        return xmlhttp;
    }

    function requestWithoutRefresh(method, url, json, callback) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open(method, url);
        xmlhttp = setDefaultRequestHeaders(xmlhttp, Settings.stravaAccessToken);
        xmlhttp.setRequestHeader('Content-Type', 'application/json');
        xmlhttp.setRequestHeader('Accept', 'application/json, text/plain, */*');
        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState===4) {
                callback(xmlhttp);
                if(xmlhttp.status!==200) {
                    console.log("Error: ", xmlhttp.responseType, xmlhttp.responseText, xmlhttp.status, xmlhttp.statusText);
                }
            }
        };

        if (method==="GET") {
            xmlhttp.send();
        } else {
            xmlhttp.send(JSON.stringify(json));
        }
    }

    function refreshAccessTokenIfRequiredAndThen(action) {
        // it is recommended to refresh the access token 1 hour before expiration
        const unixTime = Math.floor(Date.now() / 1000);
        if (Settings.stravaExpirationTime - unixTime < 3600) {
            refreshAccessToken(function() {
                action();
            });
        } else {
            action();
        }
    }

    function request(method, url, json, callback) {
        refreshAccessTokenIfRequiredAndThen(function() {
            requestWithoutRefresh(method, url, json, callback);
        });
    }

    function refreshAccessToken(callback) {
        var json = {
            client_id: Settings.stravaClientId,
            client_secret: Settings.stravaClientSecret,
            grant_type: "refresh_token",
            refresh_token: Settings.stravaRefreshToken
        };

        requestWithoutRefresh("POST", "https://www.strava.com/api/v3/oauth/token", json, function(xmlhttp) {
            if (xmlhttp.status===200) {
                var token = JSON.parse(xmlhttp.responseText);
                console.log("Access Token expires at: " + token.expires_at);

                Settings.stravaAccessToken = token.access_token;
                Settings.stravaRefreshToken = token.refresh_token;
                Settings.stravaExpirationTime = token.expires_at;

                if (callback !== undefined) {
                    callback();
                }
            }
        });
    }

    function unlink() {
        var json = {
            acceess_token: Settings.stravaAccessToken
        };

        request("POST", "https://www.strava.com/oauth/deauthorize", json, function(xmlhttp) {
            if (xmlhttp.status===200 || xmlhttp.status===401) {
                root.linked = false;
                Settings.stravaAccessToken = "";
                Settings.stravaRefreshToken = "";
                Settings.stravaExpirationTime = 0;
                Settings.stravaUserName = "";
                Settings.stravaCountry = "";
            }
        });
    }
}
