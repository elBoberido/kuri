/*
 * Copyright (C) 2019 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0
import QtPositioning 5.2
import MapboxMap 1.0

import harbour.kuri 1.0

Item {
    id: root

    property bool showSettingsButton: true
    property bool showMinMaxButton: true
    property bool showCenterPositionButton: true
    property bool mapGesturesEnabled: true

    readonly property int mapModeTracking: 0
    readonly property int mapModeViewing: 1

    property int mapMode: mapModeViewing

    property var currentPosition: QtPositioning.coordinate(52.51248, 13.36027)

    readonly property bool maximized: minmaxButton.checked

    property alias mapStyle: map.styleUrl

    signal showSettings()

    signal mapCompleted()
    signal mapDestruction()

    onCurrentPositionChanged: {
        //Map interaction is only done when map is really shown and application active
        if (!internal.viewActive) {
            return;
        }
        internal.updateCurrentPosition()
    }

    // this should be called when recording since the map will not always be visible
    function addMapPoint(point) {
        if (!internal.viewActive) {
            internal.mapHiddenPointCache.push(point);

            // prevent the cache to become too large and impact performance
            if (internal.mapHiddenPointCache.length > internal.maxCachedPoints) {
                internal.setCachedMapPoints();
                updateTrack();
            }

            return;
        }

        internal.setCachedMapPoints();

        setMapPoint(point);
    }

    // this should be called when viewing since the map will always be visible
    function setMapPoint(point) {
        // calculate best fitting view coordinates
        if (!point.coordinate.isValid) {
            console.log("Error! Called setMapPoint with invalid coordinate. TrackPoint::Tag: " + point.tag);

            if (point.tag === TrackPoint.Start) { internal.startMarkerMissing = true; }

            return;
        }

        if (internal.startMarkerMissing) {
            var missingStartPoint = point;
            missingStartPoint.tag = TrackPoint.Start;
            internal.setMapPointUnconditional(missingStartPoint);
            internal.startMarkerMissing = false;
        }

        internal.setMapPointUnconditional(point);
    }

    function updateTrack() {
        var numberOfPoints = map.trackLinePoints.length;
        if (numberOfPoints > 1) {
            map.updateSourceLine(map.trackLineKey, map.trackLinePoints);

            // prevent the cache to become too large and impact performance
            if (numberOfPoints > internal.maxCachedPoints) {
                internal.startNewTrackLine(map.trackLinePoints[numberOfPoints - 1]);
            }

            // center track on map
            if (internal.mapCenterMode === internal.mapCenterTrack && !root.maximized) {
                fitView();
            }
        }
    }

    function fitView() {
        if (internal.mapFittingPoints.length > 0) { map.fitView(internal.mapFittingPoints); }
    }

    Item {
        id: internal

        readonly property bool viewActive: root.visible && appWindow.applicationActive

        readonly property int mapCenterPosition: 0
        readonly property int mapCenterTrack: 1

        readonly property int mapCenterMode: root.mapMode === root.mapModeViewing ? mapCenterTrack : Settings.mapCenterMode

        readonly property var layers: QtObject {
            readonly property string location: "kuri-layer-location"
            readonly property string locationUncertainty: "kuri-layer-location-uncertainty"
            readonly property string track: "kuri-layer-track"
            readonly property string trackMarker: "kuri-layer-track-marker"
            readonly property string trackPositionMarker: "kuri-layer-track-position-marker"
        }

        readonly property var sources: QtObject {
            readonly property string currentPosition: "kuri-source-current-position"
            readonly property string track: "kuri-source-track"
            readonly property string trackBegin: "kuri-source-track-begin"
            readonly property string trackPause: "kuri-source-track-pause"
            readonly property string trackResume: "kuri-source-track-resume"
            readonly property string trackSection: "kuri-source-track-section"
            readonly property string trackEnd: "kuri-source-track-end"
        }

        property bool startMarkerMissing: false

        property int maxCachedPoints: 900
        property var mapHiddenPointCache: []

        property var mapFittingPoints: []

        function setCachedMapPoints() {
            if (internal.mapHiddenPointCache.length > 0) {
                for(var i = 0; i < internal.mapHiddenPointCache.length; i++) {
                    setMapPoint(internal.mapHiddenPointCache[i]);
                }

                internal.mapHiddenPointCache = [];
            }
        }

        function updateCurrentPosition()
        {
            if (!root.currentPosition.isValid) {
                return;
            }
            map.updateSourcePoint(internal.sources.currentPosition, root.currentPosition);
            updateMapUncertainty();

            if ((internal.mapCenterMode === internal.mapCenterPosition || internal.mapFittingPoints.length === 0)  && !root.maximized) {
                map.center = root.currentPosition;
            }
        }

        function updateMapUncertainty()
        {
            if (map.metersPerPixel > 0 && root.mapMode === root.mapModeTracking)
            {
                map.setPaintProperty(internal.layers.locationUncertainty, "circle-radius", (GeoPositionInfo.accuracy / map.metersPerPixel) / map.pixelRatio);
            }
        }

        function setMapPointUnconditional(point) {
            if (internal.mapFittingPoints.length === 0) {
                internal.mapFittingPoints.push(point.coordinate);
                internal.mapFittingPoints.push(point.coordinate);
            } else {
                const NORTH_EAST = 0;
                const SOUTH_WEST = 1;
                const latitude = point.coordinate.latitude;
                const longitude = point.coordinate.longitude;
                if (latitude > internal.mapFittingPoints[NORTH_EAST].latitude) { internal.mapFittingPoints[NORTH_EAST].latitude = latitude; }
                if (longitude > internal.mapFittingPoints[NORTH_EAST].longitude) { internal.mapFittingPoints[NORTH_EAST].longitude = longitude; }

                if (latitude < internal.mapFittingPoints[SOUTH_WEST].latitude) { internal.mapFittingPoints[SOUTH_WEST].latitude = latitude; }
                if (longitude < internal.mapFittingPoints[SOUTH_WEST].longitude) { internal.mapFittingPoints[SOUTH_WEST].longitude = longitude; }
            }

            switch(point.tag) {
                case TrackPoint.Info:
                    // 'Info' this is currently just a placeholder and ignored
                    break;
                case TrackPoint.Track:
                    //Save that to global array
                    map.trackLinePoints.push(point.coordinate);
                    break;
                case TrackPoint.Start:
                    const layerId = internal.layers.trackMarker + "-begin";
                    setMarker(point.coordinate, internal.sources.trackBegin, layerId, "kuri-image-begin", "circled-play-green-stroke.png");

                    if (root.mapMode === root.mapModeViewing) { root.currentPosition = point.coordinate; }

                    startNewTrackLine(point.coordinate);
                    break;
                case TrackPoint.Pause:
                    const sourceId = internal.sources.trackPause + "-" + map.trackIndex.toString();
                    const layerId = internal.layers.trackMarker + "-pause-" + map.trackIndex.toString();
                    const imageId = "kuri-image-pause-" + map.trackIndex.toString();
                    setMarker(point.coordinate, sourceId, layerId, imageId, "pause-button-yellow-stroke.png");
                    break;
                case TrackPoint.Resume:
                    const sourceId = internal.sources.trackResume + "-" + map.trackIndex.toString();
                    const layerId = internal.layers.trackMarker + "-resume-" + map.trackIndex.toString();
                    const imageId = "kuri-image-resume-" + map.trackIndex.toString();
                    setMarker(point.coordinate, sourceId, layerId, imageId, "circled-play-yellow-stroke.png");

                    //Doing the update here is OK because there should not be too many pauses.
                    map.updateSourceLine(map.trackLineKey, map.trackLinePoints);

                    startNewTrackLine(point.coordinate);
                    break;
                case TrackPoint.SectionCrossing:
                    const sourceId = internal.sources.trackSection + "-" + map.trackIndex.toString();
                    const layerId = internal.layers.trackMarker + "-section-crossing-" + map.trackIndex.toString();
                    const imageId = "kuri-image-section-crossing-" + map.trackIndex.toString();
                    setMarker(point.coordinate, sourceId, layerId, imageId, "record-blue-stroke.png");

                    //Doing the update here is OK because there should not be too many section crossings.
                    map.updateSourceLine(map.trackLineKey, map.trackLinePoints);

                    startNewTrackLine(point.coordinate);
                    break;
                case TrackPoint.Stop:
                    const layerId = internal.layers.trackMarker + "-end";
                    setMarker(point.coordinate, internal.sources.trackEnd, layerId, "kuri-image-end", "stop-circled-red-stroke.png");

                    updateTrack();
                    fitView();
                    break;
                default:
                    console.log("Unexpected TrackPoint::Tag: " + point.tag);
                    break;
            }
        }

        function setMarker(coordinate, sourceId, layerId, imageId, imageFileName) {
            internal.setImage(coordinate, sourceId, layerId, imageId, imageFileName, internal.layers.trackMarker);
        }

        function setImage(coordinate, sourceId, layerId, imageId, imageFileName, layerIdBefore) {
            map.addSourcePoint(sourceId, coordinate);
            map.addImagePath(imageId, Qt.resolvedUrl("../icons/map/" + imageFileName));
            map.addLayer(layerId, {"type": "symbol", "source": sourceId}, layerIdBefore);
            map.setLayoutProperty(layerId, "icon-image", imageId);
            map.setLayoutProperty(layerId, "icon-size", 0.33);
            map.setLayoutProperty(layerId, "icon-allow-overlap", true);
        }

        function startNewTrackLine(coordinate) {
            //Clear current point array and write first coordinate of new track segment to line array
            map.trackLinePoints = [];
            map.trackLinePoints.push(coordinate);

            map.trackIndex++;
            map.trackLineKey = internal.sources.track + "-" + map.trackIndex.toString();

            internal.addTrackLayer();
        }

        function addTrackLayer() {
            const LayerTrack = internal.layers.track + "-" + map.trackIndex.toString();
            map.addLayer(LayerTrack, { "type": "line", "source": map.trackLineKey }, internal.layers.track)
            map.setLayoutProperty(LayerTrack, "line-join", "round");
            map.setLayoutProperty(LayerTrack, "line-cap", "round");
            map.setPaintProperty(LayerTrack, "line-color", "red");
            map.setPaintProperty(LayerTrack, "line-width", 2.0);
        }

        function addCurrentPositionLayer() {
            //Create current position point on map
            map.addSourcePoint(internal.sources.currentPosition, map.center);

            if (root.mapMode === root.mapModeTracking) {
                map.addLayer(internal.layers.locationUncertainty, {"type": "circle", "source": internal.sources.currentPosition}, internal.layers.location);
                map.setPaintProperty(internal.layers.locationUncertainty, "circle-radius", (300 / map.metersPerPixel) / map.pixelRatio);
                map.setPaintProperty(internal.layers.locationUncertainty, "circle-color", "#7f1515");
                map.setPaintProperty(internal.layers.locationUncertainty, "circle-opacity", 0.25);
            }

            const LayerLocationRing = internal.layers.location + "-ring";
            map.addLayer(LayerLocationRing, {"type": "circle", "source": internal.sources.currentPosition}, internal.layers.location);
            map.setPaintProperty(LayerLocationRing, "circle-radius", 6.0);
            map.setPaintProperty(LayerLocationRing, "circle-color", "black");

            const LayerLocationFill = internal.layers.location + "-fill";
            map.addLayer(LayerLocationFill, {"type": "circle", "source": internal.sources.currentPosition}, internal.layers.location);
            map.setPaintProperty(LayerLocationFill, "circle-radius", 4.5);
            if (root.mapMode === root.mapModeTracking) {
                map.setPaintProperty(LayerLocationFill, "circle-color", "#a01b1b");
            } else {
                map.setPaintProperty(LayerLocationFill, "circle-color", "#228bc0");
            }
        }

        onViewActiveChanged: {
            if (viewActive) {
                internal.updateCurrentPosition();
                internal.setCachedMapPoints();
                root.updateTrack();
            }
        }
    }

    MapboxMap {
        id: map
        anchors.fill: parent

        center: QtPositioning.coordinate(52.51248, 13.36027)
        margins: Qt.rect(0.07, 0.07, 0.86, 0.86)
        zoomLevel: 16
        minimumZoomLevel: 0
        maximumZoomLevel: 20
        pixelRatio: Theme.pixelRatio * scale
        readonly property real scale: 1.75

        accessToken: "pk.eyJ1IjoiamRyZXNjaGVyIiwiYSI6ImNqYmVta256YTJsdjUzMm1yOXU0cmxibGoifQ.JiMiONJkWdr0mVIjajIFZQ"
        cacheDatabaseDefaultPath: true

        property string trackLineKey: ""
        property var trackLinePoints: []
        property int trackIndex: 0

        onMetersPerPixelChanged: internal.updateMapUncertainty()

        Component.onCompleted: {
            map.styleUrl = Settings.mapStyle;
            map.cacheDatabaseMaximalSize = Settings.mapCache * 1024 * 1024;

            map.addSourcePoint("kuri-source-root", map.center);
            map.addLayer(internal.layers.trackMarker, {"type": "line", "source" : "kuri-source-root"});
            map.addLayer(internal.layers.trackPositionMarker, {"type": "line", "source" : "kuri-source-root"});
            map.addLayer(internal.layers.track, {"type": "line", "source" : "kuri-source-root"});
            map.addLayer(internal.layers.location, {"type": "line", "source" : "kuri-source-root"});

            internal.addCurrentPositionLayer();
            internal.addTrackLayer();

            root.mapCompleted();
        }

        Component.onDestruction: {
            mapDestruction()
        }

        // TODO add centerTrackButton
        Rectangle
        {
            id: centerPositionButton
            anchors.left: parent.left
            anchors.leftMargin: Theme.paddingSmall
            anchors.top: parent.top
            anchors.topMargin: Theme.paddingSmall
            width: parent.width / 10
            height: parent.width / 10
            radius: width / 10
            color: "transparent"
            border.color: "#333333"
            border.width: 3
            visible: showCenterPositionButton
            z: 200

            Rectangle
            {
                anchors.fill: parent
                radius: parent.radius
                color: "#DDDDDD"
                opacity: 0.75
            }
            MouseArea
            {
                anchors.fill: parent
                onReleased: {
                    // TODO this should be handled by the centerTrackButton
                    if (root.mapMode === root.mapModeTracking) {
                        if(root.currentPosition.isValid) {
                            map.center = root.currentPosition;
                        } else {
                            console.log("Error: currentPosition is invalid!");
                        }
                    } else {
                        root.fitView();
                    }
                }
            }
            Image
            {
                anchors.fill: parent
                source: "../icons/map/hunt-pebble-padded.png"
            }
        }
        Rectangle
        {
            id: minmaxButton
            anchors.right: parent.right
            anchors.rightMargin: Theme.paddingSmall
            anchors.top: parent.top
            anchors.topMargin: Theme.paddingSmall
            width: parent.width / 10
            height: parent.width / 10
            radius: width / 10
            color: "transparent"
            border.color: "#333333"
            border.width: 3
            visible: showMinMaxButton
            z: 200

            property bool checked: false

            Rectangle
            {
                anchors.fill: parent
                radius: parent.radius
                color: "#DDDDDD"
                opacity: 0.75
            }
            MouseArea
            {
                anchors.fill: parent
                onReleased: minmaxButton.checked = !minmaxButton.checked
            }
            Image
            {
                anchors.fill: parent
                source: maximized ? "../icons/map/collapse-pebble-padded.png" : "../icons/map/expand-pebble-padded.png"
            }
        }
        Rectangle
        {
            id: settingsButton
            anchors.right: parent.right
            anchors.rightMargin: Theme.paddingSmall
            anchors.bottom: parent.bottom
            anchors.bottomMargin: Theme.paddingSmall
            width: parent.width / 10
            height: parent.width / 10
            radius: width / 10
            color: "transparent"
            border.color: "#333333"
            border.width: 3
            visible: showSettingsButton
            z: 200

            Rectangle
            {
                anchors.fill: parent
                radius: parent.radius
                color: "#DDDDDD"
                opacity: 0.75
            }
            MouseArea
            {
                anchors.fill: parent
                onReleased:
                {
                    showSettings();
                }
            }
            Image
            {
                anchors.fill: parent
                source: "../icons/map/settings-pebble-padded.png"
            }
        }

        MapboxMapGestureArea
        {
            id: gestureArea
            enabled: mapGesturesEnabled
            map: map
            anchors.fill: parent
            activeClickedGeo: true
            activeDoubleClickedGeo: true
            activePressAndHoldGeo: false
            z: 100

            onDoubleClicked:
            {
                map.setZoomLevel(map.zoomLevel + 1, Qt.point(mouse.x, mouse.y) );
            }
            onDoubleClickedGeo:
            {
                map.center = geocoordinate;
            }
        }

        MapScaleBar {
            id: scaleBar

            anchors.bottom: parent.bottom
            anchors.bottomMargin: Theme.paddingSmall
            anchors.left: parent.left
            anchors.leftMargin: Theme.paddingSmall
            z: 100

            Connections
            {
                target: map
                onMetersPerPixelChanged: scaleBar.update()
                onWidthChanged: scaleBar.update()
            }
        }
    }
}
