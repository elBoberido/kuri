/*
 * Copyright (C) 2020 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

Item {
    id: root

    property int indicatorWidth: 120
    property real accuracy: -1
    property real accuracyThreshold: 40

    RowLayout {
        spacing: 0
        anchors.centerIn: parent

        Text {
            id: label
            font.pixelSize: Theme.fontSizeExtraSmall
            color: displayColors.secondary

            text: indicator.visible ? qsTr("GPS: ") : qsTr("Waiting for GPS")
        }

        Rectangle {
            id: indicator
            visible: accuracy >= 0
            width: indicatorWidth

            height: root.height / 4
            color: "black"
            border.color: accuracy <= accuracyThreshold ? "#424242" : "red"
            border.width: 1

            Rectangle {
                anchors.left: parent.left
                height: parent.height

                property real strength: indicator.visible ? (1 - accuracy / accuracyThreshold) : 0
                property color strengthColor: strength < 0.25 ? "red" : (strength < 0.5 ? "orange" : strength < 0.75 ? "yellow" : "green")

                width: indicatorWidth * strength
                color: strengthColor
            }
        }
    }
}
