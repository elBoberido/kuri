/*
 * Copyright (C) 2020 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQml 2.2
import QtQuick.Layouts 1.0

import Sailfish.Silica 1.0

Item {
    id: root

    property color backgroundColor
    property color textColor
    property string text
    property string textLeft
    property string textRight
    property int fontSize: Theme.fontSizeMedium
    property bool slide: false
    property bool keepPressed: false

    readonly property bool pressed: (slide ? button.active : mouse.pressed) | keepPressed
    signal clicked()

    height: Theme.itemSizeSmall

    Item {
        id: slidingArea
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 5 * parent.height
        visible: root.slide

        Rectangle {
            id: slidingAreaBackground
            anchors.fill: parent
            radius: Theme.paddingMedium
            color: "#80000000"
            border.color: textColor
            opacity: mouse.pressed || bouncingAnimation.running ? 1 : 0

            Behavior on opacity {
                NumberAnimation { duration: 250 }
            }

            Text {
                id: backgroundArrows
                anchors.baseline: parent.top
                anchors.baselineOffset: 2 * root.height
                anchors.horizontalCenter: parent.horizontalCenter
                opacity: slidingAreaBackground.color.a

                font.pixelSize: root.height * 2
                font.bold: true
                lineHeightMode: Text.FixedHeight
                lineHeight: root.height * 0.8
                text: "︿\n︿\n︿"
                color: backgroundColor
            }
        }
    }

    Rectangle {
        id: button
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: mouse.pressed || bouncingAnimation.running ? undefined : parent.bottom
        height: root.height
        radius: Theme.paddingMedium
        color: root.pressed ? textColor : backgroundColor
        border.color: backgroundColor
        border.width: root.slide ? Theme.paddingSmall : 0

        SequentialAnimation on y {
            id: bouncingAnimation
            running: false

            readonly property int topPosition: - 3 * button.height;

            NumberAnimation { from: 0; to: bouncingAnimation.topPosition; duration: 150; easing.type: Easing.OutQuad }
            NumberAnimation { from: bouncingAnimation.topPosition; to: 0; duration: 850; easing.type: Easing.OutBounce; easing.amplitude: 2.5 }
        }

        property bool active: false
        property bool moving: false
        property int moveStartPosition: 0

        readonly property int yMin: button.height - slidingArea.height
        readonly property int yMax: 0

        RowLayout {
            anchors.centerIn: parent
            spacing: Theme.paddingMedium

            Text {
                font.pixelSize: fontSize
                font.bold: true
                color: root.pressed ? backgroundColor : textColor

                text: root.textLeft
            }

            Item {
                Layout.fillWidth: true
            }

            Text {
                font.pixelSize: fontSize
                font.bold: true
                color: root.pressed ? backgroundColor : textColor

                text: root.text
            }

            Item {
                Layout.fillWidth: true
            }

            Text {
                font.pixelSize: fontSize
                font.bold: true
                color: root.pressed ? backgroundColor : textColor

                text: root.textRight
            }
        }

        MouseArea {
            id: mouse
            anchors.fill: parent
            preventStealing: true

            onPressed: {
                if(keepPressed) { return }

                if (root.slide) {
                    button.moving = true;
                }
            }

            onReleased: {
                if(keepPressed) { return }

                button.moving = false;

                if(!root.slide || button.active) {
                    root.clicked();
                } else if (button.y === button.yMax) {
                    bouncingAnimation.running = true;
                }
                button.active = false;
            }

            onMouseYChanged: {
                if (button.moving) {
                    button.y = button.y - (button.moveStartPosition - mouse.y);
                    if (button.y < button.yMin) {
                        button.y = button.yMin;
                    } else if (button.y > button.yMax) {
                        button.y = button.yMax;
                    }
                    button.active = button.y === button.yMin;
                }
            }
        }
    }
}
