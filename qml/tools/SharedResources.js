/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

.pragma library

/*--------------START scanned bluetooth adapters--------------*/

var arrayMainDevicesArray = new Array();

function fncAddDevice(sBTName, sBTAddress)
{
    var iPosition = arrayMainDevicesArray.length;

    arrayMainDevicesArray[iPosition] = new Object();
    arrayMainDevicesArray[iPosition]["BTName"] = sBTName;
    arrayMainDevicesArray[iPosition]["BTAddress"] = sBTAddress;
}

function fncDeleteDevices()
{
    arrayMainDevicesArray = new Array();
}

function fncGetDevicesNumber()
{
    return arrayMainDevicesArray.length;
}

function fncGetDeviceBTName(iIndex)
{
    return arrayMainDevicesArray[iIndex]["BTName"];
}

function fncGetDeviceBTAddress(iIndex)
{
    return arrayMainDevicesArray[iIndex]["BTAddress"];
}

/*--------------END scanned bluetooth adapters--------------*/



/*--------------START workout table --------------*/

var arrayWorkoutTypes =
[
    { name: "running", labeltext: qsTr("Running"), icon: "../icons/workout/running.png" },
    { name: "biking", labeltext: qsTr("Roadbike"), icon: "../icons/workout/cycling.png" },
    { name: "mountainBiking", labeltext: qsTr("Mountainbike"), icon: "../icons/workout/cycling-mountain-bike.png" },
    { name: "walking", labeltext: qsTr("Walking"), icon: "../icons/workout/walking.png" },
    { name: "inlineSkating", labeltext: qsTr("Inline skating"), icon: "../icons/workout/roller-skating.png" },
    { name: "skiing", labeltext: qsTr("Skiing"), icon: "../icons/workout/skiing.png" },
    { name: "hiking", labeltext: qsTr("Hiking"), icon: "../icons/workout/trekking.png" },
    { name: "unknown", labeltext: qsTr("Unknown"), icon: "../icons/workout/question-mark.png" }
]

var arrayStravaWorkoutTypes =
[
    { name: "running", stravaType: "Run" },
    { name: "biking", stravaType: "Ride" },
    { name: "mountainBiking", stravaType: "Ride" },
    { name: "walking", stravaType: "Walk" },
    { name: "inlineSkating", stravaType: "InlineSkate" },
    { name: "skiing", stravaType: "AlpineSki" },
    { name: "hiking", stravaType: "Hike" }
]

var arrayWorkoutTypesFilterMainPage =
[
    { name: "allactivities", labeltext: qsTr("All activities"), icon: "../icons/four-squares-white.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "running", labeltext: qsTr("Running"), icon: "../icons/workout/running.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "biking", labeltext: qsTr("Roadbike"), icon: "../icons/workout/cycling.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "mountainBiking", labeltext: qsTr("Mountainbike"), icon: "../icons/workout/cycling-mountain-bike.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "walking", labeltext: qsTr("Walking"), icon: "../icons/workout/walking.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "inlineSkating", labeltext: qsTr("Inline skating"), icon: "../icons/workout/roller-skating.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "skiing", labeltext: qsTr("Skiing"), icon: "../icons/workout/skiing.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "hiking", labeltext: qsTr("Hiking"), icon: "../icons/workout/trekking.png", iDistance: 0, iDuration: 0, iWorkouts: 0 },
    { name: "unknown", labeltext: qsTr("Unknown"), icon: "../icons/workout/question-mark.png", iDistance: 0, iDuration: 0, iWorkouts: 0 }
]

function toStravaType(t)
{
    var ret = "";
    for (var i = 0; i < arrayStravaWorkoutTypes.length; i++)
    {
        if (arrayStravaWorkoutTypes[i].name === t) {
            ret = arrayStravaWorkoutTypes[i].stravaType;
            break;
        }
    }
    return ret;
}

function fromStravaType(t)
{
    console.log("looking for type ", t);
    var ret = "";
    for (var i = 0; i < arrayStravaWorkoutTypes.length; i++)
    {
        if (arrayStravaWorkoutTypes[i].stravaType === t) {
            ret = arrayStravaWorkoutTypes[i].name;
            break;
        }
    }
    return ret;
}

//Create lookup table for workout types.
//This is a helper table to easier access the workout type table.
var arrayLookupWorkoutTableByName = {};
for (var i = 0; i < arrayWorkoutTypes.length; i++)
{
    arrayLookupWorkoutTableByName[arrayWorkoutTypes[i].name] = arrayWorkoutTypes[i];
}

function fncGetIndexByName(sWorkoutName)
{
    for (var i = 0; i < arrayWorkoutTypes.length; i++)
    {
        if (arrayWorkoutTypes[i].name === sWorkoutName)
        {
            return i;
        }
    }

    return 0;
}

/*--------------END workout table --------------*/

