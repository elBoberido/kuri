/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.5
import QtQuick.Layouts 1.1
import Sailfish.Silica 1.0
import harbour.kuri 1.0

import "../components/"

CoverBackground
{
    id: root

    Image
    {
        anchors.margins: Theme.paddingMedium
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.right: parent.right
        fillMode: Image.PreserveAspectFit
        opacity: 0.2
        source: "../kuri.png"
    }

    ColumnLayout
    {
        anchors.margins: Theme.paddingMedium
        spacing: Theme.paddingSmall
        anchors.fill: parent

        Item {
            id: labelItem
            height: labelDisplay.height * 1.5
            Layout.fillWidth: true

            Label {
                id: labelDisplay
                anchors.centerIn: parent

                text: !ActivityRecorder.recording ? "Kuri" : (ActivityRecorder.pausing ? qsTr("Paused") : qsTr("Recording"))
            }
        }

        //vertical spacer
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        InfoItem
        {
            id: activityField
            visible: !ActivityRecorder.recording
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            label: qsTr("Activities")
            value: ""
            unit: ""

            function update() {
                value = ActivityHistory.activityCountFromFilter;
            }

            Component.onCompleted: {
                root.statusChanged.connect(activityField.update);
            }
        }

        InfoItem
        {
            id: durationField
            visible: ActivityRecorder.recording
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            label: qsTr("Duration")
            value: ActivityRecorder.durationAsHoursMinutesSeconds
            unit: qsTr("h:m:s")
        }

        //vertical spacer
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        InfoItem
        {
            id: totalDistanceField
            visible: !ActivityRecorder.recording
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            label: qsTr("Total distance")
            value: ""
            unit: qsTr("km")

            function update() {
                value = (ActivityHistory.activityDistanceFromFilter / 1000000.).toFixed(0)
            }

            Component.onCompleted: {
                // update();
                root.statusChanged.connect(totalDistanceField.update);
            }
        }

        InfoItem
        {
            id: distanceField
            visible: ActivityRecorder.recording
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            label: qsTr("Distance")
            value: (ActivityRecorder.distance / 1000000.).toFixed(2)
            unit: qsTr("km")
        }

        //vertical spacer
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        InfoItem
        {
            id: totalTimeField
            visible: !ActivityRecorder.recording
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            label: qsTr("Total time")
            value: ""
            unit: qsTr("hrs")

            function update() {
                value = (ActivityHistory.activityDurationFromFilter / (60 * 60)).toFixed(1)
            }

            Component.onCompleted: {
                // update();
                root.statusChanged.connect(totalTimeField.update);
            }
        }

        InfoItem {
            id: paceField
            visible: ActivityRecorder.recording
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true

            label: qsTr("Pace ⌀")
            value: ActivityRecorder.paceAverageInMinutesPerKm
            unit: qsTr("min/km")
        }

        //vertical spacer
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
